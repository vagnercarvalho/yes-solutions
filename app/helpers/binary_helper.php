<?php
/**
 * Project:    mmn.dev
 * File:       binary_helper.php
 * Author:     Felipe Medeiros
 * Createt at: 27/05/2016 - 09:25
 */
defined('BASEPATH') OR exit('No direct script access allowed');

function get_user($owner_id, $direction)
{
    $direction = 'u'.$direction;
	$tree = Binarytree::find_by_sql("SELECT * FROM `binarytrees` WHERE `user_id` = '{$owner_id}' LIMIT 1")[0];
	if (($userId = $tree->{$direction}))
	{
		$user = User::find_by_id($userId);
		return $user;
	}

	return FALSE;
}

    function create_tree($userId) {
        Binarytree::create(['user_id' => $userId, 'uleft' => 0, 'uright' => 0]);

        return Binarytree::find_by_user_id($userId);
    }



function get_direction($userId) {
    $user = User::find_by_id($userId);

    if ($user->position == 'left')
        $ladoescolhido = 'left';
    elseif ($user->position == 'right')
        $ladoescolhido = 'right';
    elseif ($user->position == 'auto') {
        if ($user->pleft > $user->pright) {
            $ladoescolhido = 'right';
        } else if ($user->pleft < $user->pright){
            $ladoescolhido = 'left';
        } else {
            $ladoescolhido = 'left';
        }
    }
    return $ladoescolhido;
}

function binary($user_id = FALSE, $pedid = FALSE) {

    if (!is_numeric($user_id) || !is_numeric($pedid) || User::count(array('conditions' => array('id = ?', $user_id))) != 1)
        return false;

    $user = User::find_by_id($user_id);

    $posicionado = Binarytree::find_by_user_id($user_id);

    // posicionado = usuário na árvore, pode ter um id de usuario a esquerda e a direita

    if (!$posicionado) {

        $createtree = create_tree($user_id); // árvore do usuário

        $sponsor = User::find_by_id($user->enroller);

        $tree = Binarytree::find_by_user_id($sponsor->id); // arvore do patrocinador

        $directionx = get_direction($sponsor->id); // direção do patrocinador
        $direction = "u" . $directionx . "";

        if ($tree) {
            if ($tree->{$direction} == 0) {
                $tree->{$direction} = $user->id;
                $tree->save();

                //return $sponsor->id;
            } else {
                $search = array($tree->{$direction});
                $actual = reset($search);

                while ($actual) {
                    
                    $tree = Binarytree::find_by_user_id($actual);

                    if ($tree->{$direction} == 0) {
                        $tree->{$direction} = $user->id;
                        $tree->save();
                        break;
                    } else {
                        $search[] = $tree->{$direction};
                        $actual = next($search);
                    }
                }
            }
        }
    }
    
    
    $invoice = Invoice::find_by_id($pedid);

    $items = InvoicesItem::find_by_sql("SELECT  pdt.bonus_indicacao_direta, pdt.tipo_bonus_indicacao_direta, pdt.bonus_points, pdt.bonus_indicacao_level_2, pdt.bonus_indicacao_level_3, pdt.bonus_indicacao_level_4,
                                                pdt.bonus_indicacao_level_5, pdt.bonus_indicacao_level_6  
                                                FROM invoices_items i
                                                INNER JOIN products pdt ON (i.plan_id = pdt.id)
                                                WHERE i.invoice_id = {$invoice->id};");
    foreach ($items as $invoiceItem) {
        $bonus = 0;
        $points = $invoiceItem->bonus_points;


        if ($invoiceItem->tipo_bonus_indicacao_direta == 'p') {
            $level = 1;
            $u = User::find_by_id($user_id);
            while ($level <= 6 && $u->enroller > 0) {

                $u = User::find_by_id($u->enroller);

                switch ($level) {
                    case 1: 
                        $bonus = $invoiceItem->tipo_bonus_indicacao_direta;
                        break;
                    case 2: 
                        $bonus = $invoiceItem->bonus_indicacao_level_2;
                        break;
                    case 3: 
                        $bonus = $invoiceItem->bonus_indicacao_level_3;
                        break;
                    case 4: 
                        $bonus = $invoiceItem->bonus_indicacao_level_4;
                        break;
                    case 5: 
                        $bonus = $invoiceItem->bonus_indicacao_level_5;
                        break;
                    case 6: 
                        $bonus = $invoiceItem->bonus_indicacao_level_6;
                        break;
                }

                if ($bonus > 0) {
                    update_points($u->id, $bonus);
                }
                $level++;
            }
        }

        if ($points > 0) {
            $updatepoints = update_points($user_id, $points);
        }
    }
}

function update_points($user = FALSE, $points = FALSE) {
    $actual = $user;
    $continue = TRUE;
    while ($continue) {
        $tree = Binarytree::find_by_sql("SELECT * FROM `binarytrees` WHERE (`uleft` = '{$actual}' OR `uright` = '{$actual}') LIMIT 1")[0];
        if (!$tree) {
            $continue = FALSE;
            break;
        } else {
            $update = User::find_by_id($tree->user_id);

            if ($tree->uleft == $actual)
                $update->pleft += $points;
            elseif ($tree->uright == $actual)
                $update->pright += $points;

            $update->save();

            $actual = $tree->user_id;
            if (!$actual) {
                $continue = FALSE;
                break;
            }
        }
    }
}
