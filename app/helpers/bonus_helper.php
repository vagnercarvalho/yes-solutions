<?php
/**
 * Project:    mmn.dev
 * File:       bonus_helper.php
 * Author:     Felipe Medeiros
 * Createt at: 27/05/2016 - 09:25
 */
defined('BASEPATH') OR exit('No direct script access allowed');



function bonus_indication($user_id = FALSE, $level = FALSE, $pedid = FALSE, $bonusSource = "") {

    if (!is_numeric($user_id) || !is_numeric($pedid) || User::count(array('conditions' => array('id = ?', $user_id))) != 1)
        return false;

    $user = User::find_by_id($user_id);

    $invoice = Invoice::find_by_id($pedid);


    $items = InvoicesItem::find_by_sql("SELECT  pdt.bonus_indicacao_direta, pdt.tipo_bonus_indicacao_direta, pdt.bonus_indicacao_level_2, pdt.bonus_indicacao_level_3, pdt.bonus_indicacao_level_4,
                                                pdt.bonus_indicacao_level_5, pdt.bonus_indicacao_level_6 
                                                FROM invoices_items i
                                                INNER JOIN products pdt ON (i.plan_id = pdt.id)
                                                WHERE i.invoice_id = {$invoice->id};");

                                                    
    foreach ($items as $invoiceItem) {
        
        $bonus = 0;
        
        switch ($level) {
            case 1: 
                $bonus = $invoiceItem->bonus_indicacao_direta;
                break;
            case 2: 
                $bonus = $invoiceItem->bonus_indicacao_level_2;
                break;
            case 3: 
                $bonus = $invoiceItem->bonus_indicacao_level_3;
                break;
            case 4: 
                $bonus = $invoiceItem->bonus_indicacao_level_4;
                break;
            case 5: 
                $bonus = $invoiceItem->bonus_indicacao_level_5;
                break;
            case 6: 
                $bonus = $invoiceItem->bonus_indicacao_level_6;
                break;
        }

        if ($bonus > 0) {
            
            //$bonus = $invoiceItem->bonus_indicacao_direta;
            
            if ($invoiceItem->tipo_bonus_indicacao_direta == 'r') {
                
                /* Insere extrato */
                $insert = array();
                $insert['user_id']		= $user_id;
                $insert['date']		= date('Y-m-d H:i:s');
                $insert['value']		= $bonus;

                if($level == 1) { 
                    $insert['description'] = "Bonus de indicação direta de {$bonusSource}.";
                } else {
                    $insert['description'] = "Bonus de indicação indireta de {$bonusSource}.";
                }

                $insert['type']	= 'credit';

                if($level == 1) { 
                    $insert['bonus_cod'] = 1;
                } else { 
                    $insert['bonus_cod'] = 2;
                }

                $insert['subtype']		= 'bonus';
                $insert['invoice_id']   = $pedid;

                Extract::create($insert);
                
                $user->balance += $bonus; //CREDITA VALOR BONUS
                $user->ganhos += $bonus;
                $user->save();
            } else {
                //exit("Não é R");
            }
            
        } else {
            //exit("Bonus é zero");
        }


        if (!$level) {
            $level = 1;
        } else {
            ++$level;
        }

        if ($level <= 6 AND $user->enroller > 0) {
            bonus_indication($user->enroller, $level, $pedid);
        }
    }
}