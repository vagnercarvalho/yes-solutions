<?php
/**
* Project:    MMN-VIP
* File:       tree_helper.php
* Author:     Felipe Sites
* Createt at: 27/05/2016 - 09:25
*/
defined('BASEPATH') OR exit('No direct script access allowed');

function make_linear($enroller, $level = FALSE) {
if (!$level)	$level = 1;
else			++$level;
$users = User::all(['conditions' => ['enroller = ?', $enroller], 'order' => 'id asc']);
if ($users) {
$return = '';
foreach ($users as $user) {
$return	.= '<tr>
<td class="text-center"><span class="badge bg-teal">' . $level . '</span></td>
<td>' . $user->firstname . ' ' . $user->lastname . '</td>
<td>' . $user->email . '</td>
<td class="text-center">' . $user->mobilephone . '</td>
<td class="text-center">' . $user->pleft . '</td>
<td class="text-center">' . $user->pright . '</td>
<td class="text-center"><span class="label label-'; 
if($user->banned == 'Y'): $return .= 'warning';
elseif($user->status != 'active'): $return .= 'default';
else: $return .= 'success';
endif; $return .= '">'; 
if ($user->banned == 'Y'): $return .= 'BLOQUEADO';
elseif ($user->status != 'active'): $return .= 'PENDENTE';
else: $return .= 'ATIVO';
endif; $return .= '</span></td>
</tr>';
if ($level < 6)
$return	.= make_linear($user->id, $level);
}
return $return;
}
return FALSE;
}

function count_linear($enroller, $level = FALSE) {
global $i;
if (!$level)	$level = 1;
else			++$level;
$users = User::all(['conditions' => ['enroller = ?', $enroller], 'order' => 'id asc']);
if ($users) {
foreach ($users as $user) {
++$i;
if ($level < 6) count_linear($user->id, $level);
}
return $i;
}
return FALSE;
}

function count_inlevels($enroller, $level = FALSE) {
global $i;
global $nolevel1;
global $nolevel2;
global $nolevel3;
global $nolevel4;
global $nolevel5;
global $nolevel6;
if (!$level)	$level = 1;
else			++$level;
$users = User::all(['conditions' => ['enroller = ?', $enroller], 'order' => 'id asc']);
if ($users) {
foreach ($users as $user) {
++$i;
if($level == 1) ++$nolevel1;
if($level == 2) ++$nolevel2;
if($level == 3) ++$nolevel3;
if($level == 4) ++$nolevel4;
if($level == 5) ++$nolevel5;
if($level == 6) ++$nolevel6;
if ($level < 6) count_inlevels($user->id, $level);
}
}
if(empty($nolevel1)) $nolevel1 = 0;
if(empty($nolevel2)) $nolevel2 = 0;
if(empty($nolevel3)) $nolevel3 = 0;
if(empty($nolevel4)) $nolevel4 = 0;
if(empty($nolevel5)) $nolevel5 = 0;
if(empty($nolevel6)) $nolevel6 = 0;

/*
$return = "
<h1>&nbsp;&nbsp; Nível 1: <b>".$nolevel1."</b> - Nível 2: <b>".$nolevel2."</b> - Nível 3: <b>".$nolevel3."</b> - Nível 4: <b>".$nolevel4."</b> - Nível 5: <b>".$nolevel5."</b> - Nível 6: <b>".$nolevel6."</b> </h1>";*/


$return = "
<h1>&nbsp;&nbsp; 1º Nível: <b>".$nolevel1."</b> - 2º Nível: <b>".$nolevel2."</b> - 3º Nível: <b>".$nolevel3."</b>- 4º Nível: <b>".$nolevel4."</b>- 5º Nível: <b>".$nolevel5."</b></h1>";
return $return;
}