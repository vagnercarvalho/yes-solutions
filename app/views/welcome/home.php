<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Cronos Finance</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">


    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<![endif]-->

    <!-- CSS Files
    ================================================== -->
    <link rel="stylesheet" href="css/bootstrap.css" type="text/css">
    <link rel="stylesheet" href="css/jpreloader.css" type="text/css">
    <link rel="stylesheet" href="css/animate.css" type="text/css">
    <link rel="stylesheet" href="css/flexslider.css" type="text/css">
    <link rel="stylesheet" href="css/plugin.css" type="text/css">
    <link rel="stylesheet" href="css/prettyPhoto.css" type="text/css">
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="css/owl.theme.css" type="text/css">
    <link rel="stylesheet" href="css/style.css" type="text/css">
	<link rel="icon" type="image/png" href="images/favicon.png" />


    <!-- custom style css -->
    <link rel="stylesheet" href="css/custom-style.css" type="text/css">

    <!-- color scheme -->
    <link rel="stylesheet" href="css/color.css" type="text/css">

    <!-- revolution slider -->
    <link rel="stylesheet" href="rs-plugin/css/settings.css" type="text/css">
    <link rel="stylesheet" href="css/rev-settings.css" type="text/css">

    <!-- load fonts -->
    <link rel="stylesheet" href="fonts/font-awesome/css/font-awesome.css" type="text/css">
    <link rel="stylesheet" href="fonts/elegant_font/HTML_CSS/style.css" type="text/css">
    <link rel="stylesheet" href="fonts/et-line-font/style.css" type="text/css">

    <!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131074023-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'UA-131074023-1');
	</script>


		<!-- amCharts javascript sources -->
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/ammap.js"></script>
		<script type="text/javascript" src="https://www.amcharts.com/lib/3/maps/js/brazilLow.js"></script>

		<!-- amCharts javascript code -->
		<script type="text/javascript">
			AmCharts.makeChart("map",{
					"type": "map",
					"pathToImages": "http://www.amcharts.com/lib/3/images/",
					"addClassNames": true,
					"fontSize": 15,
					"color": "#000000",
					"projection": "mercator",
					"backgroundAlpha": 1,
					"backgroundColor": "rgba(255,255,255,1)",
					"dataProvider": {
						"map": "brazilLow",
						"getAreasFromMap": true,
						"images": [
							{
								"top": 40,
								"left": 60,
								"width": 80,
								"height": 40,
								"pixelMapperLogo": true,
								"imageURL": "http://pixelmap.amcharts.com/static/img/logo-black.svg",
								"url": "http://www.amcharts.com"
							},
							{
								"selectable": true,
								"title": "SEDE - PRESIDENTE PRUDENTE (18) 9999-9999",
								"longitude": -51.4518,
								"latitude": -21.1343,
								"svgPath": "M3.5,13.277C3.5,6.22,9.22,0.5,16.276,0.5C23.333,0.5,29.053,6.22,29.053,13.277C29.053,14.54,28.867,15.759,28.526,16.914C26.707,24.271,16.219,32.5,16.219,32.5C16.219,32.5,4.37,23.209,3.673,15.542C3.673,15.542,3.704,15.536,3.704,15.536C3.572,14.804,3.5,14.049,3.5,13.277C3.5,13.277,3.5,13.277,3.5,13.277M16.102,16.123C18.989,16.123,21.329,13.782,21.329,10.895C21.329,8.008,18.989,5.668,16.102,5.668C13.216,5.668,10.876,8.008,10.876,10.895C10.876,13.782,13.216,16.123,16.102,16.123C16.102,16.123,16.102,16.123,16.102,16.123",
								"color": "rgba(216,184,75,1)",
								"scale": 1
							},
							{
								"selectable": true,
								"title": "ITAPEMA (47) 9999-9999",
								"longitude": -49.14,
								"latitude": -27.0232,
								"svgPath": "M3.5,13.277C3.5,6.22,9.22,0.5,16.276,0.5C23.333,0.5,29.053,6.22,29.053,13.277C29.053,14.54,28.867,15.759,28.526,16.914C26.707,24.271,16.219,32.5,16.219,32.5C16.219,32.5,4.37,23.209,3.673,15.542C3.673,15.542,3.704,15.536,3.704,15.536C3.572,14.804,3.5,14.049,3.5,13.277C3.5,13.277,3.5,13.277,3.5,13.277M16.102,16.123C18.989,16.123,21.329,13.782,21.329,10.895C21.329,8.008,18.989,5.668,16.102,5.668C13.216,5.668,10.876,8.008,10.876,10.895C10.876,13.782,13.216,16.123,16.102,16.123C16.102,16.123,16.102,16.123,16.102,16.123",
								"color": "rgba(186,178,154,1)",
								"scale": 1
							}
						]
					},
					"balloon": {
						"horizontalPadding": 15,
						"borderAlpha": 0,
						"borderThickness": 1,
						"verticalPadding": 15
					},
					"areasSettings": {
						"color": "rgba(129,129,129,1)",
						"outlineColor": "rgba(255,255,255,1)",
						"rollOverOutlineColor": "rgba(255,255,255,1)",
						"rollOverBrightness": 20,
						"selectedBrightness": 20,
						"selectable": true,
						"unlistedAreasAlpha": 0,
						"unlistedAreasOutlineAlpha": 0
					},
					"imagesSettings": {
						"alpha": 1,
						"color": "rgba(129,129,129,1)",
						"outlineAlpha": 0,
						"rollOverOutlineAlpha": 0,
						"outlineColor": "rgba(255,255,255,1)",
						"rollOverBrightness": 20,
						"selectedBrightness": 20,
						"selectable": true
					},
					"linesSettings": {
						"color": "rgba(129,129,129,1)",
						"selectable": true,
						"rollOverBrightness": 20,
						"selectedBrightness": 20
					},
					"zoomControl": {
						"zoomControlEnabled": true,
						"homeButtonEnabled": false,
						"panControlEnabled": false,
						"right": 38,
						"bottom": 30,
						"minZoomLevel": 0.25,
						"gridHeight": 100,
						"gridAlpha": 0.1,
						"gridBackgroundAlpha": 0,
						"gridColor": "#FFFFFF",
						"draggerAlpha": 1,
						"buttonCornerRadius": 2
					}
				});
		</script>		</head>

<body id="homepage">

    <!-- This section is for Splash Screen -->
    <div id="jSplash">
        <section class="selected">
            Carregando...
        </section>
    </div>
    <!-- End of Splash Screen -->


    <div id="wrapper">
        <div class="page-overlay">
        </div>


        <!-- header begin -->
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <!-- logo begin -->
                        <h1 id="logo">
                            <a href="<?=base_url('');?>">
                                <img class="logo-1" src="images/logo.png" alt="">
                                <img class="logo-2" src="images/logo-2.png" alt="">
                            </a>
                        </h1>
                        <!-- logo close -->

                        <!-- small button begin -->
                        <span id="menu-btn"></span>
                        <!-- small button close -->

                        <!-- mainmenu begin -->
                        <nav>
                            <ul id="mainmenu">
                                <li><a class="active" href="#wrapper">INÍCIO</a></li>
                                <li><a href="#section-about">SOBRE NÓS</a></li>
                                <li><a href="#section-produtos">NOSSA EQUIPE</a></li>
                                <li><a href="#section-ganhos">NEGÓCIOS</a></li>
                                <li><a href="#section-unidades">UNIDADES</a></li>
				<li><a href="<?=base_url('backoffice/login');?>" target="_blank">BACKOFFICE</a></li>
                                <li><a href="#section-contact">CONTATO</a></li>
                            </ul>
                        </nav>

                    </div>
                    <!-- mainmenu close -->

                </div>
            </div>
        </header>
        <!-- header close -->


        <!-- content begin -->
        <div id="content" class="no-bottom no-top">


            <!-- section begin -->
            <section class="full-height dark no-padding dark" data-speed="5" data-type="background">
                <div class="de-video-container">
                    <div class="de-video-content">
                        <div class="text-center">
                            VOCÊ ESTÁ PREPARADO PARA O QUE VEM POR AÍ?
                            <div class="text-slider big-text">
                                <div class="text-item"><span class="id-color">+</span> <b>BANCO DIGITAL</b></div>
                                <div class="text-item"><span class="id-color">+</span> <b>EXCHANGE</b></div>
								<div class="text-item"><span class="id-color">+</span> <b>TECNOLOGIA</b></div>
								<div class="text-item"><span class="id-color">+</span> <b>TRADING</b></div>
                            </div>
                            <div class="spacer-single"></div>
                        </div>
                    </div>

                    <div class="de-video-overlay"></div>

                    <!-- load your video here -->
                    <video autoplay="" poster="images/background/bg-5.jpg">
						<source src="video/video.mp4" type="video/mp4" />
					</video>


                </div>

            </section>
            <!-- section close -->


            <!-- section begin -->
            <section id="section-about">
                <div class="container">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h1 class="animated" data-animation="fadeInUp">INSTITUCIONAL</h1>
                                    <span class="small-border animated" data-animation="fadeInUp"></span>
                                </h1>
                                <p class="lead animated" data-animation="fadeInUp">
                                    CRONOS FINANCE<br>É uma empresa pertencente ao grupo CRONOS CAPITAL, Somos especialistas em gerir carteira de investimentos. Utilizamos a inteligência artificial para gerir múltiplos negócios e oferecer grandes oportunidades de rentabilização.
                                </p>
                                <div class="spacer-single"></div>
                            </div>
                        </div>
                        <!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="0">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-genius"></i>
                                    <h3>Seriedade</h3>
                                </div>
                                <div class="info">
                                    Uma equipe competente, composta de pessoas cujo objetivo é oferecer a melhor oportunidade para você
                                	<br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

						<!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="400">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-layers"></i>
                                    <h3>Visão</h3>
                                </div>
                                <div class="info">
                                    Qualificar pessoas a serem promotores de benefícios para sua família e para sociedade
                                	<br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

						<!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="200">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-linegraph"></i>
                                    <h3>Resultado</h3>
                                </div>
                                <div class="info">
                                    Criamos um negócio perfeito, com o objetivo de trazer resultados reais e mudança de vida para todos nossos Investidores
                                	<br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

                        <!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="600">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-heart"></i>
                                    <h3>Amor</h3>
                                </div>
                                <div class="info">
                                    Realmente, nós temos paixão pelo que fazemos: oferecer uma mudança real e completa à vida das pessoas
                                	<br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

                    </div>
                </div>
            </section>
            <!-- section close -->

             <!-- section begin -->
            <section id="section-produtos">
                <div class="container">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h1 class="animated" data-animation="fadeInUp">NOSSA EQUIPE</h1>
                                    <span class="small-border animated" data-animation="fadeInUp"></span>
                                </h1>
                                <p class="lead animated" data-animation="fadeInUp">
                                    Nossa equipe é formada por especialistas multicanais, profissionais que se destacam em cada uma das suas áreas. Prezamos pelo desenvolvimento colaborativo e bem estar de todos os nossos colaboradores, clientes e amigos.
                                </p>
                                <div class="spacer-single"></div>
                            </div>
                        </div>
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                               <img src="images/equipe.png" alt="NOSSA EQUIPE" width="100%">
                            </div>
                        </div>
                     </section><!-- section close -->


                                  <!-- section begin -->
            <section id="section-ganhos">
                <div class="container">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h1 class="animated" data-animation="fadeInUp">NEGÓCIOS</h1>
                                    <span class="small-border animated" data-animation="fadeInUp"></span>
                                </h1>
                                <div class="spacer-single"></div>

                            </div>
                        </div>
                        <!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="0">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-wallet"></i>
                                    <h3>BANCO DIGITAL</h3>
                                </div>
                                <div class="info">
                                    Sua vida evoluiu. Seu jeito de investir não pode ficar parado no tempo. Vem para o Cronos Bank.
                                	<br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

						<!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="400">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-bargraph"></i>
                                    <h3>EXCHANGE</h3>
                                </div>
                                <div class="info">
                                    Atuamos no segmento de criptomoedas. Negociamos bitcoin com liquidez, facilidade, segurança e agilidade.
                                	<br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

						<!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="200">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-linegraph"></i>
                                    <h3>TRADING</h3>
                                </div>
                                <div class="info">
                                     Modelo de negócio rentável e sustentável, atuamos com os melhores traders do mercado internacional.
                                    <br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

                        <!-- featured box begin -->
                        <div class="feature-box-small-icon box-fx center col-md-3 animated" data-animation="fadeInUp" data-delay="600">
                            <div class="inner">
                                <div class="front">
                                    <i class="icon-laptop"></i>
                                    <h3>TECNOLOGIA</h3>
                                </div>
                                <div class="info">
                                    Somos uma incubadora tecnológica, atuamos com empresas com potencial de crescimento
                                	<br>
                                </div>
                            </div>
                        </div>
                        <!-- featured box close -->

                    </div>
                </div>
            </section>
            <!-- section close -->

            <!-- section begin -->
            <section id="section-about-us-2" class="side-bg no-padding">
                <div class="image-container col-md-5 pull-left animated" data-animation="fadeInLeft" data-delay="0"></div>

                <div class="container">
                    <div class="row">
                        <div class="inner-padding">
                            <div class="col-md-6 col-md-offset-6 animated" data-animation="fadeInRight" data-delay="200">
                                <h2>Um novo conceito</h2>

                                <p class="intro">A CRONOS FINANCE foi fundada para inovar o mercado, uma empresa especializada em desenvolver múltiplas oportunidades. Utilizamos a estratégia de marketing de afiliados e expandimos nossas operações por todo mercado brasileiro, formamos gestores e qualificamos para atuarem em diversas regiões. Nossa base operacional onde ocorre as negociações através da plataforma de negociação com todos os Mercados do Mundo, está localizada em Presidente Prudente – SP. Atuamos com a implementação de dois grandes mercados, iniciamos um sistema de comissionamento inteligente e sustentável, onde oferecemos a oportunidade de transformação financeira e social aos nossos associados.
Através de nossos produtos financeiros, podemos oferecer resultados reais, com segmentações precisas, custo baixo e ótimo custo x benefício aos nossos clientes e parceiros. Somos detentores de uma base de dados com informações, estratégicas e análise técnica.


                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- section close -->
            <!-- section begin -->
            <section class="no-padding dark" data-speed="5" data-type="background">
                <div class="de-video-container">
                    <div class="de-video-content">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-6 col-md-offset-3 text-center">
                                    <h1 class="animated" data-animation="fadeInUp">Você está <span class="id-color">pronto?</span>
                                        <span class="small-border animated" data-animation="fadeInUp"></span>
                                    </h1>
                                    <div class="animated" data-animation="fadeInUp">
                                        Oferecemos uma oportunidade única para você realizar uma mudança completa na sua forma de gerir seus investimentos e negócios. Investindo em negócios seguros, você poderá realizar muitos sonhos. Então Sonhe, volte a sonhar, pois com a Cronos Finance os seus sonhos irão se tornarão realidade!
                                    	<div class="spacer-single"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="de-video-overlay"></div>

                    <!-- load your video here -->
                    <video autoplay="" loop="" muted="">
                        <source src="video/video-1.webm" type="video/webm" />
                        <source src="video/video-1.mp4" type="video/mp4" />
                        <source src="video/video-1.ogg" type="video/ogg" />
                    </video>


                </div>
            </section>
            <!-- section close -->



            <!-- section begin -->
            <section id="section-about-us-3" class="side-bg no-padding" >
                    <div class="row">
                        <div class="col-md-6  animated" data-animation="fadeInLeft" data-delay="200">
                        <img src="../images/background/bg-side-2.jpg" alt="">
                        </div>

                            <div class="col-md-4 col-md-offset-1 animated" data-animation="fadeInRight" data-delay="200" style="padding-top: 50px;">
                                <h2>INVESTIR</h2>

                                <p class="intro">Esse é o verdadeiro caminho para uma vida financeira segura.<br>A verdade é que não importa quanto dinheiro você tem, mas sim o que você faz com ele.
Conheça as opções de investimentos que lhe proporciona flexbilidade e segurança.<br><br>
Baixe nossa apresentação e conheça nosso portfólio.<br><br>
                        <div class="feature-box-small-icon col-md-8 animated" data-animation="fadeInUp" data-delay="600">
                            <div class="inner">
                                <i class="icon-phone"></i>
                                <div class="text">
                                    <h3>Slides de Apresentação</h3>
                                    <a href="https://cronosfinance.com/images/apncronos.pdf" target="_blank"><font color="black">Clique aqui para acessar nossa apresentação e portfólio</font></a>
                                </div>
                            </div>
                        </div>
                        <!-- feature box close -->


                        <!-- feature box begin -->
                        <div class="feature-box-small-icon col-md-8 animated" data-animation="fadeInUp" data-delay="800">
                            <div class="inner">
                                <i class="icon-global"></i>
                                <div class="text">
                                    <h3>Escritório Virtual</h3>
                                    <a href="http://cronosfinance.com.br/backoffice/login" target="_blank"><font color="black"> Para acessar a sua conta em nossa Plataforma Virtual, clique aqui.</a><br><br><br><br>
                                </div>
                            </div>
                        </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
            </section>
            <!-- section close -->


             <!-- section begin -->
            <section id="section-unidades">
                <div class="container">
                    <div class="row">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3 text-center">
                                <h1 class="animated" data-animation="fadeInUp">NOSSAS UNIDADES</h1>
                                    <span class="small-border animated" data-animation="fadeInUp"></span>
                                </h1>
<div id="map" style="width: 100%; height: 708px;"></div>
<div class="spacer-single"></div>
                            </div>
                        </div>
                     </section><!-- section close -->





            <!-- section begin -->
            <section id="section-contact" class="dark">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <h1 class="animated" data-animation="fadeInUp">Entre em <span class="id-color"><b>Contato</b></span>
                        	<span class="small-border animated" data-animation="fadeInUp"></span>
                            </h1>
                            <p class="animated" data-animation="fadeIn">
                                Estamos prontos para receber suas dúvidas, sugestões e críticas :).
                            </p>
                            <div class="spacer-single"></div>
                        </div>

                        <div class="col-md-8 animated" data-animation="fadeInUp" data-delay="200" data-speed="5">

                            <form name="contactForm" id='contact_form' method="post" action='email.php'>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div id='name_error' class='error'>Por favor, preencha o campo nome.</div>
                                        <div>
                                            <input type='text' name='name' id='name' class="form-control" placeholder="Nome">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div id='email_error' class='error'>Preencha um e-mail válido.</div>
                                        <div>
                                            <input type='text' name='email' id='email' class="form-control" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div id='message_error' class='error'>O campo de mensagem não pode ser vazio.</div>
                                        <div>
                                            <textarea name='message' id='message' class="form-control" placeholder="Mensagem"></textarea>
                                        </div>
                                    </div>
                                    <div id='mail_success' class='success'>Sua mensagem foi enviada com sucesso. Obrigado!</div>
                                    <div id='mail_fail' class='error'>Ops, parece que ocorreu um erro ao enviar a sua mensagem.</div>
                                    <div class="col-md-12">
                                        <p id='submit'>
                                            <input type='submit' id='send_message' value='Enviar mensagem' class="btn btn-border">
                                        </p>
                                    </div>
                                </div>
                            </form>

                        </div>

                        <div class="col-md-4">
						<br>
                            <address>
                                <span><i class="fa fa-map-marker fa-lg"></i>Presidente Prudente, SP, Brasil</span>
                                <span><i class="fa fa-envelope-o fa-lg"></i><a href="mailto:contato@cronosfinance.com.br">contato@cronosfinance.com.br</a></span>
                            </address>
                        </div>
                    </div>
                </div>
            </section>
            <!-- section close -->

            <!-- footer begin -->
            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <div class="social-icons">
                                <a href="#"><i class="fa fa-facebook fa-lg"></i></a>
                                <a href="#"><i class="fa fa-envelope-o fa-lg"></i></a>
                            </div>
                        </div>

                            <div class="col-md-12 text-center">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<script>
				     (adsbygoogle = window.adsbygoogle || []).push({
				          google_ad_client: "ca-pub-4182181370978127",
				          enable_page_level_ads: true
				     });
				</script>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
            <!-- footer close -->
        </div>
    </div>


    <!-- Javascript Files
    ================================================== -->
    <script src="js/jquery.min.js"></script>
    <script src="js/jpreLoader.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.prettyPhoto.js"></script>
    <script src="js/easing.js"></script>
    <script src="js/jquery.ui.totop.js"></script>
    <script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.scrollto.js"></script>
    <script src="js/owl.carousel.js"></script>
    <script src="js/jquery.countTo.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/designesia.js"></script>
    <script src="js/validation.js"></script>

    <!-- SLIDER REVOLUTION SCRIPTS  -->
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.plugins.min.js"></script>
    <script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>

</body>
</html>
