<div class="panel panel-flat">
    <div class="panel-body">
        <form id="form-cadastro" method="post" class="form-horizontal" action="<?php echo site_url('admin/qualifications/save')?>" enctype="multipart/form-data">
            
            <input type="hidden" id="codigo" name="codigo" value="<?php echo $qualification->id?>" />
            
            <div class="row">
               
                <div class="col col-sm-4 texxt-center"> 
                    <img src="<?php echo site_url($qualification->foto)?>" alt="" class="img-thumbnail" style="max-height: 320px; max-width: 320px;">
                </div>
                
                
                <div class="col col-sm-8">
                    <div class="form-group row">
                        <label for="name" class="col-sm-12 control-label"><b>Nome:</b></label>
                        <div class="col-sm-12"><input id="name" name="name" class="form-control" type="text" value="<?= $qualification->name; ?>" required/></div>
                    </div>

                    <div class="form-group row">
                        <label for="foto" class="col-sm-12 control-label"><b>Foto da qualificação:</b></label>
                        <div class="col-sm-12"><input id="foto" name="foto" class="form-control" type="file" /></div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="pos" class="col-sm-12 control-label"><b>Posição:</b></label>
                        <div class="col-sm-12"><input id="pos" name="pos" class="form-control numeric" type="text" value="<?= $qualification->pos ?>" required/></div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="points" class="col-sm-12 control-label"><b>Pontos:</b></label>
                        <div class="col-sm-12"><input id="points" name="points" class="form-control numeric" type="text" value="<?= $qualification->points ?>" required/></div>
                    </div>
                    <div class="form-group row">
                        <label for="teto" class="col-sm-12 control-label"><b>Teto:</b></label>
                        <div class="col-sm-12"><input id="teto" name="teto" class="form-control money-mask" type="text" value="<?= display_money2($qualification->teto);  ?>" required/></div>
                    </div>
                </div>
            </div>
            

            <div class="text-right">
                <button type="button" class="btn bg-teal btn-labeled" onclick="window.location.href = '<?= site_url('admin/qualifications/index'); ?>'" id="btn-cancelar">
                    <b><i class="icon-circle-left2"></i></b>Voltar
                </button>
                <button type="submit" class="btn btn-primary btn-labeled" id="btn-salvar"> 
                    <b><i class="icon-floppy-disk"></i></b>Salvar
                </button>
            </div>
        </form>
    </div>
</div>

<script src="<?php echo site_url('js/jquery.form.js')?>" type="text/javascript"></script>
<script src="<?php echo site_url('js/jquery.alphanumeric.js')?>" type="text/javascript"></script>

<script>

    $(document).ready(function () {
        
        $(".numeric").numeric();
        
        $("#form-cadastro").ajaxForm({
            dataType: 'json',
            beforeSubmit: function () {
                $("#btn-cancelar, #btn-salvar").prop("disabled", true);
            },
            success: function (json) {
                try {
                    if (json.sucesso) {
                        $("#btn-cancelar, #btn-salvar").prop("disabled", false);
                        toastr["success"](json.mensagem, false);
                        
                        setTimeout(function () {
                            location = '<?php echo site_url('admin/qualifications/index')?>';
                        }, 3000);
                    } else {
                        toastr["error"](json.mensagem, false);
                    }
                } catch (e) {
                    toastr["error"](e, false);
                }
                $("#btn-cancelar, #btn-salvar").prop("disabled", false);
            }
        });
    });


</script>