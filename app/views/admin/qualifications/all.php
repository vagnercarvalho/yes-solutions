<?php
/**
 * mmn.dev
 * Arquivo:     plans.php
 * Autor:       Vagner Carvalho
 * Criado em:   11/12/2019
 */
?>
<div class="panel">
    <div class="panel-heading p-10 border-bottom-primary-800 border-bottom-lg">
        <h5 class="panel-title"><i class="icon-package position-left"></i> Lista de Qualificações</h5>
        <div class="text-right">
            <a href="<?= site_url('admin/qualifications/edit'); ?>"><span class="btn btn-primary">Adicionar novo <i class="icon-file-plus position-right"></i></span></a>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table data">
            <thead><tr>
                    <th width="10px">#</th>
                    <th>Qualificação</th>
                    <th style="text-align: center;">Pontos</th>
                    <th style="text-align: center;">Teto</th>
                    <th width="100px">Ações</th>
                </tr></thead>
            <tbody>
                <?php
                if (sizeof($qualifications) > 0) {
                    foreach ($qualifications as $qualification) {
                        ?>
                        <tr>
                            <td class="text-center"><?= $qualification->pos; ?></td>
                            <td><?= $qualification->name; ?></td>
                            <td style="text-align: center;"><?= $qualification->points; ?></td>
                            <td style="text-align: center;"><?= $qualification->teto; ?></td>
                            <td class="text-center">
                                <ul class="icons-list">
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-menu9"></i>
                                        </a>
                                        <ul class="dropdown-menu dropdown-menu-right">
                                            <li><a href="<?= site_url('admin/qualifications/edit/' . $qualification->id); ?>"><i class="icon-pencil"></i> Editar</a></li>
                                            <div class="divider"></div>
                                            <li><a href="<?= site_url('admin/qualifications/delete/' . $qualification->id); ?>"><i class="icon-pencil"></i> Excluir</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <?php
                    }
                }
                ?>
            </tbody>
        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $('.data').dataTable({
            columnDefs: [{
                    orderable: false,
                    width: '100px',
                    targets: [3]
                }],
            drawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
            },
            preDrawCallback: function () {
                $(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
            }
        });
    });
</script>