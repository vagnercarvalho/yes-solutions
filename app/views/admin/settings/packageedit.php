<div class="panel panel-flat">
	<div class="panel-body">
		<form method="post" class="form-horizontal">
			<div class="form-group row">
				<label for="name" class="col-sm-3 control-label"><b>Nome:</b></label>
				<div class="col-sm-5"><input id="name" name="name" class="form-control" type="text" value="<?=$package->name;?>" required/></div>
			</div>
                    
			<div class="form-group row">
				<label for="enabled" class="col-sm-3 control-label"><b>Status:</b></label>
				<div class="col-sm-5">
				<?php $options = array(
				'1'	=> 'Ativo',
				'0'	=> 'Inativo',
					);
			echo form_dropdown('enabled', $options, $package->enabled, 'class="form-control"'); ?>
				</div>
			</div>
                    
                    
                        <div class="form-group row">
				<label for="pay_profit" class="col-sm-3 control-label"><b>Paga Comissão:</b></label>
				<div class="col-sm-5">
				<?php $optionsPay = array(
				'1'	=> 'Sim',
				'0'	=> 'Não',
					);
			echo form_dropdown('pay_profit', $optionsPay, $package->pay_profit, 'class="form-control"'); ?>
				</div>
			</div>
                    
			<div class="form-group row">
				<label for="price" class="col-sm-3 control-label"><b>Valor:</b></label>
				<div class="col-sm-5"><input id="price" name="price" class="form-control money-mask" type="text" value="<?=display_money2($package->price);?>" required/></div>
			</div>
                    
			<div class="form-group row">
				<label for="profit_six_months" class="col-sm-3 control-label"><b>Rentabilidade Mensal (6 meses):</b></label>
				<div class="col-sm-5">
                                    <input id="profit_six_months" name="profit_six_months" class="form-control money-mask" type="text" value="<?=display_money2($package->profit_six_months);?>" required/>
                                </div>
			</div>
                    
			<div class="form-group row">
				<label for="profit_one_year" class="col-sm-3 control-label"><b>Rentabilidade Mensal (12 meses):</b></label>
				<div class="col-sm-5">
                                    <input id="profit_one_year" name="profit_one_year" class="form-control money-mask" type="text" value="<?=display_money2($package->profit_one_year);?>" required/>
                                </div>
			</div>
                    
			<div class="form-group row">
				<label for="profit_eighteen_months" class="col-sm-3 control-label"><b>Rentabilidade Mensal (18 meses):</b></label>
				<div class="col-sm-5">
                                    <input id="profit_eighteen_months" name="profit_eighteen_months" class="form-control money-mask" type="text" value="<?=display_money2($package->profit_eighteen_months);?>" required/>
                                </div>
			</div>
                    
                    
                    
                    <br><br>
                    <div class="row">
                        <div class="col col-sm-12">
                            <h4 class="page-header">Bonus de indicação direta</h4>
                        </div>
                    </div>
                    <br><br>
                    
                    
			<div class="form-group row">
				<label for="indication_level_one" class="col-sm-3 control-label"><b>Bonus de Indicação Direta (Nível 1):</b></label>
				<div class="col-sm-5">
                                    <input id="indication_level_one" name="indication_level_one" class="form-control money-mask" type="text" value="<?=display_money2($package->indication_level_one);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="indication_level_two" class="col-sm-3 control-label"><b>Bonus de Indicação Direta (Nível 2):</b></label>
				<div class="col-sm-5">
                                    <input id="indication_level_two" name="indication_level_two" class="form-control money-mask" type="text" value="<?=display_money2($package->indication_level_two);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="indication_level_three" class="col-sm-3 control-label"><b>Bonus de Indicação Direta (Nível 3):</b></label>
				<div class="col-sm-5">
                                    <input id="indication_level_three" name="indication_level_three" class="form-control money-mask" type="text" value="<?=display_money2($package->indication_level_three);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="indication_level_four" class="col-sm-3 control-label"><b>Bonus de Indicação Direta (Nível 4):</b></label>
				<div class="col-sm-5">
                                    <input id="indication_level_four" name="indication_level_four" class="form-control money-mask" type="text" value="<?=display_money2($package->indication_level_four);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="indication_level_five" class="col-sm-3 control-label"><b>Bonus de Indicação Direta (Nível 5):</b></label>
				<div class="col-sm-5">
                                    <input id="indication_level_five" name="indication_level_five" class="form-control money-mask" type="text" value="<?=display_money2($package->indication_level_five);?>" required/>
                                </div>
			</div>
                    
                    
                    <br><br>
                    <div class="row">
                        <div class="col col-sm-12">
                            <h4 class="page-header">Bonus Residual</h4>
                        </div>
                    </div>
                    <br><br>
                    
                    
			<div class="form-group row">
				<label for="residual_level_one" class="col-sm-3 control-label"><b>Bonus Residual (Nível 1):</b></label>
				<div class="col-sm-5">
                                    <input id="residual_level_one" name="residual_level_one" class="form-control money-mask" type="text" value="<?=display_money2($package->residual_level_one);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="residual_level_two" class="col-sm-3 control-label"><b>Bonus Residual (Nível 2):</b></label>
				<div class="col-sm-5">
                                    <input id="residual_level_two" name="residual_level_two" class="form-control money-mask" type="text" value="<?=display_money2($package->residual_level_two);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="residual_level_three" class="col-sm-3 control-label"><b>Bonus Residual (Nível 3):</b></label>
				<div class="col-sm-5">
                                    <input id="residual_level_three" name="residual_level_three" class="form-control money-mask" type="text" value="<?=display_money2($package->residual_level_three);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="residual_level_four" class="col-sm-3 control-label"><b>Bonus Residual (Nível 4):</b></label>
				<div class="col-sm-5">
                                    <input id="residual_level_four" name="residual_level_four" class="form-control money-mask" type="text" value="<?=display_money2($package->residual_level_four);?>" required/>
                                </div>
			</div>
			<div class="form-group row">
				<label for="residual_level_five" class="col-sm-3 control-label"><b>Bonus Residual (Nível 5):</b></label>
				<div class="col-sm-5">
                                    <input id="residual_level_five" name="residual_level_five" class="form-control money-mask" type="text" value="<?=display_money2($package->residual_level_five);?>" required/>
                                </div>
			</div>
                    
                    
			<div class="text-right">
			<button type="button" class="btn bg-teal btn-labeled" onclick="window.location.href='<?=site_url('admin/system/packages');?>'"><b><i class="icon-circle-left2"></i></b>Voltar</button>
				<button type="submit" class="btn btn-primary btn-labeled"><b><i class="icon-floppy-disk"></i></b>Alterar</button>
			</div>
		</form>
	</div>
</div>