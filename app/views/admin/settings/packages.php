<?php
/**
 * mmn.dev
 * Arquivo:     plans.php
 * Autor:       Vagner Carvalho
 * Criado em:   11/12/2019
 */
?>
<div class="panel">
	<div class="panel-heading p-10 border-bottom-primary-800 border-bottom-lg">
		<h5 class="panel-title"><i class="icon-package position-left"></i> Lista de Pacotes</h5>
					<div class="text-right">
				<a href="<?=site_url('admin/system/packagecreate');?>"><span class="btn btn-primary">Adicionar novo <i class="icon-file-plus position-right"></i></span></a>
			</div>
	</div>
	<div class="table-responsive">
		<table class="table data">
			<thead><tr>
				<th width="10px">#</th>
				<th>Pacote</th>
				<th style="text-align: center;">Valor</th>
				<th style="text-align: center;">6 Meses</th>
				<th style="text-align: center;">12 Meses</th>
				<th style="text-align: center;">18 Meses</th>
				<th class="text-center">Status</th>
				<th width="100px">Editar</th>
			</tr></thead>
			<tbody><?php foreach($packages as $package): ?>
				<tr>
					<td class="text-center"><?=$package->id;?></td>
					<td><?=$package->name;?></td>
                                        <td style="text-align: center;"><?=display_money($package->price);?></td>
                                        <td style="text-align: center;"><?= number_format($package->profit_six_months, 2, ",", "");?></td>
                                        <td style="text-align: center;"><?= number_format($package->profit_one_year, 2, ",", "");?></td>
					<td style="text-align: center;"><?= number_format($package->profit_eighteen_months, 2, ",", "");?></td>
					<td class="text-center">
						<span class="label label-<?=($package->enabled > 0 ? 'success' : 'danger');?>"><?=($package->enabled > 0 ? 'Ativo' : 'Inativo');?></span>
					</td>
					<td class="text-center">
						<ul class="icons-list">
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-menu9"></i>
								</a>
								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="<?=site_url('admin/system/packageedit/' . $package->id);?>"><i class="icon-pencil"></i> Editar</a></li>
								</ul>
							</li>
						</ul>
					</td>
				</tr>
			<?php endforeach;?></tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.data').dataTable({
			columnDefs: [{ 
				orderable: false,
				width: '100px',
				targets: [ 3 ]
			}],
			drawCallback: function () {
				$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
			},
			preDrawCallback: function() {
				$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
			}
		});
	});
</script>