<?php
/**
 * mmn.dev
 * Arquivo:     plans.php
 * Autor:       Vagner Carvalho
 * Criado em:   11/12/2019
 */

?>
<div class="panel">
    <div class="panel-heading p-10 border-bottom-primary-800 border-bottom-lg">
        <h5 class="panel-title"><i class="icon-package position-left"></i> Bônus Diário</h5>
    </div>
    <div class="panel-body">
        
        <table class="table condensed">
            <tr>
                <td  class="text-center">
                    <form method="post" action="<?= site_url('admin/system/dailyprofit'); ?>">
                        <input type="hidden" value="<?php echo $anterMes->format("Y-m-d") ?>" name="data" />
                        <button type="submit"  class="btn btn-primary"><< Config Mês Anterior</button>
                    </form>
                </td>
                <td class="text-center"><h4><?php echo $mesAtual ?>/<?php echo $anoAtual ?></h4></td>
                <td  class="text-center">
                    <form method="post" action="<?= site_url('admin/system/dailyprofit'); ?>">
                       
                        <input type="hidden" value="<?php echo $proxMes->format("Y-m-d") ?>" name="data" />
                        <button type="submit" class="btn btn-primary">Config Prox Mês >></button>
                    </form>
                </td>
            </tr>
            
            
            <tr>
                <td>
                    <label for="month-profit">Informe o Bônus total do Mês: </label>
                </td>
                <td colspan="2">
                    <div>
                        <div class="form-group">
                            <input type="text" class="form-control "  value="<?php echo number_format($profit, 2, ",", "")?>" id="month-profit"/>
                        </div>
                    </div>
                </td>
            </tr>
        <?php 
        $dia = 1;
        $percentual = 4;
        
        $qtdDias = 0;
        while ($dia <= $ultimoDiadoMes) {
            $idRef = $anoAtual . ($mesAtual > 9 ? $mesAtual : "0{$mesAtual}") .($dia > 9 ? $dia : "0{$dia}");
            
            $diaAtual = new DateTime("{$anoAtual}-" . ($mesAtual > 9 ? $mesAtual : "0{$mesAtual}") . "-" . ($dia > 9 ? $dia : "0{$dia}"));
            $diaSemana = $diaAtual->format("w");
            if ($diaSemana > 0 && $diaSemana < 6) {
                //exit($daily[$diaAtual->format("Y-m-d")]);
                $value = number_format((isset($daily[$diaAtual->format("Y-m-d")]) ? $daily[$diaAtual->format("Y-m-d")] : 0), 2, ".", "");
            ?>
            <tr>
                <td>
                    <label for="date-<?php echo $idRef ?>">Dia <?php echo $dia ?></label>
                </td>
                <td colspan="2">
                    <div>
                        <div class="form-group">
                            <input type="text" class="p form-control"  value="<?php echo $value ?>" id="date-<?php echo $idRef ?>" data-ref="<?php echo $diaAtual->format("Y-m-d") ?>"/>
                        </div>
                        
                    </div>
                </td>
            </tr>
            <?php
                $qtdDias++;
            }
            $dia++;
        }
        ?>
            
            <tr>
                <td>
                    <label for="date-<?php echo $idRef ?>">Acumulado dos <?php echo $qtdDias ?> dias: </label>
                </td>
                <td colspan="2">
                    <div>
                        <div class="form-group">
                            <input type="text" class="form-control"  value="" id="acumulado-dias" disabled="true" />
                            <p>Verifique com atenção se este campo corresponde ao valor desejado. Ele apresenta a soma dos percentuais de cada dia do mês. Sendo assim, o valor contido neste campo
                             será o valor somado no final do mês.</p>
                        </div>
                        
                    </div>
                </td>
            </tr>
        </table>
        
        <div class="form-group">
            <button class="btn btn-primary" type="button" onclick="atualizarTabela();">Atualizar Tabela</button>
        </div>
        
    </div>
</div>
<script type="text/javascript">
  
    
    
    $(document).ready(function () {
        
        $("#month-profit").keyup(function () {
            gerarProfit();
        });
        
        $(".p").keyup(function () {
            calcular();
        });
        
        <?php if ($loadScript) {?>
        gerarProfit();
        <?php }?>
    });
            

        function gerarProfit() {
            var totalDias = <?php echo $qtdDias ?>;
            var value = 0;
            var monthPercent = parseFloat($("#month-profit").val().length > 0 ? $("#month-profit").val().replace(",", '.') : 0);
            
            if (monthPercent > 0) {
                var percentPorDia = 100 / totalDias;
                var variacaoAtual = 0;

                var indice = 1;
                $(".p").each(function () {
                    let v = percentPorDia;

                    if (indice < totalDias) {
                        if (variacaoAtual > 0) {
                            v = v - variacaoAtual;
                            variacaoAtual = 0;
                        } else {
                            variacaoAtual = Math.random();
                            v = v + variacaoAtual;
                        }

                        $(this).val((v / 100 * monthPercent).toFixed(2));
                    } else {
                        v = monthPercent - value;
                        $(this).val(v.toFixed(2));
                    }


                    value += parseFloat($(this).val());

                    indice++;
                });
            
            
            } else {
                $(".p").val("0,00");
            }
            
            $("#acumulado-dias").val(value.toFixed(2).replace(".", ","));
        }

        function atualizarTabela() {
            
            let dias = [];
            $(".p").each(function () {
                let v = parseFloat($(this).val().replace(',', '.'));
                let r = $(this).attr('data-ref');
                
                dias.push({valor: v, ref: r});
            });
            
            $.ajax({
                url: '<?= site_url('admin/system/savedailyprofit'); ?>',
                method: 'post',
                dataType: 'json',
                data: {
                    profit: $("#month-profit").val(),
                    ref: "<?php echo "{$anoAtual}-" . ($mesAtual > 9 ? $mesAtual : "0{$mesAtual}") ?>",
                    dias: dias
                },
                success: function (json) {
                    try {
                        if (json.sucesso) {
                            alert("Tabela atualizada com sucesso!");
                        } else {
                            alert(json.mensagem);
                        }
                    } catch (e) {
                        alert(e);
                    }
                }
            });
            
        }
        
        function calcular() {
            let value = 0;
            $(".p").each(function () {
                value += parseFloat($(this).val());
            });
            $("#acumulado-dias").val(value.toFixed(2).replace(".", ","));
        }
</script>