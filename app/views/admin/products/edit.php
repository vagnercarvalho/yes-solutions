<div class="panel panel-flat">
    <div class="panel-body">
        <form id="form-cadastro" method="post" class="form-horizontal" action="<?php echo site_url('admin/products/save')?>" enctype="multipart/form-data">
            
            <input type="hidden" id="codigo" name="codigo" value="<?php echo $produto->id?>" />
            
            <div class="row">
                <div class="col col-sm-4 texxt-center"> 
                    <img src="<?php echo site_url($produto->foto)?>" alt="" class="img-thumbnail" style="max-height: 320px; max-width: 320px;">
                </div>
                
                
                <div class="col col-sm-8">
                    <div class="form-group row">
                        <label for="name" class="col-sm-12 control-label"><b>Nome:</b></label>
                        <div class="col-sm-12"><input id="name" name="name" class="form-control" type="text" value="<?= $produto->name; ?>" required/></div>
                    </div>

                    <div class="form-group row">
                        <label for="foto" class="col-sm-12 control-label"><b>Foto do Produto:</b></label>
                        <div class="col-sm-12"><input id="foto" name="foto" class="form-control" type="file" /></div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="status" class="col-sm-12 control-label"><b>Status:</b></label>
                        <div class="col-sm-12">
                            <?php
                            $options = array(
                                '1' => 'Ativo',
                                '0' => 'Inativo',
                            );
                            echo form_dropdown('status', $options, $produto->status, 'class="form-control"');
                            ?>
                        </div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="price" class="col-sm-12 control-label"><b>Valor:</b></label>
                        <div class="col-sm-12"><input id="price" name="price" class="form-control money-mask" type="text" value="<?= display_money2($produto->price); ?>" required/></div>
                    </div>
                    
                </div>
            </div>
            
            
            <div class="form-group row">
                <label for="binario_percentual_payment" class="col-sm-12 control-label"><b>Percentual de Pagamento do menor lado do binário:</b></label>
                <div class="col-sm-12"><input id="binario_percentual_payment" name="binario_percentual_payment" class="form-control " type="text" value="<?= ($produto->binario_percentual_payment); ?>" required/></div>
            </div>

            
            <div class="form-group row">
                <label for="bonus_points" class="col-sm-12 control-label"><b>Pontos Bonus:</b></label>
                <div class="col-sm-12"><input id="bonus_points" name="bonus_points" class="form-control" type="text" value="<?= $produto->bonus_points; ?>" required/></div>
            </div>
            
            
            <div class="form-group row">
                <label for="description" class="col-sm-12 control-label"><b>Descrição:</b></label>
                <div class="col-sm-12">
                    <textarea  id="description" name="description" class="form-control "><?= $produto->description; ?></textarea>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="characteristics" class="col-sm-12 control-label"><b>Características:</b></label>
                <div class="col-sm-12">
                    <textarea  id="characteristics" name="characteristics" class="form-control"><?= $produto->characteristics; ?></textarea>
                </div>
            </div>
            
            
            
            <div class="form-group row">
                <label for="reactivate" class="col-sm-12 control-label"><b>Tipo de produto:</b></label>
                <div class="col-sm-12">
                    <?php
                    $optionsPay = array(
                        '0' => 'Produto',
                        '1' => 'Reativação',
                    );
                    echo form_dropdown('reactivate', $optionsPay, $produto->reactivate, 'class="form-control"');
                    ?>
                </div>
            </div>
            
            <h4>Bônus de Indicação</h4>
            
            
            <div class="form-group row">
                <label for="tipo_bonus_indicacao_direta" class="col-sm-12 control-label"><b>Tipo de Bonus de indicação:</b></label>
                <div class="col-sm-12">
                    <?php
                    $optionsPay = array(
                        'p' => 'Pontos',
                        'r' => 'Saldo em R$',
                    );
                    echo form_dropdown('tipo_bonus_indicacao_direta', $optionsPay, $produto->tipo_bonus_indicacao_direta, 'class="form-control"');
                    ?>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="bonus_indicacao_direta" class="col-sm-12 control-label"><b>Bonus de indicação direta:</b></label>
                <div class="col-sm-12"><input id="bonus_indicacao_direta" name="bonus_indicacao_direta" class="form-control money-mask" type="text" value="<?= display_money2($produto->bonus_indicacao_direta); ?>" required/></div>
            </div>
            
            
            <div class="form-group row">
                <label for="bonus_indicacao_level_2" class="col-sm-12 control-label"><b>Bonus de indicação nível 2:</b></label>
                <div class="col-sm-12"><input id="bonus_indicacao_level_2" name="bonus_indicacao_level_2" class="form-control money-mask" type="text" value="<?= display_money2($produto->bonus_indicacao_level_2); ?>" required/></div>
            </div>
            
            <div class="form-group row">
                <label for="bonus_indicacao_level_3" class="col-sm-12 control-label"><b>Bonus de indicação nível 3:</b></label>
                <div class="col-sm-12"><input id="bonus_indicacao_level_3" name="bonus_indicacao_level_3" class="form-control money-mask" type="text" value="<?= display_money2($produto->bonus_indicacao_level_3); ?>" required/></div>
            </div>
            
            <div class="form-group row">
                <label for="bonus_indicacao_level_4" class="col-sm-12 control-label"><b>Bonus de indicação  nível 4:</b></label>
                <div class="col-sm-12"><input id="bonus_indicacao_level_4" name="bonus_indicacao_level_4" class="form-control money-mask" type="text" value="<?= display_money2($produto->bonus_indicacao_level_4); ?>" required/></div>
            </div>
            
            <div class="form-group row">
                <label for="bonus_indicacao_level_5" class="col-sm-12 control-label"><b>Bonus de indicação  nível 5:</b></label>
                <div class="col-sm-12"><input id="bonus_indicacao_level_5" name="bonus_indicacao_level_5" class="form-control money-mask" type="text" value="<?= display_money2($produto->bonus_indicacao_level_5); ?>" required/></div>
            </div>
            
            <div class="form-group row">
                <label for="bonus_indicacao_level_6" class="col-sm-12 control-label"><b>Bonus de indicação  nível 6:</b></label>
                <div class="col-sm-12"><input id="bonus_indicacao_level_6" name="bonus_indicacao_level_6" class="form-control money-mask" type="text" value="<?= display_money2($produto->bonus_indicacao_level_6); ?>" required/></div>
            </div>
            

            <div class="text-right">
                <button type="button" class="btn bg-teal btn-labeled" onclick="window.location.href = '<?= site_url('admin/products/index'); ?>'" id="btn-cancelar">
                    <b><i class="icon-circle-left2"></i></b>Voltar
                </button>
                <button type="submit" class="btn btn-primary btn-labeled" id="btn-salvar"> 
                    <b><i class="icon-floppy-disk"></i></b>Salvar
                </button>
            </div>
        </form>
    </div>
</div>

<script src="<?php echo site_url('js/jquery.form.js')?>" type="text/javascript"></script>
<script src="<?php echo site_url('js/jquery.alphanumeric.js')?>" type="text/javascript"></script>

<script>

    $(document).ready(function () {
        
        $("#bonus_points").numeric();
        
        $("#form-cadastro").ajaxForm({
            dataType: 'json',
            beforeSubmit: function () {
                $("#btn-cancelar, #btn-salvar").prop("disabled", true);
            },
            success: function (json) {
                try {
                    if (json.sucesso) {
                        $("#btn-cancelar, #btn-salvar").prop("disabled", false);
                        toastr["success"](json.mensagem, false);
                        
                        setTimeout(function () {
                            location = '<?php echo site_url('admin/products/index')?>';
                        }, 3000);
                    } else {
                        toastr["error"](json.mensagem, false);
                    }
                } catch (e) {
                    toastr["error"](e, false);
                }
                $("#btn-cancelar, #btn-salvar").prop("disabled", false);
            }
        });
    });


</script>