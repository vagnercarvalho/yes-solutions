<?php
/**
 * mmn.dev
 * Arquivo:     plans.php
 * Autor:       Vagner Carvalho
 * Criado em:   11/12/2019
 */
?>
<div class="panel">
	<div class="panel-heading p-10 border-bottom-primary-800 border-bottom-lg">
		<h5 class="panel-title"><i class="icon-package position-left"></i> Lista de Produtos</h5>
					<div class="text-right">
				<a href="<?=site_url('admin/products/edit');?>"><span class="btn btn-primary">Adicionar novo <i class="icon-file-plus position-right"></i></span></a>
			</div>
	</div>
	<div class="table-responsive">
		<table class="table data">
			<thead><tr>
				<th width="10px">#</th>
				<th>Produto</th>
				<th style="text-align: center;">Preço</th>
				<th style="text-align: center;">Bonus</th>
				<th style="text-align: center;">Data criação</th>
				<th class="text-center">Status</th>
				<th class="text-center">Reativação</th>
				<th width="100px">Editar</th>
			</tr></thead>
			<tbody>
                            <?php 
                            if (sizeof($products) > 0) {
                                foreach($products as $produto) {
                            ?>
				<tr>
					<td class="text-center"><?=$produto->id;?></td>
					<td><?=$produto->name;?></td>
                                        <td style="text-align: center;"><?= number_format($produto->price, 2, ",", "");?></td>
                                        <td style="text-align: center;"><?= number_format($produto->bonus_points, 2, ",", "");?></td>
                                        <td style="text-align: center;"><?= $produto->date_created ?></td>
					<td class="text-center">
                                            <span class="label label-<?=($produto->status > 0 ? 'success' : 'danger');?>"><?=($produto->status > 0 ? 'Ativo' : 'Inativo');?></span>
					</td>
                                        <td class="text-center">
                                            <span class="label label-<?=($produto->reactivate > 0 ? 'success' : 'danger');?>"><?=($produto->reactivate > 0 ? 'Sim' : 'Não');?></span>
					</td>
					<td class="text-center">
                                            <ul class="icons-list">
                                                <li class="dropdown">
                                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                                        <i class="icon-menu9"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-right">
                                                        <li><a href="<?=site_url('admin/products/edit/' . $produto->id);?>"><i class="icon-pencil"></i> Editar</a></li>
                                                    </ul>
                                                </li>
                                            </ul>
					</td>
				</tr>
			<?php 
                                }
                            }
                        ?>
                        </tbody>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
		$('.data').dataTable({
			columnDefs: [{ 
				orderable: false,
				width: '100px',
				targets: [ 3 ]
			}],
			drawCallback: function () {
				$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').addClass('dropup');
			},
			preDrawCallback: function() {
				$(this).find('tbody tr').slice(-3).find('.dropdown, .btn-group').removeClass('dropup');
			}
		});
	});
</script>