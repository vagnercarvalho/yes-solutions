
<div class="panel ">
    <div class="panel-heading p-10 border-bottom-none">
        <h5 class="panel-title"><i class="icon-coins fa-fw position-left"></i> Loja Virtual</h5>
    </div>

    <div class="panel-body " style="background-color:  transparent;">
        <form method="post" class="form-horizontal">

            
            <div class="text-right">
                <a class="btn btn-primary btn-labeled btn-lg" href="<?php echo site_url('backoffice/home/invoice') ?>">
                    <b class="cart-qty"><i class="icon-cart"></i> <?php echo $qtdProdutos ?></b>
                    Ir para o Carrinho
                </a>
                
                <br><br>
            </div>
            
            <div class="row">
                
                
                <?php
                
                    foreach ($produtos as $prod) {
                        $foto = (empty($prod->foto) ? "assets/images/sem-foto.jpg" : "uploads/{$prod->foto}");
                ?>
                
                <div class="col col-md-4 col-lg-3 col-xs-1 col-sm-2">
                    
                    <div class="panel" style="height: 400px; background-color: rgba(0,0,0,0.8);">
                        <div class="panel-body">
                            
                            <div class="row">
                                <div class="col col-xs-12 text-center">
                                    <img src="<?php echo site_url($foto) ?>" id="product-<?= $prod->id; ?>"  style="width: 100%; max-height: 200px;">
                                </div>
                                
                                <div class="col col-xs-12">
                                    <h6><?= $prod->name; ?></h6>
                                </div>
                            </div>
                        </div>
                        
                        <div class="panel-footer" style="background-color: transparent; padding: 0px;">
                            <div class="row">
                                <div class="col col-xs-12">
                                    <h4><strong>Preço: <?= display_money($prod->price); ?></strong></h4>
                                    <button type="button" class="btn btn-success btn-block" onclick="addToCart(<?php echo $prod->id ?>);" >Comprar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <br><br>
                </div>
                <?php 
                
                    } 
                ?>
                
            </div>
            
            
            <nav aria-label="Page navigation example" class="text-center">
                <ul class="pagination" >
                    
                    
                    <li class="page-item disabled">
                        <a class="page-link" href="<?php echo ($pagina == 1 ? "#" : site_url('backoffice/virtualstore/index/' . ($pagina-1)) )?>" tabindex="-1" <?php echo ($pagina == 1 ? "aria-disabled='true'" : "aria-disabled='false'") ?>>Anterior</a>
                    </li>
                    
                    <?php 
                    for ($i = 0; $i < $totalPaginas; $i++) {
                    ?>
                    <li class="page-item <?php echo (($i+1) == $pagina) ? "active" : "" ?>">
                        <a class="page-link " href="<?php echo site_url('backoffice/virtualstore/index/' . ($i+1))?>"><?php echo ($i+1)?></a>
                    </li>
                    <?php 
                    }
                    ?>
                    <li class="page-item">
                        <a class="page-link" href="<?php echo ($pagina == $totalPaginas ? "#" : site_url('backoffice/virtualstore/index/' . ($pagina+1)) )?>" <?php echo ($pagina == $totalPaginas ? "aria-disabled='true'" : "aria-disabled='false'") ?>>Next</a>
                    </li>
                </ul>
            </nav>
            

            <script type="text/javascript">
                $(document).ready(function ($) {

                });


                function addToCart(productId) {

                    if (productId > 0) {

                        $.ajax({
                            url: '<?php echo site_url('backoffice/home/addToCart') ?>',
                            method: 'post',
                            dataType: 'json',
                            data: {
                                product: productId
                            },
                            success: function (json) {
                                try {
                                    if (json.sucesso) {

                                        toastr["success"](json.mensagem, 'Invoice!');
                                        $(".cart-qty").html("<i class='icon-cart'></i> " + json.qtd);
                                    } else {
                                        toastr["error"](json.mensagem, 'Erro ao criar invoice!');
                                    }
                                } catch (e) {
                                    toastr["error"](e, 'Erro ao criar invoice!');
                                }
                            }
                        });
                    } else {
                        toastr["error"]("Por favor, selecione um plano.", 'Erro ao criar invoice!');
                    }
                }

            </script>
            <script>
                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip({html: true});
                });
            </script>

            
            <br><br>
            <div class="text-right">
                <a class="btn btn-primary btn-labeled btn-lg" href="<?php echo site_url('backoffice/home/invoice') ?>">
                    <b class="cart-qty"><i class="icon-cart"></i> <?php echo $qtdProdutos?></b>
                    Ir para o Carrinho
                </a>
            </div>
        </form>
    </div>
</div>
