<?php
/**
* Author:      Felipe Medeiros
* File:        withdrawal.php
* Created in:  26/06/2016 - 09:52
*/
?>
<!-- BEGIN PANEL STATS -->
<div class="panel panel-default">
<div class="panel-heading border-bottom-primary">
<h5 class="panel-title"><i class="icon-coins position-left"></i> Solicitações de Saque</h5>
<div class="heading-elements">
<button type="button" class="btn bg-primary-800 btn-labeled" data-toggle="modal" data-target="#modal_withdrawal">
<b><i class="icon-plus3"></i></b> Solicitar saque
</button>
</div>
</div>
<div class="table-responsive no-border">
<table class="table table-xs table-striped data">
<thead><tr>
<th class="text-center">Data</th>
<th class="text-center">Data Pgto</th>
<th>Forma</th>
<th>Banco</th>
<th>Carteira</th>
<th>Valor</th>
<th class="text-center">Status</th>
<th class="text-center">Ações</th>
</tr></thead>
<tbody><?php foreach ($withdrawals as $row): ?>
<?php $bank = Bank::find_by_code($row->bank_code); ?>
<tr>
<td class="text-center"><?=date('d/m/Y', strtotime($row->date));?></td>
<td class="text-center"><?php
if($row->status != 'open') echo date('d/m/Y', strtotime($row->payment_date));
else echo '-';
?></td>
<td><?=ucfirst($row->gateway);?></td>
<td><?=$bank->code;?> - <?=$bank->name;?></td>
<td><?php
if($row->gateway == 'bitcoin') echo $row->bitcoin_address;
else echo '-';
?></td>
<td><?=display_money($row->value);?></td>
<td class="text-center">
<span class="label label-<?php
if($row->status == 'open') 		 echo 'primary';
elseif($row->status == 'paid') 		 echo 'success';
elseif($row->status == 'chargeback') echo 'danger';
elseif($row->status == 'cancel')	 echo 'danger';
?>"><?php
if($row->status == 'open') 		 echo 'PENDENTE';
elseif($row->status == 'paid') 		 echo 'PAGO';
elseif($row->status == 'chargeback') echo 'ESTORNADO';
elseif($row->status == 'cancel')	 echo 'CANCELADO';
?></span>
</td>
<td class="text-center">
<ul class="icons-list">
<li class="dropdown">
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
<i class="icon-menu9"></i>
</a>
<ul class="dropdown-menu dropdown-menu-right">
<li><a href="<?=site_url('backoffice/ewallet/view/' . $row->id);?>"><i class="icon-eye"></i> Ver</a></li>
</ul>
</li>
</ul>
</td>
</tr>
<?php endforeach; ?></tbody>
</table>
</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
$('.data').dataTable();
})
</script>
<div id="modal_withdrawal" class="modal fade">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header bg-primary pr-15 pl-15 pt-10 pb-10">
<button type="button" class="close" data-dismiss="modal">&times;</button>
<h6 class="modal-title">Solicitar saque</h6>
</div>
<form action="<?=site_url('backoffice/ewallet/withdrawal');?>" method="post">
<div class="modal-body">
<!--<font color='red'><b>Você só pode sacar valores ganhos por partidas ou por rede, nunca de recarga.</b></font>
<br>
<br>-->
<div class="form-group">
<label for="type">Forma de recebimento:</label>
<?php $options = array(
'transferencia'		=> 'Transferência Bancária',
'bitcoin'			=> 'Carteira Bitcoin',
);
echo form_dropdown('type', $options, '', 'class="form-control"'); ?>
<p><b>Preencha corretamente seus dados bancários e seu endereço de carteira bitcoin.</b></p>
</div>
<div class="form-group">
<label for="amount">Valor do Saque:</label>
<input type="text" name="amount" id="amount" class="form-control money-mask" placeholder="Ex: <?=display_money(rand(100, 2000), '');?>" required />
<p><b>Saldo disponível:</b> <b><?php
$calculo = $creditswithdrawal - $debitswithdrawal;
if($calculo <= 0) $calculo = 0;
echo display_money($calculo);
?></b></p>
</div>
<div class="clearfix"></div>
<div class="form-group">
<p>Ao solicitar saque, você está ciente que:</p>
<ul class="clist clist-angle">
<li>O valor de saque minimo é de <b><?=display_money($this->settings->min_withdrawal);?></b>.</li>

<?php if($this->settings->withdrawal_percent == 0): ?>
<li>Não será cobrada nenhuma taxa sobre esta solicitação.</li>
<?php else: ?>
<li>Cobrado taxa de <?=$this->settings->withdrawal_percent?>% sobre esta solicitação.</li>
<?php endif; ?>
<li>O pagamento é enviado em até <b>1</b> dia(s) úteis.</li>
</ul>
</div>
</div>
<div class="modal-footer" style="margin-top: -50px;">
<input type="submit" class="btn btn-xs btn-primary" value="Solicitar" />
</div>
</form>
</div>
</div>
</div>