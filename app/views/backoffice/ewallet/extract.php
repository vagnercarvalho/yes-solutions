<?php
/**
 * Author:      Felipe Medeiros
 * File:        extract.php
 * Created in:  24/06/2016 - 22:37
 */
?>
<!-- BEGIN PANEL STATS -->
<div class="row">
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="dashboard-stat bg-primary-800">
			<div class="visual">
				<i class="icon-coins"></i>
			</div>
			<div class="details">
				<div class="number"><?=display_money($balance);?></div>
				<div class="desc">Saldo Disponível</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="dashboard-stat bg-success-800">
			<div class="visual">
				<i class="icon-diff-added"></i>
			</div>
			<div class="details">
				<div class="number"><?=display_money($credits);?></div>
				<div class="desc">Créditos no mês</div>
			</div>
		</div>
	</div>
	<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
		<div class="dashboard-stat bg-danger-800">
			<div class="visual">
				<i class="icon-diff-removed"></i>
			</div>
			<div class="details">
				<div class="number"><?=display_money($debits);?></div>
				<div class="desc">Debitos no mês</div>
			</div>
		</div>
	</div>
</div>
<!-- END PANEL STATS -->
<div class="panel panel-default">
	<div class="panel-heading border-bottom-primary">
		<h5 class="panel-title"><i class="icon-printer4 position-left"></i> Extrato mensal</h5>
		<div class="heading-elements">
			<div class="btn-group heading-btn">
				<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar22 position-left"></i> <?=$year;?> <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . date('Y') . '/' . $month);?>"><i class="icon-calendar22 position-left"></i> <?=date('Y');?></a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . (date('Y') - 1) . '/' . $month);?>"><i class="icon-calendar22 position-left"></i> <?=(date('Y') - 1);?></a></li>
				</ul>
			</div>
			<div class="btn-group heading-btn">
				<button type="button" class="btn btn-xs btn-primary dropdown-toggle" data-toggle="dropdown"><i class="icon-calendar22 position-left"></i> <?=date('F', strtotime($year . '-' . $month . '-01'));?> <span class="caret"></span></button>
				<ul class="dropdown-menu dropdown-menu-right">
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/01')?>"><i class="icon-calendar22 position-left"></i> Janeiro</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/02')?>"><i class="icon-calendar22 position-left"></i> Fevereiro</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/03')?>"><i class="icon-calendar22 position-left"></i> Março</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/04')?>"><i class="icon-calendar22 position-left"></i> Abril</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/05')?>"><i class="icon-calendar22 position-left"></i> Maio</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/06')?>"><i class="icon-calendar22 position-left"></i> Junho</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/07')?>"><i class="icon-calendar22 position-left"></i> Julho</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/08')?>"><i class="icon-calendar22 position-left"></i> Agosto</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/09')?>"><i class="icon-calendar22 position-left"></i> Setembro</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/10')?>"><i class="icon-calendar22 position-left"></i> Outubro</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/11')?>"><i class="icon-calendar22 position-left"></i> Novembro</a></li>
					<li><a href="<?=site_url('backoffice/ewallet/extract/' . $year . '/12')?>"><i class="icon-calendar22 position-left"></i> Dezembro</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="table-responsive no-border">
		<table class="table table-xs table-striped data">
			<thead><tr>
				<th>Data</th>
				<th>Descrição</th>
				<th>Tipo</th>
				<th>Valor</th>
			</tr></thead>
			<tbody><?php
			$final_balance = 0;
			foreach ($extracts as $extract):
				if ($extract->type == 'credit'):	$final_balance += $extract->value;
				elseif ($extract->type == 'debit'):	$final_balance -= $extract->value;
				endif;
			?>
				<tr>
					<td class="text-center"><span><span class="hidden"><?=strtotime($extract->date);?></span> <?=date($this->settings->date_format, strtotime($extract->date));?></span></td>
					<td><?=$extract->description;?></td>
					<td class="text-center"><span class="label label-<?php if ($extract->type == 'debit'):
						echo 'danger';
					elseif ($extract->type == 'lost'):
						echo 'warning';
					elseif ($extract->type == 'credit'):
						echo 'success';
					endif; ?>"><?php if ($extract->type == 'debit'):
						echo 'Débito';
					elseif ($extract->type == 'lost'):
						echo 'Perca';
					elseif ($extract->type == 'credit'):
						echo 'Crédito';
					endif; ?></span></td>
					<td class="text-center"><?=display_money($extract->value);?></td>
				</tr>
			<?php endforeach; ?></tbody>
			<tfoot><tr>
				<td colspan="2"></td>
				<td class="text-right"><b>Saldo final:</b></td>
				<td><span class="label label-<?php if ($final_balance == 0):
					echo 'default';
				elseif ($final_balance > 0):
					echo 'success';
				elseif ($final_balance < 0):
					echo 'danger';
				endif; ?> label-block"><?=display_money($final_balance);?></span></td>
			</tr></tfoot>
		</table>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function () {
		$('.data').dataTable();
	})
</script>