<?php 
if (count($announcements) != 0) { 
    $class = "";
    
    if ($announcement->priority <= 1)		$class = 'primary';
    elseif ($announcement->priority <= 2)	$class = 'warning';
    elseif ($announcement->priority >= 3)	$class =  'danger';
?>
	<div class="row"><?php foreach($announcements as $announcement): ?>
		<div class="col-sm-6 col-lg-4 col-xs-12">
			<div class="panel panel-<?php echo $class ?> border-left-lg border-left-<?php echo $class ?>">
				<div class="panel-heading p-10">
					<h6 class="panel-title"><b><i class="icon-info3 position-left"></i> <?=$announcement->title;?></b></h6>
				</div>
				<div class="panel-body p-10" style="min-height: 190px;">
					<p class="text-justify"><?=character_limiter(nl2br($announcement->body), 300);?></p>
					
                                        
                                        Prioridade: <span class="text-semibold">
                                        <?php
                                           if($announcement->priority == 1)  echo 'Baixa';
                                        elseif($announcement->priority == 2) echo 'Média';
                                        elseif($announcement->priority == 3) echo 'Alta';
                                        ?>
                                        </span>
                                        
                                        <br>
                                        <span class="heading-text">
                                            Publicado em: 
                                            <span class="text-semibold"><?=date($this->settings->date_format, strtotime($announcement->date));?></span> 
                                            às <span class="text-semibold"><?=date($this->settings->date_time_format, strtotime($announcement->date));?></span>
                                        </span>
                                        
                                        <br><br>
                                        <p class="text-right no-margin"><a href="<?=site_url('backoffice/announcements/view/' . $announcement->slug);?>">Continuar lendo <i class="icon-arrow-right15"></i></a></p>
				</div>
				
			</div>
		</div>
	<?php endforeach; ?></div>
	<div class="mb-20"><?=$paginator;?></div>
<?php } else { ?>
	<div class="alert alert-warning alert-styled-left">
		<span class="text-semibold">Atenção!</span> No momento não existe nenhum comunicado.
	</div>
<?php } ?>