<?php if($lottery->status == 'open' AND empty($meustickets)): ?>
<div class="alert alert-info alert-styled-left">
	<span class="text-semibold">Atenção!</span> Você ainda não adquiriu tickets desta loteria. Adquira já!
</div>
<?php endif; ?>
<?php if($lottery->status == 'cancel'): ?>
<div class="alert alert-warning alert-styled-left">
	<span class="text-semibold">Atenção!</span> Loterias canceladas tem os tickets reembolsados automaticamente.
</div>
<?php endif; ?>
<div class="panel panel-<?php
                            if($lottery->status == 'open')       echo 'primary';
                        elseif($lottery->status == 'paid')       echo 'success';
                        elseif($lottery->status == 'cancel')     echo 'danger';
                        ?> panel-bordered">
    <div class="panel-heading p-10">
        <h6 class="panel-title"><b><i class="icon-ticket position-left"></i> <?php echo $lottery->name?></b> (#<?php echo $lottery->id?>)</h6>
    </div>
    <div class="panel-body p-10">
        <center>
			<?php if($lottery->type == 'fix') $tipoloteria = "Loteria Fixa"; else $tipoloteria = "Loteria Acumulativa"; ?>
				<h5>
				<?php echo $lottery->name?> (#<?php echo $lottery->id;?>) (<?=$tipoloteria?>)<br>
				<?php echo $lottery->description?><br><br>
				
            <span class="label label-<?php
                            if($lottery->status == 'open')       echo 'primary';
                        elseif($lottery->status == 'paid')       echo 'success';
                        elseif($lottery->status == 'cancel')     echo 'danger';
                        ?> col-md-6 col-md-offset-3" style="font-size: 20px; margin-bottom:20px;">
                        <?php
                            if($lottery->status == 'open')       echo 'ABERTA';
                        elseif($lottery->status == 'paid')       echo 'PAGA';
                        elseif($lottery->status == 'cancel')     echo 'CANCELADA';
                        ?>
            </span>
			
				<br>
			<table class="table table-responsive text-left" style="width: 600px;">
				<tr>
					<td class="field-title">Inicio:</td>
					<td><?php echo date($this->settings->date_format, strtotime($lottery->starts));?> às <?php echo date($this->settings->date_time_format, strtotime($lottery->starts));?></td>
				</tr>
				<tr>
					<td class="field-title">Fim:</td>
					<td><?php echo date($this->settings->date_format, strtotime($lottery->ends));?> às <?php echo date($this->settings->date_time_format, strtotime($lottery->ends));?></td>
				</tr>
				<tr>
					<td class="field-title">Valor Ticket:</td>
					<td><?php echo display_money($lottery->ticket_price);?></td>
				</tr>
				<tr>
					<td class="field-title">Tickets Vendidos:</td>
					<td><?php echo $lottery->buyed_tickets;?></td>
				</tr>				
				<tr>
					<td class="field-title">Máximo de Tickets:</td>
					<td><?php echo $lottery->max_tickets;?></td>
				</tr>
				<tr>
					<td class="field-title">Valor do Prêmio:</td>
					<td>
					<?php 
					if($lottery->type == 'fix') echo display_money($lottery->value_initial);
					else echo display_money($lottery->value_initial+($lottery->buyed_tickets*$lottery->ticket_price));
					?> (<?=$lottery->percent_emp?>% Tx. Adm.)
					</td>
				</tr>
			</table>
				</h5>
				
		<div class="row">
	<?php if($lottery->status != 'cancel'): ?>	
	<div class="col-md-6">
		<div class="panel">
			<div class="panel-heading p-10 border-bottom-orange">
				<h5 class="panel-title"><i class="icon-ticket position-left"></i> Meus Tickets</h5>
			</div>
			<div class="table-responsive">
				<table class="table table-xs data">
					<thead><tr>
						<th class="text-center" style="width: 10px">#</th>
						<th>Data</th>
					</tr></thead>
					<tbody>
					<?php foreach ($meustickets as $ticket): ?>
						<tr>
							<td class="text-center"><?=$ticket->id;?></td>
							<td><span class="hidden"><?=strtotime($ticket->date);?></span> <?=date($this->settings->date_format, strtotime($ticket->date));?> às <?=date($this->settings->date_time_format, strtotime($ticket->date));?></td>
						</tr>
					<?php endforeach;?>
					<?php if(empty($meustickets)) echo "<tr><td colspan=2>Nenhum ticket adquirido.</td></tr>"; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php if($lottery->status == 'paid'): ?>
	<div class="col-md-6">
		<div class="panel">
			<div class="panel-heading p-10 border-bottom-orange">
				<h5 class="panel-title"><i class="icon-gift position-left"></i> Ganhadores</h5>
			</div>
			<div class="table-responsive">
				<table class="table table-xs data">
					<thead><tr>
						<th class="text-center" style="width: 10px">#</th>
						<th>Ganhador</th>
						<th>Ticket</th>
						<th>Valor</th>
					</tr></thead>
					<tbody><?php foreach ($winners as $winner): 
					$namewinner = User::find_by_id($winner->winner_id);
					?>
						<tr>
							<td class="text-center"><?=$winner->position;?></td>
							<td><?=$namewinner->firstname;?> <?=$namewinner->lastname;?></td>
							<td><?=$winner->ticket_id;?></td>
							<td><?=display_money($winner->value_win);?></td>
						</tr>
					<?php endforeach; ?>
					<?php if(empty($winners)) echo "<tr><td colspan=4>Nenhum ganhador.</td></tr>"; ?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<?php endif; ?>
	<?php if($lottery->status == 'open'): ?>	
	<div class="col-md-6">
		<div class="panel">
			<div class="panel-heading p-10 border-bottom-orange">
				<h5 class="panel-title"><i class="icon-coins position-left"></i> Comprar Tickets</h5>
			</div>
			<?php
			
			if(($lottery->buyed_tickets >= $lottery->max_tickets) AND ($lottery->max_tickets != 0)) 
				echo "<h2><font color='red'>Tickets esgotados. :(</font></h2>";
			else {
			
			//if($contagemtickets >= $lottery->max_tickets_person) 
				?>
			<div class="table-responsive">
				<table class="table table-xs data">
					<thead>
					<tr>
						<th class="text-center" colspan="2">Você tem <?=$contagemtickets?> Tickets.</th>
					</tr>
					<tr>
						<th class="text-center" style="width:50%;">Tickets vendidos:</th>
						<th class="text-center"><?=$lottery->buyed_tickets?></th>
					</tr>
					<tr>
						<th class="text-center" style="width:50%;">Tickets restantes:</th>
						<th class="text-center">
						<?php if($lottery->max_tickets == 0): ?>
						Tickets ilimitados.
						<?php else: ?>
						<?=($lottery->max_tickets-$lottery->buyed_tickets)?>
						<?php endif; ?>
						</th>
					</tr>
					</thead>
					<tbody>
							<?php if(($lottery->max_tickets_person == 0) OR ($lottery->max_tickets_person > $contagemtickets)): ?>
						<tr>
							<?php if($lottery->max_tickets_person == 0): ?>
							<th class="text-center" colspan="2">Esta loteria não tem limite de compra de tickets.</th>
							<?php else: ?>
							<th class="text-center" colspan="2">Você ainda pode comprar <?=($lottery->max_tickets_person - $contagemtickets)?> Tickets.</th>
							<?php endif; ?>
						</tr>
						<tr>
							<td class="text-center" colspan="2">
							<form action="<?=site_url('backoffice/lotterys/view/'.$lottery->id);?>" method="post">
								<input type="number" name="amount" id="amount" class="form-control" min="0" placeholder="Ex: <?=rand(1, 10);?>" required style="width: 100px; display: inline;" />
								<input type="submit" class="btn btn-xs btn-primary" value="OK"/>
							</form>
							</td>
						</tr>
							<?php else: ?>
						<tr>
							<th class="text-center" colspan="2">Você já atingiu o limite de <?=$lottery->max_tickets_person?> tickets por participante.</th>
						</tr>
							<?php endif; ?>
					</tbody>
				</table>
			</div>
			<?php } ?>
		</div>
	</div>
	<?php endif; ?>
</div>
		
        </center>
    </div>
</div>
