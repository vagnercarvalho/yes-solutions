<?php

$invoices = Invoice::find_by_sql("SELECT COUNT(*) AS qtd FROM invoices i "
        . " INNER JOIN invoices_items ii ON (ii.invoice_id = i.id) "
        . " INNER JOIN products p ON (p.id = ii.plan_id) "
        . " WHERE i.status = 'paid' AND i.type = 'buy' AND i.user_id = {$this->user->id} AND p.reactivate = 0;")[0]->qtd;

if (($invoices > 0)) {

?>
<?php if (Invoice::count(array('conditions' => array('status = ? and user_id = ?', 'open', $this->user->id))) > 0) { ?>
<div class="alert alert-warning alert-styled-left">
	<span class="text-semibold">Atenção!</span> Você já tem pedido(s) em aberto! <a href="<?=base_url('backoffice/invoices/all');?>">Clique aqui para ver.</a>
</div>
<?php } ?>

<div class="panel ">
	<div class="panel-heading p-10 border-bottom-none">
		<h5 class="panel-title"><i class="icon-coins fa-fw position-left"></i> Reativação de Cadastro</h5>
	</div>

	<style>
    </style>

    <div class="panel-body " style="background-color:  transparent;">
        <form method="post" class="form-horizontal">

            <h1><b>Escolha o Pacote de Ativação</b></h1>
            <p>
                Clique sobre o pacote para selecioná-lo.
            </p>
            <table width="100%" cellspacing="0">
                <tr>
                    <td class="shadow">
                        <?php 
                        foreach ($produtos as $prod) { 
                            $foto = (empty($prod->foto) ?  "assets/images/sem-foto.jpg" : "uploads/{$prod->foto}");
                        ?>
                            <label>
                                <img src="<?php echo site_url($foto) ?>" id="product-<?= $prod->id; ?>" width="140" style="opacity: .5" class="plan_select" data-toggle="tooltip" title="<?= $prod->name; ?> - <?= $prod->description; ?>">
                                <input type="radio" name="produto" value="<?= $prod->id; ?>" style="display: none;" data-price="<?php echo $prod->price ?>" />
                                <br><center><b><?= display_money($prod->price); ?></b></center>
                            </label>
                        <?php } ?>
                    </td>
                </tr>
            </table>


            <hr>


            <script type="text/javascript">
                $(document).ready(function($) {
                    
                    $("*.plan_select").click(function() {
                        var plan_id = $(this).parent().find('input').val();
                        $(this).rotate({ angle: 0, animateTo:360 });
                        $("*.plan_select").css('opacity', '.5');
                        $(this).css('opacity', '1');
                    });
                    
                    
                    $("*.avatar_select").click(function() {
                        var avatar_id = $(this).parent().find('input').val();
                        document.getElementById('my_avatar').src = '<?= base_url('assets/images/avatar/'); ?>/' + avatar_id + '.png';
                        document.getElementById('my_avatar2').src = '<?= base_url('assets/images/avatar/'); ?>/' + avatar_id + '.png';
                        $(this).rotate({ angle: 0, animateTo: 360});
                        $("*.avatar_select").css('opacity', '.5');
                        $(this).css('opacity', '1');
                    });
                });
                
                
                function salvar() {
                    
                    let productId = $("input:radio[name='produto']:checked").val();
                    
                    if (productId  > 0) {
                    
                        $.ajax({
                            url: '<?php echo site_url('backoffice/recharge') ?>',
                            method: 'post',
                            dataType: 'json',
                            data: {
                                product: productId
                            },
                            success: function (json) {
                                try {
                                    if (json.success) {

                                        toastr["success"](json.message, 'Invoice!');
                                        setTimeout(function() {
                                            location = json.redirect;
                                        }, 3000);
                                    } else {
                                        toastr["error"](json.message, 'Erro ao criar invoice!');
                                    }
                                } catch (e) {
                                    toastr["error"](e, 'Erro ao criar invoice!');
                                }
                            }
                        });
                    } else {
                        toastr["error"]("Por favor, selecione um plano.", 'Erro ao criar invoice!');
                    }
                }
                
            </script>
            <script>
                $(document).ready(function () {
                    $('[data-toggle="tooltip"]').tooltip({html: true});
                });
            </script>

            <div class="text-right">
                <button type="button" class="btn btn-primary btn-labeled" onclick="salvar();">
                    <b><i class="icon-cart"></i></b>
                    Gerar pedido e Pagar
                </button>
            </div>
        </form>
    </div>
</div>

<?php } else {  ?>
<div class="alert alert-danger alert-styled-left">
	<span class="text-semibold">Atenção!</span> Você precisa adquirir um pacote antes de ser elegivel a reativar mensalmente sua conta. 
        <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-investment">Clique aqui para adquirir um pacote.</a>
</div>
<?php
}
?>