<?php

$photo = "";

if (empty($this->user->photo)) {
    if ($this->user->gender == "male") {
        $photo = site_url('assets/images/user_male-x.png');
    } else {
        $photo = site_url('assets/images/user_female-x.png');
    }
} else {
    $photo = site_url('uploads/' . $this->user->photo);
}

?>
<div class="panel panel-flat">
    <div class="panel-body">
        <form method="post" class="form-horizontal" enctype="multipart/form-data">
            
            
            <div class="row">
                <div class="col col-sm-4 texxt-center"> 
                    <img src="<?php echo $photo ?>" alt="" class="img-thumbnail" style="max-height: 320px; max-width: 320px;">
                </div>
                <div class="col col-sm-8">
                    <div class="form-group row">
                        <label for="sponsor" class="col-sm-12 control-label"><b>Patrocinador:</b></label>
                        <div class="col-sm-12"><input id="sponsor" class="form-control" type="text" value="<?= ($sponsor->firstname . ' ' . $sponsor->lastname); ?>" readonly="readonly" disabled/></div>
                    </div>
                    <div class="form-group row">
                        <label for="fullname" class="col-sm-12 control-label"><b>Nome completo:</b></label>
                        <div class="col-sm-12">
                            <input id="fullname" class="form-control" type="text" value="<?= ($this->user->firstname . ' ' . $this->user->lastname); ?>"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="photo" class="col-sm-12 control-label"><b>Foto do Perfil:</b></label>
                        <div class="col-sm-12"><input id="photo" name="photo" class="form-control" type="file" /></div>
                    </div>
                    
                    <div class="form-group row">
                        <label for="cpf" class="col-sm-12 control-label"><b>CPF:</b></label>
                        <div class="col-sm-12">
                            <input id="cpf" class="form-control cpf-mask" type="text" name="cpf" value="<?= $this->user->cpf; ?>"/>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col col-sm-6">
                    <div class="form-group row">
                        <label for="birthday" class="col-sm-12 control-label"><b>Data de nascimento:</b></label>
                        <div class="col-sm-12">
                            <input id="birthday" class="form-control date-mask" type="text" name="birthday" value="<?= date($this->settings->date_format, strtotime($this->user->birthday)); ?>"/>
                        </div>
                    </div>
                </div>
                <div class="col col-sm-6">
                    <div class="form-group row">
                        <label for="gender" class="col-sm-12 control-label"><b>Sexo:</b></label>
                        <div class="col-sm-12">
                            <select class="form-control" name="gender" required>
                                <option value="female" <?php if ($this->user->gender == 'female'): echo 'selected'; endif; ?>>Feminino</option>
                                <option value="male" <?php if ($this->user->gender == 'male'): echo 'selected';endif; ?>>Masculino</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col col-xs-12">
                    
                    <div class="form-group row">
                        <label for="phone" class="col-sm-12 control-label"><b>Telefone:</b></label>
                        <div class="col-sm-12">
                            <input id="phone" class="form-control phone-mask" type="text" name="phone" value="<?= $this->user->phone; ?>" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="mobilephone" class="col-sm-12 control-label"><b>Celular:</b></label>
                        <div class="col-sm-12">
                            <input id="mobilephone" class="form-control mobilephone-mask" type="text" name="mobilephone" value="<?= $this->user->mobilephone; ?>" />
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="email" class="col-sm-12 control-label"><b>E-mail:</b></label>
                        <div class="col-sm-12">
                            <input id="email" class="form-control" type="texto" value="<?= $this->user->email; ?>" data-validate="required" readonly="readonly" disabled/>
                        </div>
                    </div>
                    
                </div>
            </div>
            
            <div class="text-right">
                <button type="submit" class="btn btn-primary">Salvar alterações <i class="icon-floppy-disk position-right"></i></button>
            </div>
        </form>
    </div>
</div>