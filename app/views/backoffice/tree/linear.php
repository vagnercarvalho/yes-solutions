<?php
/**

 * Author:      Felipe Medeiros

 * File:        linear.php

 * Created in:  24/06/2016 - 17:04

 */
?>

<div class="panel">

	<div class="panel-heading border-bottom-primary-800">

		<h5 class="panel-title"><i class="icon-tree5 position-left"></i> Rede linear</h5>

	</div>


	<?=count_inlevels($this->user->id);?>


	<div class="table-responsive no-border">

		<table class="table table-xs data">

			<thead><tr>

				<th width="10">Nv.</th>

				<th>Nome</th>

				<th>E-mail</th>

				<th width="140">Celular</th>

				<th width="140">Esquerda</th>

				<th width="140">Direita</th>

				<th width="20">Status</th>

			</tr></thead>

			<tbody>

				<?=make_linear($this->user->id);?>

			</tbody>

		</table>

	</div>

</div>

<script type="text/javascript">

	$(document).ready(function () {

		$('.data').dataTable();

	})

</script>

