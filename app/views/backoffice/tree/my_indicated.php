<?php
/**
Felipe Medeiros
 * Author:      Felipe Medeiros

 * File:        my_indicated.php

 * Created in:  24/06/2016 - 14:55

 */
?>

<div class="panel">

	<div class="panel-heading border-bottom-primary-800">

		<h5 class="panel-title"><i class="icon-users4 position-left"></i> Meus indicados</h5>

	</div>

	<div class="table-responsive no-border">

		<table class="table table-xs data">

			<thead><tr>

				<th width="10">#</th>

				<th>Nome</th>

				<th>E-mail</th>

				<th width="140">Celular</th>


				<th width="140">Esquerda</th>
				<th width="140">Direita</th>

				<th width="20">Status</th>

			</tr></thead>

			<tbody>

				<?php foreach ($indicated as $user): ?><tr>

					<td class="text-center"><?=$user->id;?></td>

					<td><?=($user->firstname . ' ' . $user->lastname);?></td>

					<td><?=$user->email;?></td>

					<td class="text-center"><?=$user->mobilephone;?></td>
					<td class="text-center"><?=$user->pleft;?></td>
					<td class="text-center"><?=$user->pright;?></td>

					<td class="text-center"><span class="label label-<?php 
					if($user->banned == 'Y'):

						echo 'warning';

					elseif($user->status != 'active'):

						echo 'default';

					else:

						echo 'success';

					endif; ?>"><?php 
					
					if($user->banned == 'Y'):

						echo 'BLOQUEADO';

					elseif($user->status != 'active'):

						echo 'PENDENTE';

					else:

						echo 'ATIVO';

					endif; ?></span></td>

				</tr><?php endforeach; ?>

			</tbody>

		</table>

	</div>

</div>

<script type="text/javascript">

	$(document).ready(function () {

		$('.data').dataTable();

	})

</script>

