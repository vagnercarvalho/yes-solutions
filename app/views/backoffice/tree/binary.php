
<script src="https://d3js.org/d3.v5.js"></script>
<script src="https://d3js.org/d3-hierarchy.v1.min.js"></script>

<style>
    
    .tree-user-avatar {
        width: 80px; 
        height: 80px; 
        border: 4px solid #333;
    }
    
    /*Now the CSS*/
    * {margin: 0; padding: 0;}

    .tree {
        margin-bottom: 25px;
        padding-bottom: 25px;
    }
    
    .tree ul {
            padding-top: 20px; position: relative;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
    }

    .tree li {
            float: left; text-align: center;
            list-style-type: none;
            position: relative;
            padding: 20px 5px 0 5px;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
            
            
    }

    /*We will use ::before and ::after to draw the connectors*/

    .tree li::before, .tree li::after{
            content: '';
            position: absolute; top: 0; right: 50%;
            border-top: 1px solid #ccc;
            width: 50%; height: 20px;
    }
    .tree li::after{
            right: auto; left: 50%;
            border-left: 1px solid #ccc;
    }

    /*We need to remove left-right connectors from elements without 
    any siblings*/
    .tree li:only-child::after, .tree li:only-child::before {
            display: none;
    }

    /*Remove space from the top of single children*/
    .tree li:only-child{ padding-top: 0;}

    /*Remove left connector from first child and 
    right connector from last child*/
    .tree li:first-child::before, .tree li:last-child::after{
            border: 0 none;
    }
    /*Adding back the vertical connector to the last nodes*/
    .tree li:last-child::before{
            border-right: 1px solid #ccc;
            border-radius: 0 5px 0 0;
            -webkit-border-radius: 0 5px 0 0;
            -moz-border-radius: 0 5px 0 0;
    }
    .tree li:first-child::after{
            border-radius: 5px 0 0 0;
            -webkit-border-radius: 5px 0 0 0;
            -moz-border-radius: 5px 0 0 0;
    }

    /*Time to add downward connectors from parents*/
    .tree ul ul::before{
            content: '';
            position: absolute; top: 0; left: 50%;
            border-left: 1px solid #ccc;
            width: 0; height: 20px;
    }

    .tree li a{
            border: 1px solid #ccc;
            padding: 5px 10px;
            text-decoration: none;
            color: #666;
            font-family: arial, verdana, tahoma;
            font-size: 11px;
            display: inline-block;

            border-radius: 5px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;

            transition: all 0.5s;
            -webkit-transition: all 0.5s;
            -moz-transition: all 0.5s;
    }

    .tree li a span{
        margin-top: 2px;
        font-size: 10px;
        display: block;
        color: #ffffff;
        font-weight: bold;
    }
    
    .tree li a:hover span{
        color: #0000ff;
    }
    
    .tree li a span:hover{
        color: #0000ff;
    }
    .tree li a:hover{
        color: #0000ff;
    }
    

    .tree li a p{
        font-size: 10px;
        display: block;
        color: orange;
        font-weight: bold;
    }
    
    /*Time for some hover effects*/
    /*We will apply the hover effect the the lineage of the element also*/
    .tree li a:hover, .tree li a:hover+ul li a {
            background: #c8e4f8; color: #000; border: 1px solid #94a0b4;
    }
    /*Connector styles on hover*/
    .tree li a:hover+ul li::after, 
    .tree li a:hover+ul li::before, 
    .tree li a:hover+ul::before, 
    .tree li a:hover+ul ul::before{
            border-color:  #94a0b4;
    }
</style>

<div class="panel panel-default">
    <div class="panel-heading">
        <h5 class="panel-title"><i class="icon-tree7 position-left"></i> Árvore binária</h5>
    </div>
    <div class="panel-body">
        
        <div class="row">
            <div class="col col-xs-6 col-md-4 ">
                <div class="panel">
                    <div class="panel-body ">
                        <h4 class="text-center">Pontos Equipe Esquerda</h4>
                        <h2 class="text-center"><?php echo $top->pleft?></h2>
                        <div class="alert alert-info text-center" id="cadastros-perna-esquerda">
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="col col-xs-6 col-md-4 col-xs-offset-0 col-md-offset-4">
                <div class="panel">
                    <div class="panel-body ">
                        <h4 class="text-center">Pontos Equipe Direita</h4>
                        <h2 class="text-center"><?php echo $top->pright ?></h2>
                        <div class="alert alert-info text-center" id="cadastros-perna-direita">
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col col-xs-12 text-center" >
                <?php if ($top->id != $this->user->id) { ?>
                <a class="btn btn-primary" href="<?php echo site_url('backoffice/tree/binary/' . $this->user->id) ?>" >
                    Voltar Para O Topo
                </a>
                <?php } ?>
            </div>
        </div>
        
        <div class="row">
            
            
            <div class="col col-xs-12" id="map" style="overflow-x: scroll; padding-bottom: 50px;">

                <div class="tree" style="margin-left: -30px;">
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    
    $(document).ready(function () {
        
        gettree();
    });
    
    function gettree() {
        
        $.ajax({
            url: '<?php echo site_url('backoffice/tree/gettree') ?>',
            method: 'post',
            dataType: 'json',
            data: {
                top: <?php echo $top->id ?>
            },
            success: function (json) {
                try {
                    if (json.sucesso) {
                        
                        $(".tree").html(json.tree);
                        $("#cadastros-perna-esquerda").html(json.pessoasEsquerda + " Cadastro(s)");
                        $("#cadastros-perna-direita").html(json.pessoasDireita + " Cadastro(s)");
                        
                        $(".tree").css("width", (Math.pow(2, 4) * 76 ) + "px");
                    } else {
                        toastr["error"](json.mensagem, 'Erro ao criar invoice!');
                    }
                } catch (e) {
                    toastr["error"](e, 'Erro ao criar invoice!');
                }
            }
        });
        
    }
</script>