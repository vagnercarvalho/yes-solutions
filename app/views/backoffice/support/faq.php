<?php if (count($faqs) != 0): ?>
	<div class="panel-group panel-group-control panel-group-control-right content-group-lg" id="accordion-control-right">
		<?php foreach ($faqs as $faq): ?>
		<div class="panel panel-white">
			<div class="panel-heading">
				<h6 class="panel-title">
					<a class="collapsed" data-toggle="collapse" data-parent="#accordion-control-right" href="#accordion-<?=$faq->id;?>"><b>#<?=$faq->number;?> -</b> <?=$faq->title;?></a>
				</h6>
			</div>
			<div id="accordion-<?=$faq->id;?>" class="panel-collapse collapse">
				<div class="panel-body">
					<?=nl2br($faq->text);?>
				</div>
			</div>
		</div>
		<?php endforeach; ?>
	</div>
<?php else: ?>
	<div class="alert alert-warning alert-styled-left">
		<span class="text-semibold">Atenção!</span> No momento não existe nenhuma questão em nosso FAQ.
	</div>
<?php endif; ?>