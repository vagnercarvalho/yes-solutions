<?php
/**
 * Project:    mmn.dev
 * File:       ads.php
 * Author:     Felipe Medeiros
 * Createt at: 27/05/2016 - 20:48
 */
?>
<div class="panel">
	<div class="panel-heading p-10 border-bottom-none">
		<h5 class="panel-title"><i class="icon-flags3 position-left"></i> Material de Apoio</h5>
	</div>
<br><br>

<div class="table-responsive no-border">
<table class="table table-xs data">
<thead><tr>
<th class="text-center"><h2>Intro</h2></th>
<th class="text-center"><h2>Promo PT-BR 1</h2></th>
</tr></thead>
<tbody>
<tr>
<td class="text-center"><iframe width="426" height="240" src="https://www.youtube.com/embed/AfhbizRml3Y" frameborder="0" allowfullscreen></iframe></td>
<td class="text-center"><iframe width="426" height="240" src="https://www.youtube.com/embed/JrX6z_PFBEA" frameborder="0" allowfullscreen></iframe></td>
</tr>
<tr>
<td class="text-center">https://youtu.be/AfhbizRml3Y</td>
<td class="text-center">https://youtu.be/JrX6z_PFBE</td>
</tr>
</tbody>
</table>
</div>
<br><br>
</div>