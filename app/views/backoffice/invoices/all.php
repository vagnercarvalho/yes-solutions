<?php
/**
 * Project:    mmn.dev
 * File:       all.php
 * Author:     Felipe Medeiros
 * Createt at: 27/05/2016 - 20:48
 */
?>
<div class="panel">
<div class="panel-heading p-10 border-bottom-none">
<h5 class="panel-title"><i class="icon-list position-left"></i> Minhas Faturas</h5>
</div>
<div class="table-responsive no-border">
<table class="table table-xs data">
<thead><tr>
<th width="70px" class="text-center">#</th>
<th class="text-center">Categoria</th>
<th class="text-center">Criada em</th>
<th class="text-center">Pago em</th>
<th class="text-center">Valor</th>
<th class="text-center">Status</th>
<th class="text-center">Ação</th>
</tr></thead>
<tbody><?php foreach ($invoices as $invoice): ?>
<tr>
<td class="text-center"><?=$invoice->id;?></td>
<td class="text-center"><span class="label label-info">
<?php
if ($invoice->type == 'buy') {
	echo 'Adesão';
} elseif ($invoice->type == 'upgrade') {
	echo 'Upgrade';
} elseif ($invoice->type == 'monthly') {
	echo 'Mensalidade';
} elseif ($invoice->type == 'recharge') {
	echo 'Recarga';
}

?>
</span></td>
<td class="text-center"><span><span class="hidden"><?=strtotime($invoice->date);?></span> <?=date("d/m/Y", strtotime($invoice->date));?></span></td>
<td class="text-center"><span class="hidden"><?=strtotime($invoice->payment_date);?></span> <?php if ($invoice->status == 'paid') {
	echo date("d/m/Y", strtotime($invoice->payment_date));
} else {
	echo "-";
}

?></td>
<td class="text-center"><span><span class="hidden"><?=($invoice->sum - $invoice->discount);?></span> <?=display_money(($invoice->sum - $invoice->discount));?></span></td>
<td class="text-center"><span class="label <?php
if ($invoice->status == "paid") {
	echo 'label-success';
} elseif ($invoice->status == "open") {
	echo 'label-primary';
} elseif ($invoice->status == "canceled") {
	echo 'label-danger';
}

?>"><?=($invoice->status == 'open' ? 'Em aberto' : ($invoice->status == 'paid' ? 'Pago' : ($invoice->status == 'canceled' ? 'Cancelado' : '-')));?></span></td>
<td class="text-center">
<a href="<?=site_url('backoffice/invoices/view/' . $invoice->id);?>" class="btn btn-icon btn-xs btn-primary"><i class="icon-eye"></i></a>
</td>
</tr>
<?php endforeach;?></tbody>
</table>
</div>
</div>
<script type="text/javascript">
$(document).ready(function () {
$('.data').dataTable();
})
</script>