<?php

$imgGestor = base_url( (empty($qualification->foto) ? 'assets/images/sem-foto.jpg' : "uploads/{$qualification->foto}") );

$progressColor = "";
$progressNextLevel = $nextQualification->name;
$levelAtual = $qualification->name;

if ($this->user->points > 0) {
    $progress = 0.00;
} else {
    $progress = number_format(($nextQualification->points > 0 ? ($this->user->points / $nextQualification->points * 100) : 0), 2, ".", "");
}


$prograssLabel = "{$progress}%";

if ($progress < 30) {
    $progressColor = "red";
} else if ($progress < 70) {
    $progressColor = "orange";
} else  {
    $progressColor = "green";
}

?>

<link rel="stylesheet" href="<?= base_url('css/circle.css'); ?>">

<!-- BEGIN PANEL STATS -->
<img src="<?=base_url('assets/images/logo-backoffice.png');?>" style="max-width: 1px" />
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat">
            <div class="dashboard-stat-inner">
                <div class="dashboard-stat-front bg-danger-400">
                    <div class="visual">
                        <i class="icon-wallet"></i>
                    </div>
                    <div class="details">
                        <?php list($currency, $value) = explode(' ', display_money($this->user->balance));?>
                        <div class="number">
                            <?=display_money($this->user->balance);?>
                        </div>
                        <div class="desc">Saldo disponível</div>
                    </div>
                </div>
                <div class="dashboard-stat-back">
                    <a class="more" href="<?=site_url('backoffice/ewallet/extract');?>"><img src="<?=base_url('assets/images/logo-FZ.png');?>" /></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat">
            <div class="dashboard-stat-inner">
                <div class="dashboard-stat-front bg-primary-400">
                    <div class="visual">
                        <i class="icon-safe"></i>
                    </div>
                    <div class="details">
                      <div class="number">
                          <?=display_money($this->user->balance_reserved);?>
                      </div>
                      <div class="desc">Saldo reservado</div>
                    </div>
                </div>
                <div class="dashboard-stat-back">
                    <a class="more" href="<?=site_url('backoffice/ewallet/extract');?>"><img src="<?=base_url('assets/images/logo-FZ.png');?>"  /></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat">
            <div class="dashboard-stat-inner">
                <div class="dashboard-stat-front  bg-success-400">
                    <div class="visual">
                        <i class="icon-users"></i>
                    </div>
                    <div class="details">
                      <div class="number counter">
                          <?=$sponsored;?>
                      </div>
                      <div class="desc">Meus indicados</div>
                    </div>
                </div>
                <div class="dashboard-stat-back">
                    <a class="more" href="<?=site_url('backoffice/tree/my_indicated');?>"><img src="<?=base_url('assets/images/logo-FZ.png');?>" /></a>
                </div>
            </div>
        </div>
    </div>

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat">
            <div class="dashboard-stat-inner">
                <div class="dashboard-stat-front bg-info-400">
                    <div class="visual">
                        <i class="icon-users"></i>
                    </div>
                    <div class="details">
                      <div class="number counter">
                          <?=count_linear($this->user->id);?>
                      </div>
                      <div class="desc">
                          Total Rede
                      </div>
                    </div>
                </div>
                <div class="dashboard-stat-back">
                    <a class="more" href="<?=site_url('backoffice/tree/linear');?>"><img src="<?=base_url('assets/images/logo-FZ.png');?>" /></a>
                </div>
            </div>
        </div>
    </div>

</div>
<!-- END PANEL STATS -->







<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="panel" >
            <div class="panel-heading p-10 border-bottom-none">
                <h4 class="panel-title"><i class="icon-alert position-left"></i> Comunicado</h4>
            </div>
            <div class="panel-body" id="comunicados-panel" style="height: 220.8px; overflow-y: scroll;">
                <?=$alerthome?>
            </div>
        </div>
    </div>
    
    
    <div class="col col-xs-12 col-sm-12 col-md-6">
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-6 text-center">
                <img src="<?php echo site_url('assets/images/yescardblackcerto.png') ?>" style="max-width: 100%; max-height: 120.8px !important;" />
            </div>
            
            <!--
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="panel">
                    <div class="panel-heading p-10 border-bottom-none">
                        <h4 class="panel-title"><i class="icon-coins position-left"></i> Carteira</h4>
                    </div>
                    <div class="table-responsive no-border">
                        <table class="table table-xs table-striped" cellspacing="0" cellpadding="0">
                            <thead>
                                <tr>
                                    <th class="col-sm-2 text-center">Valor da Carteira</th>
                                    <th class="col-sm-2 text-center" style="    padding: 8px 19px !important;">Dividendo do dia: </th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <?=display_money($investido)?>
                                    </td>
                                    <td class="text-center">
                                        <?=display_money($diario)?>
                                    </td>
                                </tr>
                                <tr>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            -->
            <div class="col-xs-12 col-sm-12 col-md-6">
                <div class="panel">
                    <div class="panel-heading p-10 border-bottom-none">
                        <h4 class="panel-title"><i class="icon-user position-left"></i> Status</h4>
                    </div>
                    <div class="panel-body text-center">
                        <div class="btn btn-<?php
                        if ($this->user->status == 'active') {
                            echo 'success';
                        } else {
                            echo 'warning';
                        }
                        ?> btn-block btn-labeled">
                                 <?php
                                 if ($this->user->status == 'active') {
                                     echo '<b><i class="icon-check"></i></b> Ativo';
                                 } else {
                                     echo '<b><i class="icon-cross"></i></b> Inativo';
                                 }
                                 ?>
                        </div>
                        <!--<?php //if($this->user->ganhos >= $this->user->teto):  ?>-->
                        <!--<b><a href="javascript:void(0);" data-toggle="modal" data-target="#modal-investment">Adquira já!</a></b><br>-->
                        <!--Teto: <font color='red'><?= display_money($this->user->ganhos) ?> de <?//=display_money($this->user->teto)?></font>-->
                        <!--<?php //else:  ?>-->
                        <!--Teto: <font color='green'><?= display_money($this->user->ganhos) ?></font> de <font color='red'><?//=display_money($this->user->teto)?></font>-->
                        <!--<?php //endif;  ?>-->
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="panel">
                    <div class="panel-heading p-10 border-bottom-purple">
                        <h5 class="panel-title"><i class="icon-balance position-left"></i> Derramamento</h5>
                    </div>
                    <div class="panel-body text-center">
                        <form method="post" class="form-horizontal">
                            <select class="form-control" name="position" required onchange="this.form.submit()">
                                <option value="auto" <?php if ($this->user->position == 'auto'): echo 'selected';
                                    endif; ?>>Automático</option>
                                <option value="left" <?php if ($this->user->position == 'left'): echo 'selected';
                                    endif; ?>>Esquerda</option>
                                <option value="right" <?php if ($this->user->position == 'right'): echo 'selected';
                                    endif; ?>>Direita</option>
                            </select>
                        </form>
                    </div>
                </div>
            </div>
            
            
        </div>
    </div>
    
    
    
</div>





<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="panel">
            <div class="panel-heading p-10 border-bottom-none">
                <h4 class="panel-title"><i class="icon-users position-left"></i> Últimos Indicados</h4>
            </div>
            <div class="table-responsive no-border">
                <table class="table table-xs table-striped" cellspacing="0" cellpadding="0" style="height: 290.8px; ">
                    <thead>
                        <tr>
                            <th class="col-sm-2 text-center">Data</th>
                            <th class="col-sm-5">Nome</th>
                            <th class="col-sm-3 text-center">Celular</th>
                        </tr>
                    </thead>
                    <tbody style="overflow: hidden;">
                        <?php if (count($l_sponsored) <= 0): ?>
                        <tr>
                            <td colspan="3" class="text-center">Você ainda não indicou ninguém.</td>
                        </tr>
                        <?php else: ?>
                        <?php foreach ($l_sponsored as $user): ?>
                        <tr>
                            <td class="text-center"><small><?=date($this->settings->date_format, strtotime($user->create_date));?></small></td>
                            <td>
                                <?=($user->firstname . ' ' . $user->lastname);?>
                            </td>
                            <td class="text-center">
                                <?=$user->mobilephone;?>
                            </td>
                        </tr>
                        <?php endforeach;?>
                        <?php endif;?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    
    
        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="panel">
                <div class="panel-heading p-10 border-bottom-none">
                    <h4 class="panel-title"><i class="icon-coins position-left"></i> Qualificação</h4>
                </div>
                <div class="panel-body  text-center" style="height: 290px;">
                    <img src="<?php echo $imgGestor ?>" style="height: 160px !important; " /> 
                    <h4 style="display: block;"><?php echo $levelAtual ?></h4>
                </div>
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-3">
            <div class="panel">
                <div class="panel-heading p-10 border-bottom-none">
                    <h4 class="panel-title"><i class="icon-coins position-left"></i> Próxima Graduação</h4>
                </div>
                <div class="panel-body  text-center">
                    <div class="c100 p<?php echo number_format($progress, 0, ".", "") ?> big" style="font-size: 160px; margin-left: calc(50% - 80px);">
                        <span><?php echo $prograssLabel ?></span>
                        <div class="slice">
                            <div class="bar"></div>
                            <div class="fill"></div>
                        </div>
                    </div>
                    <h4 style="  display: block;"><?php echo $progressNextLevel ?></h4>
                    <h4 style="  display: block;">Acumulado: <?php echo $this->user->points ?></h4>
                </div>
            </div>
        </div>

    
</div>





<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12">
        <div class="panel panel-default">
            <div class="panel-body p-10">
                <div class="form-group">
                    <label style="display: block;">
<i class="icon-link2 position-left"></i> <b>Link de Indicação</b>
<a href="<?=site_url('backoffice/settings/link');?>" class="label label-success pull-right">Alterar</a>
<button onclick="copyToClipboard();" class="label label-info pull-right" style="margin-right: 5px;">Copiar</button>
</label>
                    <input class="form-control text-center" type="text" id="user-sponsor-link" value="<?=site_url('sponsor/' . (!$this->user->link ? $this->user->id : $this->user->link));?>" readonly onclick="this.select();" />
                </div>
            </div>
        </div>
    </div>
    
    
    
    <!--<div class="col-xs-12 col-sm-12 col-md-4">-->
    <!--    <div class="panel">-->
    <!--        <div class="panel-heading p-10 border-bottom-none">-->
    <!--            <h4 class="panel-title"><i class="icon-tree5 position-left"></i> Binário</h4>-->
    <!--        </div>-->
    <!--        <div class="table-responsive no-border">-->
    <!--            <table class="table table-xs table-striped" cellspacing="0" cellpadding="0">-->
    <!--                <thead>-->
    <!--                    <tr>-->
    <!--                        <th class="col-sm-2 text-center">Esq.</th>-->
    <!--                        <th class="col-sm-2 text-center">Dir.</th>-->
    <!--                        <th class="col-sm-2 text-center">%</th>-->
    <!--                    </tr>-->
    <!--                </thead>-->
    <!--                <tbody>-->
    <!--                    <tr>-->
    <!--                        <td class="text-center"><?//=$this->user->pleft;?></td>-->
    <!--                        <td class="text-center"><?//=$this->user->pright;?></td>-->
    <!--                        <?php// if($this->user->ganhos >= $this->user->teto): ?>-->
    <!--                        <td class="text-center">-</td>-->
    <!--                        <?php// else: ?>-->
    <!--                        <td class="text-center">15%</td>-->
    <!--                        <?php// endif; ?>-->
    <!--                    </tr>-->
    <!--                </tbody>-->
    <!--            </table>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <!--<div class="col-xs-12 col-sm-12 col-md-3">-->
    <!--    <div class="panel">-->
    <!--        <div class="panel-heading p-10 border-bottom-none">-->
    <!--            <h4 class="panel-title"><i class="icon-balance position-left"></i> Derramamento</h4>-->
    <!--        </div>-->
    <!--        <div class="panel-body text-center">-->
    <!--            <form method="post" class="form-horizontal">-->
    <!--                <div class="form-group row">-->
    <!--                    <select class="form-control" name="position" required>-->
    <!--                        <option value="auto" <?php// if ($this->user->position == 'auto'): echo 'selected'; endif;?>>Automático</option>-->
    <!--                        <option value="left" <?php// if ($this->user->position == 'left'): echo 'selected'; endif;?>>Esquerda</option>-->
    <!--                        <option value="right" <?php// if ($this->user->position == 'right'): echo 'selected'; endif;?>>Direita</option>-->
    <!--                    </select>-->
    <!--                </div>-->
    <!--                <div class="text-center">-->
    <!--                    <button type="submit" class="btn btn-primary">Salvar <i class="icon-floppy-disk position-right"></i></button>-->
    <!--                </div>-->
    <!--            </form>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
</div>



<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        <div class="panel">
            <div class="btcwdgt-chart"></div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="panel">
            <div class="btcwdgt-price" bw-cur="usd"></div>
        </div>
    </div>
</div>


<div class="row" style="margin-bottom:20px;">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <img style="    width: 100%;padding-bottom: 10px;" src="<?=base_url('images/Campanha01-site.jpg');?>" />
    </div>
    <div class="col-xs-12 col-sm-12 col-md-6">
        <img style="    width: 100%;" src="<?=base_url('images/Campanha02-site.jpg');?>" />
    </div>
    
    
</div>

<script src="https://demo.pixelcave.com/proui/js/vendor/jquery.min-3.6.js"></script>
<script src="https://demo.pixelcave.com/proui/js/plugins-3.8.js"></script>
<script src="<?=base_url('assets/js/charts/widgetsStats.js');?>"></script>

                <script>
                    $(function(){ WidgetsStats.init(); });
                    
                    $(document).ready(function () {
                        
                        // Initialize
                        initPanelScroll();
                    });
                    
                    function copyToClipboard() {
                        /* Get the text field */
                        var copyText = document.getElementById("user-sponsor-link");

                        /* Select the text field */
                        copyText.select();
                        copyText.setSelectionRange(0, 99999); /*For mobile devices*/

                        /* Copy the text inside the text field */
                        document.execCommand("copy");

                     }
                     
                     function initPanelScroll() {
                        $("#comunicados-panel").niceScroll({
                            mousescrollstep: 100,
                            cursorcolor: '#ccc',
                            cursorborder: '',
                            cursorwidth: 3,
                            hidecursordelay: 100,
                            autohidemode: 'scroll',
                            horizrailenabled: false,
                            preservenativescrolling: false,
                            railpadding: {
                                    right: 0.5,
                                    top: 1.5,
                                    bottom: 1.5
                            }
                        });
                    }

                    // Remove
                    function removePanelScroll() {
                            $(".sidebar-fixed .sidebar-content").getNiceScroll().remove();
                            $(".sidebar-fixed .sidebar-content").removeAttr('style').removeAttr('tabindex');
                    }

                
                // Remove scrollbar on mobile
                $(window).on('resize', function() {
                    setTimeout(function() {            
                        if($(window).width() <= 768) {

                            // Remove nicescroll on mobiles
                            removePanelScroll();
                        }
                        else {

                            // Init scrollbar
                            initPanelScroll();
                        }
                    }, 100);
                }).resize();

                    
                </script>
                <script>
                    // (function(b,i,t,C,O,I,N) {
                    // window.addEventListener('load',function() {
                    // if(b.getElementById(C))return;
                    // I=b.createElement(i),N=b.getElementsByTagName(i)[0];
                    // I.src=t;I.id=C;N.parentNode.insertBefore(I, N);
                    // },false)
                    // })(document,'script','https://widgets.bitcoin.com/widget.js','btcwdgt');
                </script>

                ?>

                