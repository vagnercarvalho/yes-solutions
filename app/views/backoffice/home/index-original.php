<!-- BEGIN PANEL STATS -->
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat bg-primary-800">
            <div class="visual">
                <i class="icon-wallet"></i>
            </div>
            <div class="details">
                <?php list($currency, $value) = explode(' ', display_money($this->user->balance)); ?>
                <div class="number"><?= display_money($this->user->balance); ?></div>
                <div class="desc">Saldo disponível</div>
            </div>
            <a class="more" href="<?= site_url('backoffice/ewallet/extract'); ?>">
                Ver balanço <i class="fa fa-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat bg-warning-800">
            <div class="visual">
                <i class="icon-safe"></i>
            </div>
            <div class="details">
                <div class="number"><?= display_money($this->user->balance_reserved); ?></div>
                <div class="desc">Saldo reservado</div>
            </div>
            <a class="more" href="<?= site_url('backoffice/ewallet/extract'); ?>">
                Ver balanço <i class="fa fa-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat bg-primary-800">
            <div class="visual">
                <i class="icon-users"></i>
            </div>
            <div class="details">
                <div class="number counter"><?= $sponsored; ?></div>
                <div class="desc">Meus indicados</div>
            </div>
            <a class="more" href="<?= site_url('backoffice/tree/my_indicated'); ?>">
                Ver indicados <i class="fa fa-arrow-right"></i>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat bg-warning-800">
            <div class="visual">
                <i class="icon-tree6"></i>
            </div>
            <div class="details">
                <div class="number counter"><?= count_linear($this->user->id); ?></div>
                <div class="desc">
                    Meus Indicados
                </div>
            </div>
            <a class="more" href="<?= site_url('backoffice/tree/linear'); ?>">
                Ver indicados <i class="fa fa-arrow-right"></i>
            </a>
        </div>
    </div>
</div>
<!-- END PANEL STATS -->

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-9">
        <div class="panel">
            <div class="panel-heading p-10 border-bottom-none">
                <h5 class="panel-title"><i class="icon-alert position-left"></i> Comunicado</h5>
            </div>
            <div class="panel-body">
                <?= $alerthome ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="panel">
            <div class="panel-heading p-10 border-bottom-none">
                <h5 class="panel-title"><i class="icon-coins position-left"></i> Solicitar Saque</h5>
            </div>
            <div class="panel-body text-center">
                <a href="<?= site_url('backoffice/ewallet/withdrawal'); ?>" class="btn btn-primary btn-block btn-labeled">
                    <b><i class="icon-plus3"></i></b>
                    Solicitar saque
                </a>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-6">
        <div class="panel">
            <div class="panel-heading p-10 border-bottom-none">
                <h5 class="panel-title"><i class="icon-users position-left"></i> Últimos Indicados</h5>
            </div>
            <div class="table-responsive no-border">
                <table class="table table-xs table-striped" cellspacing="0" cellpadding="0">
                    <thead><tr>
                            <th class="col-sm-2 text-center">Data</th>
                            <th class="col-sm-5">Nome</th>
                            <th class="col-sm-3 text-center">Celular</th>
                        </tr></thead>
                    <tbody>
                        <?php if (count($l_sponsored) <= 0): ?>
                            <tr><td colspan="3" class="text-center">Você ainda não indicou ninguém.</td></tr>
                        <?php else: ?>
                            <?php foreach ($l_sponsored as $user): ?>
                                <tr>
                                    <td class="text-center"><small><?= date($this->settings->date_format, strtotime($user->create_date)); ?></small></td>
                                    <td><?= ($user->firstname . ' ' . $user->lastname); ?></td>
                                    <td class="text-center"><?= $user->mobilephone; ?></td>
                                </tr>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="panel">
            <div class="panel-heading p-10 border-bottom-none">
                <h5 class="panel-title"><i class="icon-coins position-left"></i> Investimento</h5>
            </div>
            <div class="table-responsive no-border">
                <table class="table table-xs table-striped" cellspacing="0" cellpadding="0">
                    <thead><tr>
                            <th class="col-sm-2 text-center">Valor Investido</th>
                            <th class="col-sm-2 text-center" style="    padding: 8px 19px !important;">Rentabilidade do Mês</th>
                        </tr></thead>
                    <tbody>
                        <tr>
                            <td class="text-center"><?= display_money($investido) ?></td>
                            <td class="text-center"><?= display_money($diario) ?></td>
                        </tr>
                        <tr>
                        <!--<td colspan="2" class="text-center">Pago de Seg à Sex.</td>-->
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-3">
        <div class="panel">
            <div class="panel-heading p-10 border-bottom-none">
                <h5 class="panel-title"><i class="icon-user position-left"></i> Status</h5>
            </div>
            <div class="panel-body text-center">
                <div class="btn btn-<?php
                if ($this->user->status == 'active') {
                    echo 'success';
                } else {
                    echo 'warning';
                }
                ?> btn-block btn-labeled">
                     <?php
                     if ($this->user->status == 'active') {
                         echo '<b><i class="icon-check"></i></b> Ativo';
                     } else {
                         echo '<b><i class="icon-cross"></i></b> Inativo';
                     }
                     ?>
                </div>
                <!--<?php //if($this->user->ganhos >= $this->user->teto):  ?>-->
                <!--<b><a href="javascript:void(0);" data-toggle="modal" data-target="#modal-investment">Adquira já!</a></b><br>-->
                <!--Teto: <font color='red'><?= display_money($this->user->ganhos) ?> de <?//=display_money($this->user->teto)?></font>-->
                <!--<?php //else:  ?>-->
                <!--Teto: <font color='green'><?= display_money($this->user->ganhos) ?></font> de <font color='red'><?//=display_money($this->user->teto)?></font>-->
                <!--<?php //endif;  ?>-->
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-12" >
        <div class="panel panel-default">
            <div class="panel-body p-10">
                <div class="form-group">
                    <label style="display: block;">
                        <i class="icon-link2 position-left"></i> <b>Link de Indicação</b>
                        <a href="<?= site_url('backoffice/settings/link'); ?>" class="label label-success pull-right">Alterar</a>
                    </label>
                    <input class="form-control text-center" type="text" value="<?= site_url('sponsor/' . (!$this->user->link ? $this->user->id : $this->user->link)); ?>" readonly onclick="this.select();" />
                </div>
            </div>
        </div>
    </div>
    <!--<div class="col-xs-12 col-sm-12 col-md-4">-->
    <!--    <div class="panel">-->
    <!--        <div class="panel-heading p-10 border-bottom-none">-->
    <!--            <h5 class="panel-title"><i class="icon-tree5 position-left"></i> Binário</h5>-->
    <!--        </div>-->
    <!--        <div class="table-responsive no-border">-->
    <!--            <table class="table table-xs table-striped" cellspacing="0" cellpadding="0">-->
    <!--                <thead>-->
    <!--                    <tr>-->
    <!--                        <th class="col-sm-2 text-center">Esq.</th>-->
    <!--                        <th class="col-sm-2 text-center">Dir.</th>-->
    <!--                        <th class="col-sm-2 text-center">%</th>-->
    <!--                    </tr>-->
    <!--                </thead>-->
    <!--                <tbody>-->
    <!--                    <tr>-->
    <!--                        <td class="text-center"><?//=$this->user->pleft;?></td>-->
    <!--                        <td class="text-center"><?//=$this->user->pright;?></td>-->
    <!--                        <?php // if($this->user->ganhos >= $this->user->teto):  ?>-->
    <!--                        <td class="text-center">-</td>-->
    <!--                        <?php // else:  ?>-->
    <!--                        <td class="text-center">15%</td>-->
    <!--                        <?php // endif;  ?>-->
    <!--                    </tr>-->
    <!--                </tbody>-->
    <!--            </table>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
    <!--<div class="col-xs-12 col-sm-12 col-md-3">-->
    <!--    <div class="panel">-->
    <!--        <div class="panel-heading p-10 border-bottom-none">-->
    <!--            <h5 class="panel-title"><i class="icon-balance position-left"></i> Derramamento</h5>-->
    <!--        </div>-->
    <!--        <div class="panel-body text-center">-->
    <!--            <form method="post" class="form-horizontal">-->
    <!--                <div class="form-group row">-->
    <!--                    <select class="form-control" name="position" required>-->
    <!--                        <option value="auto" <?php // if ($this->user->position == 'auto'): echo 'selected'; endif; ?>>Automático</option>-->
    <!--                        <option value="left" <?php // if ($this->user->position == 'left'): echo 'selected'; endif; ?>>Esquerda</option>-->
    <!--                        <option value="right" <?php // if ($this->user->position == 'right'): echo 'selected'; endif; ?>>Direita</option>-->
    <!--                    </select>-->
    <!--                </div>-->
    <!--                <div class="text-center">-->
    <!--                    <button type="submit" class="btn btn-primary">Salvar <i class="icon-floppy-disk position-right"></i></button>-->
    <!--                </div>-->
    <!--            </form>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</div>-->
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        <div class="panel">
            <div class="btcwdgt-chart"></div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="panel">
            <div class="btcwdgt-price" bw-cur="usd"></div>
        </div>
    </div>
</div>
<div class="row" style="margin-bottom:20px;">
    <div class="col-xs-6 col-sm-6 col-md-6">
        <img style="    width: 100%;padding-bottom: 10px;" src="<?= base_url('images/Campanha01-site.jpg'); ?>" />
        <img style="    width: 100%;" src="<?= base_url('images/Campanha02-site.jpg'); ?>" />
    </div>
    <div class="col-xs-6 col-sm-6 col-md-6" style="background-color: white;">
        <div style="background-color: #fff;">
            <div class="widget-advanced widget-advanced-alt">
                <div style="height: auto;
                     min-height: 150px;padding: 15px;position: relative;overflow: hidden;background-color: #234058;text-align: center;">
                    <h3 style="color: #fff;float: left !important;text-align: left;margin-bottom: 15px;font-weight: 300;font-size: 24px;margin-top: 20px;line-height: 1.1;">
                        <strong style="font-weight: 600;">Histórico de Rendimentos</strong><br>
                        <small style="color: #eee;font-weight: 300;font-size: 65%;line-height: 1;">2019</small>
                    </h3>
                    <div id="chart-widget1" style="height: 360px;padding: 0px; position: relative;"><canvas class="flot-base" width="787" height="360" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 360px;"></canvas><canvas class="flot-overlay" width="787" height="360" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 787px; height: 360px;"></canvas></div>
                </div>
                <div style="padding: 15px;position: relative;">
                    <div style="text-align: center;">
                        <div class="col-xs-6" style="font-family: Roboto,Helvetica Neue,Helvetica,Arial,sans-serif;
                             font-size: 13px;    ">
                            <h3 class="animation-hatch"><strong>3,4% a.m.</strong><br><small style="font-weight: 500 !important;">Estimativa de Rendimentos CRONOS</small></h3>
                        </div>
                        <div class="col-xs-6">
                            <h3 class="animation-hatch"><strong>0,32% a.m.</strong><br><small style="font-weight: 500 !important;">Rendimentos da Poupança</small></h3>
                        </div>
                        <!--<div class="col-xs-4">-->
                        <!--<h3 class="animation-hatch">$<strong>31.230</strong><br><small>Ganhos</small></h3>-->
                        <!--</div>-->
                    </div>
                </div>

                <script src="https://demo.pixelcave.com/proui/js/vendor/jquery.min-3.6.js"></script>
                <script src="https://demo.pixelcave.com/proui/js/vendor/bootstrap.min-3.6.js"></script>
                <script src="https://demo.pixelcave.com/proui/js/plugins-3.8.js"></script>
                <script src="<?= base_url('assets/js/charts/widgetsStats.js'); ?>"></script>

                <script>$(function () {
        WidgetsStats.init();
    });</script>
                <script>
                // (function(b,i,t,C,O,I,N) {
                // window.addEventListener('load',function() {
                // if(b.getElementById(C))return;
                // I=b.createElement(i),N=b.getElementsByTagName(i)[0];
                // I.src=t;I.id=C;N.parentNode.insertBefore(I, N);
                // },false)
                // })(document,'script','https://widgets.bitcoin.com/widget.js','btcwdgt');
                </script>


                <?php
                /* if($this->user->first_recharge == 'N'): ?>
                  <!-- Modal -->
                  <div class="modal fade" id="FirstRecharge" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                  <div class="modal-content">
                  <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel"><?=$this->settings->company_name?></h4>
                  </div>
                  <div class="modal-body">
                  Olá! Seja muito bem-vindo(a)! Parece que você é novo no PacmoneyCoin.<br><br>
                  Temos algo muito bom para você! Como forma de incentivo aos novos jogadores estamos realizando uma promoção onde você faz sua primeira recarga e automaticamente ganha um saldo especial bônus de <font color='green'>100%</font>  do valor recarregado!<br><br>
                  Seja muito bem-vindo(a) ao mundo dos bitcoins! Esperamos que tenha um bom jogo!
                  <br><br>
                  Atenciosamente,
                  <br><br>
                  Equipe PacMoneyCoin.<br>
                  www.pacmoneycoin.com
                  <img src=''>
                  </div>
                  <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                  </div>
                  </div>
                  </div>
                  </div>
                  <script type="text/javascript">
                  $(document).ready(function () {
                  $('#FirstRecharge').modal('show');
                  });
                  </script>
                  <?php endif; */?>