<?php
$act_uri = $this->uri->segment(2, 0);
$lastsec = $this->uri->total_segments();
$act_uri_submenu = $this->uri->segment($lastsec);
$act_uri_submenu = $this->uri->segment(3, 0);
if (!$act_uri) {
    $act_uri = 'home';
}

while (is_numeric($act_uri_submenu)):
    --$lastsec;
    $act_uri_submenu = $this->uri->segment($lastsec);
endwhile;

$last_activity = User::find_by_id($this->user->id);
$last_activity->last_activity = date("Y-m-d H:i:s");
$last_activity->save();

$availableProducts = Product::find_by_sql("SELECT * FROM `products` WHERE status = 1 AND virtual_store = 0");

$habilitarLoja = InvoicesItem::find_by_sql("SELECT COUNT(*) AS count FROM invoices i 
                                            INNER JOIN invoices_items ii ON (i.id = ii.invoice_id)
                                            WHERE i.status = 'paid' AND i.type = 'buy' AND i.user_id = {$this->user->id} AND ii.profit > 0 AND ii.value > 0;")[0]->count > 0;
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" type="image/png" href="<?= base_url('favicon.png'); ?>" />
        <title>Escritório Virtual | <?= $this->settings->company_name; ?></title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/icons/icomoon/styles.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/icons/fontawesome/styles.min.css'); ?>" rel="stylesheet" type="text/css">
        <link href="<?= base_url('assets/css/bootstrap.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/core.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/components.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/colors.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/binario.css'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?= base_url('assets/css/custom.css'); ?>" rel="stylesheet" type="text/css" />
        <!-- /global stylesheets -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?= base_url('assets/js/core/libraries/jquery.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/core/libraries/bootstrap.min.js'); ?>"></script>
        <script type="text/javascript">
            var base_url = '<?= base_url(); ?>';
        </script>
        <!-- /core JS files -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    </head>
    <body class="navbar-top" style="background-repeat: repeat-y !important; background-image: URL(<?php echo site_url('assets/images/backgrounds/background_interno.jpg')?>)">
        <!-- Main navbar -->
        <div class="navbar navbar-default navbar-fixed-top header-highlight">
            <div class="navbar-header">
                <a class="navbar-brand" href="<?= site_url('backoffice/home'); ?>"  style="background-color: #000000 !important;">
                    <img src="<?= base_url('assets/images/logo_header.png'); ?>" style="height: 45px !important; margin-top: -6px !important;" />
                </a>
                <ul class="nav navbar-nav pull-right visible-xs-block">
                    <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-user"></i></a></li>
                    <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
            </div>
            <div class="navbar-collapse collapse" style="border-color: transparent;" id="navbar-mobile">
                <ul class="nav navbar-nav">
                    <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown dropdown-user">
                        <a class="dropdown-toggle" data-toggle="dropdown">
                            <img src="<?= base_url('assets/images/user_' . $this->user->gender . '.png'); ?>" id="my_avatar" /></span>
                            <span><?= ($this->user->firstname . ' ' . $this->user->lastname); ?></span>
                            <i class="caret"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <li><a href="<?= site_url('backoffice/settings/profile'); ?>"><i class="icon-profile"></i> Perfil</a></li>
                            <?php /* <li><a href="<?=site_url('backoffice/settings/avatar');?>"><i class="icon-user"></i> Avatar</a></li> */ ?>
                            <li><a href="<?= site_url('backoffice/settings/link'); ?>"><i class="icon-link2"></i> Link de indicação</a></li>
                            <li><a href="<?= site_url('backoffice/settings/address'); ?>"><i class="icon-location4"></i> Endereço</a></li>
                            
                            <?php /*
                            <li><a href="<?= site_url('backoffice/settings/bank'); ?>"><i class="icon-library2"></i> Conta bancária</a></li>
                             * 
                             */
                            ?>
                            <li><a href="<?= site_url('backoffice/settings/bitcoin'); ?>"><i class="icon-coins"></i> Carteira bitcoin</a></li>
                            <li><a href="<?= site_url('backoffice/settings/password'); ?>"><i class="icon-lock"></i> Alterar senha</a></li>
                            <li class="divider"></li>
                            <li><a href="<?= site_url('backoffice/logout'); ?>"><i class="icon-switch2"></i> Logout</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        <!-- /main navbar -->
        <!-- Page container -->
        <div class="page-container">
            <!-- Page content -->
            <div class="page-content">
                <!-- Main sidebar -->
                <div class="sidebar sidebar-main sidebar-fixed">
                    <div class="sidebar-content">
                        <!-- User menu -->
                        <div class="sidebar-user">
                            <div class="category-content">
                                <div class="media">
                                    <?php /*
                                      <a href="<?=site_url('backoffice/settings/avatar');?>" class="media-left"><img src="<?=base_url('assets/images/user_' . $this->user->gender . '.png');?>" class="img-circle img-sm" alt="" id="my_avatar2"></a>
                                     */ ?>
                                    <a href="#" class="media-left"><img src="<?= base_url('assets/images/user_' . $this->user->gender . '.png'); ?>" class="img-circle img-sm" alt="" id="my_avatar2"></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold"><?= ($this->user->firstname . ' ' . $this->user->lastname); ?>
                                            <br/> ID: #<?= ($this->user->id); ?></span>
                                    </div>
                                    <div class="media-right media-middle">
                                        <ul class="icons-list">
                                            <li>
                                                <a href="<?= site_url('backoffice/settings'); ?>"><i class="icon-cog3"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /user menu -->
                        <!-- Main navigation -->
                        <div class="sidebar-category sidebar-category-visible">
                            <div class="category-content no-padding">
                                <ul class="navigation navigation-main navigation-accordion">
                                    <!-- Main -->
                                    <li class="navigation-header"><span>Menu principal</span> <i class="icon-menu" title="Menu principal"></i></li>
                                    <li class="<?php if ($act_uri == 'home') { echo "active"; } ?>">
                                        <a href="<?= site_url('backoffice/home'); ?>">
                                            <i class="icon-home4"></i>
                                            <span>Home</span>
                                        </a>
                                    </li>
                                    <li class="">
                                        <a href="<?=site_url('sponsor/' . (!$this->user->link ? $this->user->id : $this->user->link));?>" target="_NEW_CAD">
                                            <i class="icon-user"></i>
                                            <span>Cadastrar </span>
                                        </a>
                                    </li>
                                    
                                    <li class="<?php echo ($act_uri == 'settings' ? "active" : ""); ?>">
                                        <a href="#" class="has-ul ">
                                            <i class="icon-credit-card"></i>
                                            <span>Meus Dados</span>
                                        </a>
                                    
                                        <ul class="hidden-ul" style="display: none;">
                                            
                                            <li class="<?php echo ($act_uri == 'settings' && $act_uri_submenu == 'profile' ? "active" : "" ) ?>">
                                                <a href="<?= site_url('backoffice/settings/profile'); ?>"><i class="icon-profile"></i> Perfil</a>
                                            </li>
                                            <li class="<?php echo ($act_uri == 'settings' && $act_uri_submenu == 'extract' ? "link" : "" ) ?>">
                                                <a href="<?= site_url('backoffice/settings/link'); ?>"><i class="icon-link2"></i> Link de indicação</a>
                                            </li>
                                            <li class="<?php echo ($act_uri == 'settings' && $act_uri_submenu == 'extract' ? "address" : "" ) ?>">
                                                <a href="<?= site_url('backoffice/settings/address'); ?>"><i class="icon-location4"></i> Endereço</a>
                                            </li>
                                            <li class="<?php echo ($act_uri == 'settings' && $act_uri_submenu == 'extract' ? "bitcoin" : "" ) ?>">
                                                <a href="<?= site_url('backoffice/settings/bitcoin'); ?>"><i class="icon-coins"></i> Carteira bitcoin</a>
                                            </li>
                                            <li class="<?php echo ($act_uri == 'settings' && $act_uri_submenu == 'extract' ? "password" : "" ) ?>">
                                                <a href="<?= site_url('backoffice/settings/password'); ?>"><i class="icon-lock"></i> Alterar senha</a>
                                            </li>
                                            
                                        </ul>
                                    </li>
                                    
                                    
                                    <li class="<?php if ($act_uri == 'announcements') { echo "active"; } ?>">
                                        <a href="<?= site_url('backoffice/announcements'); ?>">
                                            <i class="icon-info3"></i>
                                            <span>Comunicados <span class="pull-right-container">
                                                    <small class="label bg-blue pull-right" data-toggle="tooltip" title="Comunicados"><?= $this->contcomu ?></small>
                                                </span></span>
                                        </a>
                                    </li>
                                    <li class="<?php if ($act_uri == 'recharge') { echo "active"; } ?>">
                                        <a href="javascript:void(0);" data-toggle="modal" data-target="#modal-investment">
                                            <i class="icon-coins"></i>
                                            <span>Adesão para Produtor</span>
                                        </a>
                                    </li>
                                    
                                    <li class="<?php if ($act_uri == 'recharge') { echo "active"; } ?>">
                                        <a href="<?=site_url('backoffice/recharge');?>">
                                            <i class="icon-coins"></i>
                                            <span>Ativação Mensal</span>
                                        </a>
                                    </li>
                                    
                                    <?php
                                    if ($habilitarLoja || $this->user->id == 1) {
                                    ?>
                                    <li class="<?php if ($act_uri == 'virtualstore') { echo "active"; } ?>">
                                        <a href="<?=site_url('backoffice/virtualstore');?>">
                                            <i class="icon-cart"></i>
                                            <span>Loja Virtual</span>
                                        </a>
                                    </li>
                                    <?php
                                    }
                                    ?>
                                    
                                    <li class="<?php if ($act_uri == 'invoices') { echo "active"; } ?>">
                                        <a href="<?= site_url('backoffice/invoices'); ?>">
                                            <i class="icon-file-text"></i>
                                            <span>Faturas</span>
                                        </a>
                                    </li>
                                    <li class="<?php if ($act_uri == 'tree') { echo "active"; } ?>">
                                        <a href="#" class="has-ul">
                                            <i class="icon-tree7"></i>
                                            <span>Minha Rede</span>
                                        </a>
                                        <ul class="hidden-ul" style="display: none;">
                                            <li class="<?php if ($act_uri == 'tree' && $act_uri_submenu == 'my_indicated') { echo "active"; } ?>">
                                                <a href="<?= site_url('backoffice/tree/my_indicated'); ?>">Diretos</a>
                                            </li>
                                            <li class="<?php if ($act_uri == 'tree' && $act_uri_submenu == 'linear') { echo "active"; } ?>">
                                                <a href="<?= site_url('backoffice/tree/linear'); ?>">Indiretos</a>
                                            </li>
                                            <li class="<?php if ($act_uri == 'tree' && $act_uri_submenu == 'binary') { echo "active"; } ?>">
                                                <a href="<?= site_url('backoffice/tree/binary'); ?>">Rede Binária</a>
                                            </li>
                                        </ul>
                                    </li>
                                    
                                    
                                    
                                    <li class="<?php  echo ($act_uri == 'ewallet' ? "active" : ""); ?>">
                                        <a href="#" class="has-ul ">
                                            <i class="icon-library2"></i>
                                            <span>Financeiro</span>
                                        </a>
                                        <ul class="hidden-ul">
                                            <li class="<?php if ($act_uri == 'ewallet' && $act_uri_submenu == 'extract') { echo "active"; } ?>">
                                                <a href="<?= site_url('backoffice/ewallet/extract'); ?>">Extrato</a>
                                            </li>
                                            <li class="<?php if ($act_uri == 'ewallet' && ($act_uri_submenu == 'withdrawal' OR $act_uri_submenu == 'view')) { echo "active"; } ?>">
                                                <a href="<?= site_url('backoffice/ewallet/withdrawal'); ?>">Saques</a>
                                            </li>
                                            <?php if ($this->settings->lock_transfer != 'Y') { ?>
                                                <li class="<?php if ($act_uri == 'ewallet' && $act_uri_submenu == 'transfer') { echo "active"; } ?>">
                                                    <a href="<?= site_url('backoffice/ewallet/transfer'); ?>">Transferir Saldo</a>
                                                </li>
                                            <?php } ?>
                                            <?php if ($this->settings->lock_payout != 'Y') { ?>
                                                <li class="<?php if ($act_uri == 'ewallet' && $act_uri_submenu == 'payout') { echo "active"; } ?>">
                                                    <a href="<?= site_url('backoffice/ewallet/payout'); ?>">Pagar Faturas</a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                    
                                    <li class="<?php //echo ($act_uri == 'statistics' ? "active" : "" ) ?>">
                                        <?php /* 
                                        <a href="<?= site_url('backoffice/statistics'); ?>">
                                        */ ?>
                                        <a href="#">
                                            
                                            <i class="icon-graph"></i>
                                            <span>Estatísticas</span>
                                        </a>
                                    </li>
                                    
                                    
                                    <li class="<?php //echo  ($act_uri == 'support' && $act_uri_submenu == 'ads' ? "active" : "" ) ?>">
                                        <?php /* 
                                        <a href="<?= site_url('backoffice/support/ads'); ?>">
                                        */ ?>
                                        <a href="#">
                                        
                                            <i class="icon-flag3"></i>
                                            <span>Material de apoio</span>
                                        </a>
                                    </li>
                                    
                                    <li class="<?php //echo ($act_uri == 'support' && $act_uri_submenu == 'faq' ? "active" : "") ?>">
                                        <?php /* 
                                        <a href="<?= site_url('backoffice/support/faq'); ?>">
                                        */ ?>
                                        <a href="#">
                                            <i class="icon-headset"></i>
                                            <span>F.A.Q</span>
                                        </a>
                                    </li>
                                    <!-- /main -->
                                </ul>
                            </div>
                        </div>
                        <!-- /main navigation -->
                    </div>
                </div>
                <!-- /main sidebar -->
                <!-- Main content -->
                <div class="content-wrapper ">


                    <div style="border-top: 1px solid #eaedf1; position: relative;
                         height: 102px;
                         overflow: hidden;
                         border-top-color: #222;">
                        <div style="z-index: 200;
                             position: absolute;
                             top: 0;
                             left: 0;
                             right: 0;
                             color: #fff;
                             background-color: #00000052;">
                            <div style="/*margin-left: -15px;
                                margin-right: -15px;*/">
                                <div style="width: 50%;    position: relative;
                                     min-height: 1px;
                                     padding-left: 15px;
                                     padding-right: 15px;">
                                    <h1>Bem-vindo(a) &agrave; <strong><?= ($this->user->firstname . ' ' . $this->user->lastname); ?></strong><br><small><?= $this->breadcrumbs->show(); ?>
                                        </small></h1>
                                </div>
                                <!--<div style="    position: relative;-->
                                <!--        min-height: 1px;-->
                                <!--        padding-left: 15px;-->
                                <!--        padding-right: 15px;">-->
                                <!--    <div style="text-align: center;">-->
                                <!--        <div class="col-xs-4 col-sm-3">-->
                                <!--        <h2 class="animation-hatch">-->
                                <!--        $<strong>93.7k</strong><br>-->
                                <!--        <small><i class="fa fa-thumbs-o-up"></i> Great</small>-->
                                <!--        </h2>-->
                                <!--        </div>-->
                                <!--        <div class="col-xs-4 col-sm-3">-->
                                <!--        <h2 class="animation-hatch">-->
                                <!--        <strong>167k</strong><br>-->
                                <!--        <small><i class="fa fa-heart-o"></i> Likes</small>-->
                                <!--        </h2>-->
                                <!--        </div>-->
                                <!--        <div class="col-xs-4 col-sm-3">-->
                                <!--        <h2 class="animation-hatch">-->
                                <!--        <strong>101</strong><br>-->
                                <!--        <small><i class="fa fa-calendar-o"></i> Events</small>-->
                                <!--        </h2>-->
                                <!--        </div>-->
                                <!--        <div class="col-sm-3 hidden-xs">-->
                                <!--        <h2 class="animation-hatch">-->
                                <!--        <strong>27° C</strong><br>-->
                                <!--        <small><i class="fa fa-map-marker"></i> Sydney</small>-->
                                <!--        </h2>-->
                                <!--        </div>-->
                                <!--    </div>-->
                                <!--</div>-->
                            </div>
                        </div>
                        <!--
                        <img src="<?= site_url('images/dashboard_header.jpg'); ?>" alt="header image" class="animation-pulseSlow">
                        -->
                    </div>


                    <!-- Page header -->
                    <div class="page-header panel noPrint no-border-top no-border-left no-border-right border-bottom-lg" style="padding-bottom: 0;">
                        <div class="page-header-content">
                            <!--<div class="page-title">-->
                            <!--<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?//=$module_name;?></span><?//=($page_name ? ' - ' . $page_name : '');?></h4>-->
                            <!--</div>-->
<?php if ($header_button != FALSE): ?><div class="heading-elements">
    <?= $header_button; ?>
                                </div><?php endif; ?>
                        </div>
                    </div>
                    <!-- /page header -->


                    <!-- Content area -->
                    <div class="content">
                                                    <?= $yield; ?>
                    </div>
                    <!-- /content area -->
                </div>
                <!-- /main content -->
                
                
                
                
                
                

                <div class="modal fade" id="modal-investment">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header bg-primary">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">
                                        <i class="icon-x icon-fw"></i>
                                    </span>
                                </button>
                                <h4 class="modal-title">
                                    Adquirir Produto
                                </h4>
                            </div>
                            <form id="f-eWallet-investment" action="<?= site_url('backoffice/recharge'); ?>" onsubmit="return false;" method="post">
                                <div class="modal-body">
                                        
                                        
                                            
                                            <?php 
                                            $i = 0;
                                            $total = 0;
                                            foreach ($availableProducts as $produtoDisponivel) {
                                                
                                                $i++;
                                                $total++;
                                                
                                                if ($i == 1) {
                                                    ?>
                                                    <div class="row">
                                                    <?php
                                                }
                                                
                                                $foto = (empty($produtoDisponivel->foto) ?  "assets/images/sem-foto.jpg" : "uploads/{$produtoDisponivel->foto}");
                                                ?>
                                                <div class="col col-md-4 col-xs-12 text-center">
                                                
                                                    <div class="form-check">
                                                        <label class="form-check-label" for="product-<?php echo $produtoDisponivel->id ?>">
                                                            <img src="<?= site_url($foto); ?>" class="rounded" alt="..." style="width: 100% !important;">
                                                            <label>
                                                                <input class="form-check-input" type="radio" name="product" data-price="<?php echo $produtoDisponivel->price ?>"  
                                                                       data-points="<?php echo $produtoDisponivel->bonus_points ?>" data-bonus="<?php echo $produtoDisponivel->bonus_indicacao_direta ?>" data-bonus-type="<?php echo $produtoDisponivel->tipo_bonus_indicacao_direta ?>"
                                                                       id="product-<?php echo $produtoDisponivel->id ?>" value="<?php echo $produtoDisponivel->id ?>">
                                                                <?php echo $produtoDisponivel->name ?>
                                                            </label>
                                                        </label>
                                                    </div>
                                                </div>
                                                <?php
                                                
                                                if ($i == 3 || $total == sizeof($availableProducts)) {
                                                    $i = 0;
                                                    ?>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            
                                        
                                    <div class="form-group"  style="color: #666666;">

                                        <p>Seus dados selecionados:</p>
                                        <ul class="clist clist-angle">
                                            <li>Valor do pacote: R$ <b id="invested-value"></b>.</li>
                                            <li>Bônus de indicação direta: <b id="invested-bonus"></b>.</li>
                                            <li>Pontuação binária: <b id="invested-points"></b> Pontos.</li>
                                        </ul>
                                        
                                    </div>
                                <div class="modal-footer">
                                    <input type="submit" class="btn btn-success" value="Comprar" />
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>


            </div>
            <!-- /page content -->
        </div>
        <!-- /page container -->

        <!-- Core JS files -->
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/loaders/pace.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/loaders/blockui.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/notifications/sweet_alert.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/notifications/toastr.js'); ?>"></script>
        <!-- /core JS files -->
        <!-- Theme JS files -->
        <script type="text/javascript" src="<?= base_url('assets/js/core/libraries/jquery_ui/interactions.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/core/libraries/jquery_ui/widgets.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/core/libraries/jquery_ui/effects.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/extensions/mousewheel.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/ui/nicescroll.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/notifications/toastr.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/tables/datatables/datatables.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/visualization/d3/d3.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/visualization/d3/d3_tooltip.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/counterup/waypoints.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/counterup/jquery.counterup.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/meiomask/jquery.meio.mask.min.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/plugins/priceformat/jquery.price_format.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/jqueryrotate.js'); ?>"></script>

        <script>
            jQuery.fn.bstooltip = jQuery.fn.tooltip;
            jQuery.fn.bspopover = jQuery.fn.popover;
        </script>
        <script type="text/javascript" src="<?= base_url('assets/js/core/app.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/e-wallet.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/core/medeiros.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/core/cep.js'); ?>"></script>
        <script type="text/javascript" src="<?= base_url('assets/js/layout_fixed_custom.js'); ?>"></script>
        <!-- /theme JS files -->
        <script>
    
    
    

                                            (function(i, s, o, g, r, a, m){i['GoogleAnalyticsObject'] = r; i[r] = i[r] || function(){
                                            (i[r].q = i[r].q || []).push(arguments)}, i[r].l = 1 * new Date(); a = s.createElement(o),
                                                    m = s.getElementsByTagName(o)[0]; a.async = 1; a.src = g; m.parentNode.insertBefore(a, m)
                                            })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
                                            ga('create', 'UA-39808340-8', 'auto');
                                            ga('send', 'pageview');
        </script>
        <script type="text/javascript">
            window.block_form = function (form, status) {
            if (status)
                    $('input, textarea, button', form).attr('disabled', true);
            else
                    $('input, textarea, button', form).removeAttr('disabled');
            };
            $.extend($.fn.dataTable.defaults, {
            autoWidth: false,
                    dom: '<"datatable-header"fl><"datatable-scroll"tr><"datatable-footer"ip>',
                    info: false,
                    iDisplayLength: 10,
                    bLengthChange: false,
                    language: {
                    sEmptyTable: "Nenhum registro encontrado",
                            sInfo: "Exibindo _START_ até _END_ de _TOTAL_ registros",
                            sInfoEmpty: "Exibindo 0 até 0 de 0 registros",
                            sInfoFiltered: "(Filtrados de _MAX_ registros)",
                            sInfoPostFix: "",
                            sInfoThousands: ".",
                            sLengthMenu: "_MENU_",
                            sLoadingRecords: "Carregando...",
                            sProcessing: '<i class="fa fa-spinner fa-pulse fa-4x fa-fw"></i>',
                            sZeroRecords: "Nenhum registro encontrado",
                            sSearch: "",
                            oPaginate: {
                            sNext: '<i class=" icon-arrow-right15"></i>',
                                    sPrevious: '<i class=" icon-arrow-left15"></i>',
                                    sFirst: "Primeiro",
                                    sLast: "Último"
                            },
                            oAria: {
                            sSortAscending: ": Ordenar colunas de forma ascendente",
                                    sSortDescending: ": Ordenar colunas de forma descendente"
                            }
                    }
            });
            $.extend($.fn.dataTable.ext.classes, {
            sFilterInput:	"form-control",
                    sLengthSelect:	"form-control"
            });
            $(document).ready(function() {
            $('.counter').counterUp({
            delay: 100,
                    time: 1200
            });
            $('.date-mask').setMask('99/99/9999');
            $('.cpf-mask').setMask('999.999.999-99');
            $('.cep-mask').setMask('99999-999');
            $('.phone-mask').setMask('(99) 9999-9999');
            $('.mobilephone-mask').setMask('(99) 99999-9999');
            $('.money-mask').priceFormat({
            prefix: '',
<?php
switch ($this->settings->money_format) {
    case 1:
        ?>
                    centsSeparator: '.',
                            thousandsSeparator: ','
        <?php
        break;
    case 2:
        ?>
                    centsSeparator: ',',
                            thousandsSeparator: '.'
        <?php
        break;
    case 3:
        ?>
                    centsSeparator: '.',
                            thousandsSeparator: ''
        <?php
        break;
    case 4:
        ?>
                    centsSeparator: ',',
                            thousandsSeparator: ''
        <?php
        break;
    default:
        ?>
                    centsSeparator: '.',
                            thousandsSeparator: ','
        <?php
        break;
}
?>
            });
<?php
$message = $message ? $message : $this->session->flashdata('message');
if ($message):
    $title = $message['title'] ? $message['title'] : FALSE;
    $text = $message['text'] ? $message['text'] : 'Erro desconhecido! Contate o administrador do sistema.';
    $type = $message['type'] ? $message['type'] : 'error';
    echo 'toastr["' . $type . '"]("' . $text . '", ' . (!$title ? 'false' : '"' . $title . '"') . ')';
endif;
?>
            });
            
            
            $(document).ready(function () {
                
                $("input:radio[name='product']").change(function () {
                    showProfit();
                });
                
            });
            
            function showProfit() {
                
                var input = $("input:radio[name='product']:checked");
                
                
                let price = 0.0;
                let points = 0;
                let bonus = 0.0;
                let type = 'p';
                
                if (typeof input !== 'undefined') {
                    price = parseFloat($(input).attr("data-price"));
                    points = parseInt($(input).attr("data-points"));
                    bonus = parseFloat($(input).attr("data-bonus"));
                    type = $(input).attr("data-bonus-type");
                }
                
                $("#invested-value").html(price.toFixed(2).replace('.', ','));
                $("#invested-points").html(points);
                $("#invested-bonus").html((type === 'r' ? "R$ " : "Pts ") + bonus.toFixed(2).replace('.', ','));
            }
            
            
        </script>
    </body>
</html>