<?php
class Qualifications extends MY_Controller {
    
    
    public function __construct() {
        parent::__construct();
        if (!$this->admin)
                redirect('admin/login');

        $this->params['module_name']	= 'Qualificações';
        $this->breadcrumbs->push($this->params['module_name'], '/admin/qualifications');
    }
	
    public function index() { 
        
        $this->params['page_name']	= 'Qualificações';
        $this->breadcrumbs->push($this->params['page_name'], '/admin/qualifications/index');
        $this->content_view = 'admin/qualifications/all';
        
        $this->params['qualifications']	= Product::find_by_sql("SELECT * FROM qualifications ORDER BY pos;");
    }
    
    
    
    public function edit($codigo = 0) {
        
        if ($codigo > 0 && is_numeric($codigo)) {
            $qualification = Qualification::find_by_id($codigo);
            
            if (empty($qualification->foto)) {
                $qualification->foto = "assets/images/sem-foto.jpg";
            } else {
                $qualification->foto = "uploads/{$qualification->foto}";
            }
            
            $this->params['qualification'] = $qualification;
        } else {
            $qualification =  new Qualification();
            $qualification->foto = "assets/images/sem-foto.jpg";
            $this->params['qualification'] = $qualification;
        }
        
        $this->params['page_name']	= 'Qualificações';
        $this->breadcrumbs->push($this->params['page_name'], '/admin/qualifications/edit');
        $this->content_view = 'admin/qualifications/edit';
    }
    
    
    
    public function save() {
       
        try {
            $data = $this->input->post();
            $data['teto']  = grava_money($data['teto'], 2);
            
            $config['upload_path']          = 'uploads/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 1024*150;
            $this->load->library('upload', $config);
                
            if ($this->upload->do_upload('foto')) {
                $uploadData = array('upload_data' => $this->upload->data());
                
                $data['foto'] = $uploadData["upload_data"]["file_name"];
            } else {
                $data['foto'] = "";
            }
            
            
            if (empty($data["name"])) {
                throw new \Exception("É necessário informar o nome da qualificação!");
            } 
            
            if (!is_numeric($data["pos"]) || !$data["pos"] > 0) {
                throw new \Exception("É necessário informar a posição da qualificação!");
            }  
            
            if (!is_numeric($data["points"]) || !$data["points"] > -1) {
                throw new \Exception("É necessário informar a quantidade de pontos da qualificação!");
            }  
            
            if (!is_numeric($data["teto"]) || !$data["teto"] > 0) {
                throw new \Exception("É necessário informar o teto de ganhos da qualificação!");
            } 
            
            //exit(print_r($data));
            if (isset($data["codigo"]) && $data["codigo"] > 0) {
                
                $qualification = Qualification::find_by_id($data["codigo"]);
                
                $qualification->name = $data["name"];
                $qualification->pos = $data["pos"];
                $qualification->points = $data["points"];
                $qualification->teto = $data["teto"];
                $qualification->foto = (empty($data["foto"]) ? $qualification->foto : $data["foto"]);
                $model = $qualification->save();
                
                if (!$model) {
                    throw new \Exception("Falha ao salvar a qualificação!");
                } 
                
            } else {
                unset($data["codigo"]);
            
                $model = Qualification::create($data);
                
                if (!$model) {
                    throw new \Exception("Falha ao salvar a qualificação!");
                } 
            }
            
            
            $json["mensagem"] = "Qualificação salva com sucesso!";
            $json["sucesso"] = true;
        } catch (\Exception $ex) {
            $json["mensagem"] = $ex->getMessage();
            $json["sucesso"] = false;
        }
        
        exit(json_encode($json));
    }
    
    
    
    public function delete($id = FALSE) {
        if (!$id || !is_numeric($id) || !($qualification = Qualification::find_by_id($id)))
            redirect('admin/qualifications/index');

        if ($qualification->delete())
            $this->session->set_flashdata('message', ['text' => 'Qualificação excluida!', 'type' => 'success']);
        else
            $this->session->set_flashdata('message', ['text' => 'Houve algum problema!', 'type' => 'error']);

        redirect('admin/qualifications/index');
    }

}
