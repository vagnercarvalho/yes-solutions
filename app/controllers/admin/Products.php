<?php
class Products extends MY_Controller {
    
    
    public function __construct() {
        parent::__construct();
        if (!$this->admin)
                redirect('admin/login');

        $this->params['module_name']	= 'Products';
        $this->breadcrumbs->push($this->params['module_name'], '/admin/products');
    }
	
    public function index() { 
        
        $this->params['page_name']	= 'Produtos';
        $this->breadcrumbs->push($this->params['page_name'], '/admin/products/index');
        $this->content_view = 'admin/products/products';
        
        $this->params['products']	= Product::all(Array("conditions" => "virtual_store = 0"));
    }
    
    
    
    public function edit($codigo = 0) {
        
        if ($codigo > 0 && is_numeric($codigo)) {
            $product = Product::find_by_id($codigo);
            
            if (empty($product->foto)) {
                $product->foto = "assets/images/sem-foto.jpg";
            } else {
                $product->foto = "uploads/{$product->foto}";
            }
            
            $this->params['produto'] = $product;
        } else {
            $product =  new Product();
            $product->foto = "assets/images/sem-foto.jpg";
            $this->params['produto'] = $product;
        }
        
        $this->params['page_name']	= 'Produtos';
        $this->breadcrumbs->push($this->params['page_name'], '/admin/products/edit');
        $this->content_view = 'admin/products/edit';
    }
    
    
    
    public function save() {
       
        try {
            $data = $this->input->post();
            
            $data['price']  = str_replace("," , "" , $data['price']);
            $data['bonus_indicacao_direta'] = str_replace("," , "" , ($data['bonus_indicacao_direta']));
            $data['bonus_indicacao_level_2'] = str_replace("," , "" , ($data['bonus_indicacao_level_2']));
            $data['bonus_indicacao_level_3'] = str_replace("," , "" , ($data['bonus_indicacao_level_3']));
            $data['bonus_indicacao_level_4'] = str_replace("," , "" , ($data['bonus_indicacao_level_4']));
            $data['bonus_indicacao_level_5'] = str_replace("," , "" , ($data['bonus_indicacao_level_5']));
            $data['bonus_indicacao_level_6'] = str_replace("," , "" , ($data['bonus_indicacao_level_6']));
            
            
            $config['upload_path']          = 'uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size']             = 1024*150;
            $this->load->library('upload', $config);
                
            if ($this->upload->do_upload('foto')) {
                $uploadData = array('upload_data' => $this->upload->data());
                
                $data['foto'] = $uploadData["upload_data"]["file_name"];
            } else {
                $data['foto'] = "";
            }
            
            if (empty($data["name"])) {
                throw new \Exception("É necessário informar o nome do produto.");
            }
            
            $data["virtual_store"] = 0;
            
              
            //exit(print_r($data));
            if (isset($data["codigo"]) && $data["codigo"] > 0) {
                
                $produto = Product::find_by_id($data["codigo"]);
                
                $produto->name = $data["name"];
                $produto->description = $data["description"];
                $produto->characteristics = $data["characteristics"];
                $produto->status = $data["status"];
                $produto->price = $data["price"];
                $produto->binario_percentual_payment = $data["binario_percentual_payment"];
                $produto->bonus_points = $data["bonus_points"];
                $produto->bonus_indicacao_direta = $data["bonus_indicacao_direta"];
                $produto->bonus_indicacao_level_2 = $data["bonus_indicacao_level_2"];
                $produto->bonus_indicacao_level_3 = $data["bonus_indicacao_level_3"];
                $produto->bonus_indicacao_level_4 = $data["bonus_indicacao_level_4"];
                $produto->bonus_indicacao_level_5 = $data["bonus_indicacao_level_5"];
                $produto->bonus_indicacao_level_6 = $data["bonus_indicacao_level_6"];
                $produto->tipo_bonus_indicacao_direta = $data["tipo_bonus_indicacao_direta"];
                $produto->reactivate = $data["reactivate"];
                $produto->virtual_store = $data["virtual_store"];
                $produto->foto = (empty($data["foto"]) ? $produto->foto : $data["foto"]);
                
                $model = $produto->save();
                
                if (!$model) {
                    throw new \Exception("Falha ao salvar o produto!");
                } 
                
            } else {
                unset($data["codigo"]);
            
                $data["date_created"] = date("Y-m-d H:i:s");
                
                $model = Product::create($data);
                
                if (!$model) {
                    throw new \Exception("Falha ao salvar o produto!");
                } 
            }
            
            
            $json["mensagem"] = "Produto salvo com sucesso!";
            $json["sucesso"] = true;
        } catch (\Exception $ex) {
            $json["mensagem"] = $ex->getMessage();
            $json["sucesso"] = false;
        }
        
        exit(json_encode($json));
    }
}
