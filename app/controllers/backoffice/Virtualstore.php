<?php


class VirtualStore extends MY_Controller {

    public function __construct() {
        parent::__construct();
        if (!$this->user)
            redirect('backoffice/login');

        $this->params['module_name'] = 'Loja Virtual';
        $this->breadcrumbs->push($this->params['module_name'], '/backoffice/virtualstore');
    }

    public function index($page = 1) {

        
        $habilitarLoja = InvoicesItem::find_by_sql("SELECT COUNT(*) AS count FROM invoices i 
                                            INNER JOIN invoices_items ii ON (i.id = ii.invoice_id)
                                            WHERE i.status = 'paid' AND i.type = 'buy' AND i.user_id = {$this->user->id} AND ii.profit > 0 AND ii.value > 0;")[0]->count > 0;

                                            
        if (!$habilitarLoja && $this->user->id != 1) {
            redirect('backoffice/home');
        }
                                            
        if ($page < 1) {
            $page = 1;
        }

        $limit = " LIMIT 25 ";
        $offset = " OFFSET " . (($page - 1) * 25);

        $totalProdutos = Product::find_by_sql("SELECT COUNT(*) AS count FROM products WHERE status > 0 AND virtual_store > 0; ")[0]->count;
        $this->params['total'] = $totalProdutos;
        $this->params['pagina'] = $page;
        $this->params['totalPaginas'] = ceil($totalProdutos / 25);
        $this->params['produtos'] = Product::find_by_sql("SELECT * FROM products WHERE status > 0 AND virtual_store > 0  ORDER BY id DESC {$limit} {$offset};");

        $invoices = Invoice::find_by_sql("SELECT * from invoices WHERE status = 'open' AND type = 'buy' AND user_id = {$this->user->id} AND virtual_store = 1; ");
        if (sizeof($invoices) > 0) {
            $invoiceId = $invoices[0]->id;
            
            $totalProducts = InvoicesItem::find_by_sql("SELECT COUNT(*) AS qtd FROM invoices_items WHERE invoice_id = {$invoiceId};");
            $qtdProdutos = $totalProducts[0]->qtd;
            $this->params['qtdProdutos'] = $qtdProdutos;
        } else {
            $this->params['qtdProdutos'] = 0;
        }
        
        $this->params['page_name'] = 'Loja Virtual';
        $this->breadcrumbs->push($this->params['page_name'], '/backoffice/virtualstore');
        $this->content_view = 'backoffice/virtualstore/index';

    }

}
