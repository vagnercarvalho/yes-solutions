<?php
/**
 * Project:    mmn.dev
 * File:       Support.php
 * Author:     Felipe Medeiros
 * Createt at: 09/06/2016 - 06:41
 */
class Support extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->user)
			redirect('backoffice/login');

		$this->params['module_name'] = 'Suporte';
		$this->breadcrumbs->push($this->params['module_name'], '/backoffice/support');
	}
	public function faq()
	{
		$this->params['page_name'] = 'F.A.Q';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/support/faq');
		$this->content_view	= 'backoffice/support/faq';

		$this->params['faqs']	= Faq::all([
			'order' => 'number asc'
		]);
	}
	public function ads()
	{
		$this->params['page_name'] = 'Material de Apoio';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/support/ads');
		$this->content_view	= 'backoffice/support/ads';

	}
}