<?php
/**
 * Author:      Felipe Medeiros
 * File:        Tree.php
 * Created in:  24/06/2016 - 14:46
 */
class Tree extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->user)
			redirect('backoffice/login');

		$this->params['module_name'] = 'Rede';
		$this->breadcrumbs->push($this->params['module_name'], '/backoffice/tree');
	}
	public function index() { redirect('backoffice/tree/my_indicated'); }
	public function my_indicated()
	{
		$this->params['page_name'] = 'Meus indicados';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/tree/my_indicated');
		$this->content_view	= 'backoffice/tree/my_indicated';

		$this->params['indicated']	= User::all(['conditions' => ['enroller = ?', $this->user->id]]);
	}
	public function linear()
	{
		$this->params['page_name'] = 'Rede linear';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/tree/linear');
		$this->content_view	= 'backoffice/tree/linear';
	}
	public function binary($id = FALSE)
	{
	    if (!$id)	$id = $this->user->id;
		if (!($top = User::find(['conditions' => ['id = ?', $id]])))
			redirect('backoffice/tree/binary');

		$this->params['page_name'] = 'Árvore Binária';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/tree/binary');
		$this->content_view	= 'backoffice/tree/binary';
		
		$this->params['top'] = $top;
	}
        
        
        
        public function getBinaryTree() {
            
            try {
                
                $data = $this->input->post();
                
                $trees = Array();
                $nextLevel = Array();
                ob_start();
                if (isset($data["next"]) && sizeof($data["next"])) {
                    foreach ($data["next"] as $idUser) {
                        $user = User::find_by_id($idUser);
                        $treeResult = Binarytree::find_by_sql("SELECT * FROM binarytrees WHERE user_id = {$idUser};");
                        
                        $tree = null;
                        if (sizeof($treeResult) > 0) {
                            $tree = $treeResult[0];
                            
                            $nextLevel[] = $tree->uleft;
                            $nextLevel[] = $tree->uright;
                        }
                        ?>

                    
                        <table class="table table-borderless">

                            <tbody>
                                <tr  class="tree-tr">
                                    <div class="row tree-row">
                                        <div class="col col-xs-12">
                                            <?php
                                            $foto = (empty($user->photo) ?  ($user->gender == "male" ? 'assets/images/user_male-x.png' : 'assets/images/user_female-x.png') : "uploads/{$user->photo}" );
                                            ?>
                                            <img src="<?php echo site_url($foto); ?>" class="img-circle tree-user-avatar" alt="<?php echo "{$user->firstname}-{$user->lastname}" ?>">
                                        </div>
                                    </div>
                                    <div class="row tree-row">
                                        <div class="col col-xs-5 text-right tree-col ">
                                            Pontos: <?php echo $user->pleft?>. <br>
                                        </div>
                                        <div class="col col-xs-2 text-center tree-col ">
                                            <hr class="tree-vertical-line" >
                                        </div>
                                        <div class="col col-xs-5 text-left tree-col ">
                                            Pontos: <?php echo $user->pright ?> <br>
                                        </div>
                                    </div>
                                    <div class="row tree-row">
                                        <div class="col col-xs-12 text-center tree-col ">
                                            <hr class="tree-horizontal-line" >
                                        </div>
                                        <div class="col col-xs-6">
                                            <div class="row tree-row">
                                                <div class="col col-xs-2 col-xs-offset-5 col-md-2 col-md-offset-5 text-center ">
                                                    <hr class="tree-vertical-line" >
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-xs-6">
                                            <div class="row tree-row">
                                                <div class="col col-xs-2 col-xs-offset-5 col-md-2 col-md-offset-5 text-center ">
                                                    <hr class="tree-vertical-line" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </tr>

                                <tr class="tree-tr">

                                    <td class="tree-td" colspan="2" <?php echo ($tree != null && $tree->uleft > 0 ? "id='container-tree-{$tree->uleft}'" : "") ?>>
                                        
                                    </td>
                                    <td  class="tree-td" colspan="2" <?php echo ($tree != null && $tree->uright > 0 ? "id='container-tree-{$tree->uright}'" : "") ?>>

                                    </td>
                                </tr>

                            </tbody>

                        </table>
                    
                        <?php
                        $html = ob_get_contents();
                        ob_end_clean();
                        
                        
                        $trees[] = Array(
                            "id" => "container-tree-{$user->id}",
                            "content" => $html
                        );
                    }
                }
                
                $json["tree"] = $trees;
                $json["next"] = $nextLevel;
                $json["sucesso"] = true;
            } catch (\Exception $ex) {
                $json["sucesso"] = false;
                $json["mensagem"] = $ex->getMessage();
            }
            exit(json_encode($json));
        }
        
        public function gettree() {
            try {
                
                $data = $this->input->post();
                $topo = (isset($data["top"]) ? $data["top"] : $this->user->id);
                
                $userTopo = User::find_by_id($topo);
                
                $treeResult = Binarytree::find_by_sql("SELECT * FROM binarytrees WHERE user_id = {$topo};");
                $uleft = 0;
                $uright = 0;
                if (sizeof($treeResult) > 0) {
                    $tree = $treeResult[0];
                    $uleft = $tree->uleft;
                    $uright = $tree->uright;
                }

                $level = 1;
                $pessoas = Array("pessoasEsquerda" => 0, "pessoasDireita" => 0, "total" => 0, "maxLevel" => 1);
                ob_start();
                ?>
                <ul>
                    <li >
                        <a href="#">
                            <?php
                            $foto = (empty($userTopo->photo) ?  ($userTopo->gender == "male" ? 'assets/images/user_male.png' : 'assets/images/user_female.png') : "uploads/{$userTopo->photo}" );
                            ?>
                            <img src="<?php echo site_url($foto); ?>" class="img-circle tree-user-avatar" alt="<?php echo "{$userTopo->firstname}-{$userTopo->lastname}" ?>">
                            <br>
                            <span><?php echo $userTopo->firstname ?></span>
                            <p>
                                PE  |  PD <br>
                                <?php echo $userTopo->pleft ?> | <?php echo $userTopo->pright ?>
                            </p>
                        </a>

                        <?php 
                        if ($uleft > 0 || $uright > 0) {
                            $pessoas = $this->getNextLevel($uleft, $uright, $level);
                        } else {
                            $this->addEmptyLevel(0, 3);
                        }
                        ?>

                    </li>
                </ul>
                <?php
                $html = ob_get_contents();
                ob_end_clean();
            
            
                $json["tree"] = $html;                
                $json["maxLevel"] = $pessoas["maxLevel"];
                $json["pessoasEsquerda"] = $pessoas["pessoasEsquerda"];
                $json["pessoasDireita"] = $pessoas["pessoasDireita"];
                $json["sucesso"] = true;
            } catch (\Exception $ex) {
                $json["sucesso"] = false;
                $json["mensagem"] = $ex->getMessage();
            }
            exit(json_encode($json));
        }


        private function getNextLevel($userIdLeft, $userIdRight, $level) {
            $level++;
            $maxLevel = 1;
            $cadastrosEsquerda = 0;
            $cadastrosDireita = 0;
            
            ?>
            <ul>
                <li>
                    
                    <a href="<?php echo site_url('backoffice/tree/binary/' . $userIdLeft) ?>">
                    <?php 
                    
                    $foto = "assets/images/user_inactive.png";
                    $uleft = 0;
                    $uright = 0;
                    $name = "";
                    $pleft = 0;
                    $pright = 0;
                    if ($userIdLeft > 0) {
                        $cadastrosEsquerda++;
                        $userLeft = User::find_by_id($userIdLeft);
                        $treeResultLeft = Binarytree::find_by_sql("SELECT * FROM binarytrees WHERE user_id = {$userIdLeft};");

                        $name = $userLeft->firstname;
                        $pleft = $userLeft->pleft;
                        $pright = $userLeft->pright;
                        if (sizeof($treeResultLeft) > 0) {
                            $tree = $treeResultLeft[0];
                            $uleft = $tree->uleft;
                            $uright = $tree->uright;
                        }
                        $foto = (empty($userLeft->photo) ?  ($userLeft->gender == "male" ? 'assets/images/user_male.png' : 'assets/images/user_female.png') : "uploads/{$userLeft->photo}" );
                    }
                    ?>
                        <?php
                        
                        ?>
                        <img src="<?php echo site_url($foto); ?>" class="img-circle tree-user-avatar" alt="<?php echo "{}-{$userLeft->lastname}" ?>">
                        <br>
                        <span><?php echo $name ?></span>
                        <p>
                            Esquerda  |  Direita <br>
                            <?php echo $pleft ?> | <?php echo $pright ?>
                        </p>
                        
                    </a>
                    
                    <?php 
                    if ($level < 4) {
                        if ($userIdLeft > 0) {

                            if ($uleft > 0 || $uright > 0) {
                                $pontos = $this->getNextLevel($uleft, $uright, $level);

                                $cadastrosEsquerda += $pontos["total"];
                                $maxLevel = ($pontos["maxLevel"] > $maxLevel ? $pontos["maxLevel"] : $maxLevel);
                            } else {
                                $this->addEmptyLevel(0, (4 - $level));
                            }
                        } else {
                            $this->addEmptyLevel(0, (4 - $level));
                        }
                    }
                    ?>
                    
                </li>
                <li>
                    <a href="<?php echo site_url('backoffice/tree/binary/' . $userIdRight) ?>">
                    
                    <?php 
                    $foto = "assets/images/user_inactive.png";
                    $uleft = 0;
                    $uright = 0;
                    $name = "";
                    $pleft = 0;
                    $pright = 0;
                    if ($userIdRight > 0) {
                        $cadastrosDireita++;
                        $userRight = User::find_by_id($userIdRight);
                        $treeResultRight = Binarytree::find_by_sql("SELECT * FROM binarytrees WHERE user_id = {$userIdRight};");

                        $name = $userRight->firstname;
                        $pleft = $userRight->pleft;
                        $pright = $userRight->pright;
                        if (sizeof($treeResultRight) > 0) {
                            $tree = $treeResultRight[0];
                            $uleft = $tree->uleft;
                            $uright = $tree->uright;
                        }
                        $foto = (empty($userRight->photo) ?  ($userRight->gender == "male" ? 'assets/images/user_male.png' : 'assets/images/user_female.png') : "uploads/{$userRight->photo}" );
                    }
                    ?>
                        <img src="<?php echo site_url($foto); ?>" class="img-circle tree-user-avatar" alt="<?php echo $name ?>">
                        <br>
                        <span><?php echo $name ?></span>
                        <p>
                            Esquerda  |  Direita <br>
                            <?php echo $pleft ?> | <?php echo $pright ?>
                        </p>
                    </a>
                    
                    <?php 
                    if ($level < 4) {
                        if ($userIdRight > 0) {
                            if ($uleft > 0 || $uright > 0) {
                                $pontos = $this->getNextLevel($uleft, $uright, $level);

                                $cadastrosDireita += $pontos["total"];
                                $maxLevel = ($pontos["maxLevel"] > $maxLevel ? $pontos["maxLevel"] : $maxLevel);
                            } else {
                                $this->addEmptyLevel(0, (4 - $level));
                            }
                        } else {
                            $this->addEmptyLevel(0, (4 - $level));
                        }
                    }
                    ?>
                    
                </li>
            </ul>
            <?php
            
            
            return Array("pessoasEsquerda" => $cadastrosEsquerda, "pessoasDireita" => $cadastrosDireita, "total" => ($cadastrosEsquerda + $cadastrosDireita), "maxLevel" => ($maxLevel > $level ? $maxLevel : $level));
        }
        
        
        
        private function addEmptyLevel($current = 0, $levels = 3) {
            $current++;
            ?>
            <ul>
                <li>
                    <a href="#">
                        <img src="<?php echo site_url("assets/images/user_inactive.png")?>" class="img-circle tree-user-avatar">
                        <br>
                        <span></span>
                        <p>
                            Esquerda  |  Direita <br>
                            0 | 0
                        </p>
                    </a>
                    
                    <?php 
                    
                    if ($current < $levels) {
                        $this->addEmptyLevel($current, $levels);
                    }
                    ?>
                </li>
                <li>
                    <a href="#">
                        <img src="<?php echo site_url("assets/images/user_inactive.png")?>" class="img-circle tree-user-avatar">
                        <br>
                        <span></span>
                        <p>
                            Esquerda  |  Direita <br>
                            0 | 0
                        </p>
                    </a>
                    
                    <?php 
                    if ($current < $levels) {
                        $this->addEmptyLevel($current, $levels);
                    }
                    ?>
                </li>
            </ul>
            <?php
            
        }
       
}