<?php

/**
 * Project:    mmn.dev
 * File:       Invoices.php
 * Author:     Felipe Medeiros
 * Createt at: 27/05/2016 - 20:49
 */

class Invoices extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->user)
			redirect('backoffice/login');

		$this->params['module_name']	= 'Faturas';
		$this->breadcrumbs->push($this->params['module_name'], '/backoffice/invoices');
	}	
	public function index() { redirect('backoffice/invoices/all'); }

	public function all($filter = FALSE)
	{

		$this->params['page_name']	= 'Lista';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/invoices/all');

		$this->params['invoices'] = Invoice::all(array('conditions' => array('user_id = ?', $this->user->id)));
		$this->content_view = 'backoffice/invoices/all';
	}
	public function view($id = FALSE)
	{
		if (!is_numeric($id) || Invoice::count(array('conditions' => array('id = ? and user_id = ?', $id, $this->user->id))) != 1)
			redirect('backoffice/invoices');

		$invoice	 = Invoice::find(array('conditions' => array('id = ? and user_id = ?', $id, $this->user->id)));
		$this->params['invoice'] = $invoice;

		$user		 = User::find(array('conditions' => array('id = ?', $invoice->user_id)));
		$this->params['user'] = $user;
		
		$comprovant = Comprovant::all(array('conditions' => array('invoice_id = ?', $invoice->id)));
		$this->params['comprovants'] = $comprovant;

		$this->params['page_name']	= 'Ver fatura';
		$this->breadcrumbs->push('Fatura Nº' . $invoice->id, '/backoffice/invoices/view/' . $invoice->id);

		$this->content_view = 'backoffice/invoices/view';
	}
    public function upload($id = NULL)
    {
		if (!is_numeric($id) || Invoice::count(array('conditions' => array('id = ? and user_id = ? and status = ?', $id, $this->user->id, 'open'))) != 1) {
			redirect('backoffice/invoices');
                }
		
            $config['upload_path']          = './assets/uploads/';
            $config['allowed_types']        = 'gif|jpg|png|pdf|jpeg';
            $config['max_size']             = 1000;
            $config['encrypt_name']			= true;

            $this->load->library('upload', $config);

	        if(Comprovant::count(array('conditions' => array('invoice_id = ?', $id))) >= 3) {
	        $this->session->set_flashdata('message', ['text' => 'Máximo de três anexos!', 'type' => 'error']);
	        redirect('backoffice/invoices/view/'.$id);
	        die;
	    	}


            if (!$this->upload->do_upload('comprovante')) {
            $error = array('error' => $this->upload->display_errors());
			$this->session->set_flashdata('message', ['text' => 'Houve algum problema!', 'type' => 'error']);
			redirect('backoffice/invoices/view/'.$id);
            } else {
            //$data = array('upload_data' => $this->upload->data());
           	$data = $this->upload->data();

            $datainsert['invoice_id'] = $id;
			$datainsert['date'] = date('Y-m-d H:i:s');
			$datainsert['filename'] = $this->upload->data('file_name'); 
			$datainsert['filename_original'] = $this->upload->data('orig_name'); 

			if (Comprovant::create($datainsert)) $this->session->set_flashdata('message', ['text' => 'Upload de comprovante realizado!', 'type' => 'success']);
			else $this->session->set_flashdata('message', ['text' => 'Houve algum problema!', 'type' => 'error']);
			redirect('backoffice/invoices/view/'.$id);
            }
    }
    
    
    public function removeItem() {
        
        try {
            $data = $this->input->post();
            $itemId = (isset($data["item"]) ? $data["item"] : 0);
            
            $item = InvoicesItem::find_by_id($itemId);
            if ($item) {
                $item->delete();
            } else {
                throw new \Exception("O item informado não foi encontrado.");
            }
            
            $invoiceId = 0;
            $invoices = Invoice::find_by_sql("SELECT * from invoices WHERE status = 'open' AND type = 'buy' AND user_id = {$this->user->id} AND virtual_store = 1; ");
            if (sizeof($invoices) > 0) {
                $invoiceId = $invoices[0]->id;
            }
            
            $totalProducts = InvoicesItem::find_by_sql("SELECT COUNT(*) AS qtd, SUM(value * amount) AS total FROM invoices_items WHERE invoice_id = {$invoiceId};");
                
            $valorTotalInvoice = $totalProducts[0]->total;

            $invoice = Invoice::find_by_id($invoiceId);
            $invoice->sum = number_format($valorTotalInvoice, 2, '.', '');
            $invoice->save($invoice);


            $json["subtotal"] = "R$ " . number_format($valorTotalInvoice, 2, ',', '.');
            $json["total"] = "R$ " . number_format($valorTotalInvoice, 2, ',', '.');
            
            $json["sucesso"] = true;
            $json["mensagem"] = "Item removido com sucesso!";
        } catch (\Exception $ex) {

            $json["sucesso"] = false;
            $json["mensagem"] = $ex->getMessage();
        }

        exit(json_encode($json));
    }
    
    
}