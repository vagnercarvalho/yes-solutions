<?php
/**
 * Project:    mmn.dev
 * File:       Home.php
 * Author:     Felipe Medeiros
 * Createt at: 09/06/2016 - 06:41
 */

class Home extends MY_Controller {
	public function __construct() {
		parent::__construct();
		if (!$this->user) {
			redirect('backoffice/login');
		}

		$this->params['module_name'] = 'Home';
		$this->breadcrumbs->push($this->params['module_name'], '/backoffice/home');
	}
	public function index() {
		if ($this->input->post()) {
			$data = $this->input->post();
			if (!in_array($data['position'], array('auto', 'left', 'right'))) {
				$this->session->set_flashdata('message', array('type' => 'error', 'text' => 'Selecione uma posição existente!'));
			} else {
				$this->session->set_flashdata('message', array('type' => 'success', 'text' => 'Alterado com sucesso!'));
				$update           = User::find_by_id($this->user->id);
				$update->position = $data['position'];
				$update->save();
				$this->user->position = $data['position']; // ATUALIZA NA TELA
			}
		}

		$this->params['page_name'] = 'Dashboard';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/home/index');
		$this->content_view = 'backoffice/home/index';

		$this->params['sponsored'] = User::count([
			'conditions' => [
				'enroller = ?',
				$this->user->id,
			],
		]);
		$this->params['l_sponsored'] = User::all([
			'conditions' => [
				'enroller = ?',
				$this->user->id,
			],
			'limit'      => 5,
			'order'      => 'id desc',
		]);

		$getuser = User::find([
                        'conditions' => [
				'enroller = ?',
				$this->user->id,
			],
			'limit' => 1,
			'order' => 'id desc',
		]);

                if ($getuser) {

                    if ($getuser->gender == 'male') {
                            $sex = 'o';
                    } else {
                            $sex = 'a';
                    }


                    $this->params['alerthome'] = '<big>Parabéns! <img src="' . base_url('assets/images/flags') . '/' . $getuser->country . '.png"> ' . $getuser->firstname . ' ' . $getuser->lastname . ' acabou de se cadastrar através da sua indicação!</big>';
                } else {
                    $this->params['alerthome'] = "";
                
                }		
		$this->params['investido'] = Invoice::find_by_sql("SELECT sum(`sum`)  as `value` FROM `invoices` WHERE `user_id`='" . $this->user->id . "' AND `status`='paid' AND `type`='buy' AND `days` < '75' AND virtual_store = 0")[0]->value;
		
                
                $dayProfit = 0;
                $dayReference = date("Y-m-d");
                $days = DailyProfit::find_by_sql("SELECT * FROM daily_profits WHERE reference = '{$dayReference}'");
               
                if (sizeof($days) > 0) {
                    $dayProfit = $days[0]->profit;
                }
                
                $this->params["points"] = $this->user->points;
                $this->params['diario']    = number_format((($this->params['investido'] * $dayProfit) / 100), 2, '.', '');
                
                
                if ($this->user->pleft > $this->user->pright) {
                    $point_remove = $this->user->pright;
                } else if ($this->user->pright > $this->user->pleft) {
                    $point_remove = $this->user->pleft;
                } else {
                    $point_remove = $this->user->pleft;
                }
                
                // verifica a qualificação do usuário de acordo com os pontos acumulados
                $qualifications = Qualification::find_by_sql("SELECT * FROM qualifications WHERE points <= {$point_remove}  ORDER BY pos DESC;");
                $qualification = $qualifications[0];
                $nextQualification = Qualification::find_by_sql("SELECT * FROM qualifications WHERE pos = " . ($qualification->pos + 1));
                if (sizeof($nextQualification) > 0) {
                    $nextQualification = $nextQualification[0];
                } else {
                    $nextQualification = new Qualification();
                }
                $this->params['qualification']    = $qualification;
                $this->params['nextQualification']    = $nextQualification;
	}
        
        
        public function addToCart() {
            
            try {
                $data = $this->input->post();
                
                $productId = (isset($data["product"]) ? $data["product"] : 0);
                
                $product = Product::find_by_id($productId);
                
                if (!$product) {
                    throw new \Exception("Produto não econtrado!");
                }
                
                if ($product->status == 0) {
                    throw new \Exception("Produto indisponível no momento!");
                }
                
                $validade = date('Y-m-d H:i:s', strtotime("+3 days"));
                $invoiceId = 0;
                $invoices = Invoice::find_by_sql("SELECT * from invoices WHERE status = 'open' AND type = 'buy' AND user_id = {$this->user->id} AND virtual_store = 1; ");
                if (sizeof($invoices) > 0) {
                    $invoiceId = $invoices[0]->id;
                } else {
                    
                    $invoice = array();
                    $invoice['user_id'] = $this->user->id;
                    $invoice['type'] = 'buy';
                    $invoice['date'] = date('Y-m-d H:i:s');
                    $invoice['sum'] = $product->price;
                    $invoice['status'] = 'open';
                    $invoice['virtual_store'] = 1;
                    $invoice['due_date'] = $validade;

                    $ped = Invoice::create($invoice);
                    
                    if ($ped) {
                        $invoiceId = $ped->id;
                    }
                }
                
                $invitem = array();
                $invitem['invoice_id'] = $invoiceId;
                $invitem['name'] = $product->name;
                $invitem['description'] = "-";
                $invitem['value'] = $product->price;
                $invitem['amount'] = 1;
                $invitem['plan_id'] = $product->id;
                $invitem['profit'] = 0;
                $invitem['expiration_date'] = $validade;
                InvoicesItem::create($invitem);
                
                $totalProducts = InvoicesItem::find_by_sql("SELECT COUNT(*) AS qtd, SUM(value * amount) AS total FROM invoices_items WHERE invoice_id = {$invoiceId};");
                
                $qtdProdutos = $totalProducts[0]->qtd;
                $valorTotalInvoice = $totalProducts[0]->total;
                
                $invoice = Invoice::find_by_id($invoiceId);
                $invoice->sum = number_format($valorTotalInvoice, 2, '.', '');
                $invoice->save($invoice);
                
                
                $json["qtd"] = $qtdProdutos;
                $json["sucesso"] = true;
                $json["mensagem"] = "Produto Adicionado com sucesso!";
            } catch (\Exception $ex) {

                $json["sucesso"] = false;
                $json["mensagem"] = $ex->getMessage();
            }
            
            exit(json_encode($json));
        }
        
        
        public function invoice() {
            
            $invoices = Invoice::find_by_sql("SELECT * from invoices WHERE status = 'open' AND type = 'buy' AND user_id = {$this->user->id} AND virtual_store = 1; ");
            
            if (sizeof($invoices) > 0) {
                redirect('backoffice/invoices/view/' . $invoices[0]->id);
            } else {
                redirect('backoffice/virtualstore');
            }
            
        }
        
}