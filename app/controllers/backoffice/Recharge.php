<?php
/**
 * Project:    mmn.dev
 * File:       Support.php
 * Author:     Felipe Medeiros
 * Createt at: 09/06/2016 - 06:41
 */
class Recharge extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->user)
			redirect('backoffice/login');

		$this->params['module_name'] = 'Recarregar Saldo';
		$this->breadcrumbs->push($this->params['module_name'], '/backoffice/recharge');
	}
    public function index() {


        if ($this->input->post()) {
            $data = $this->input->post();

            $productId = (isset($data["product"]) ? $data["product"] : 0);


            $produto = Product::find_by_id($productId);
            $contagem = Invoice::find_by_sql("SELECT count(`id`)  as `value` FROM `invoices` WHERE `user_id`='" . $this->user->id . "' AND `date` >= '" . date('Y-m-d H:i:s', time() - (60 * 30)) . "' order by id desc")[0]->value;

            $return = ['success' => FALSE];

            if (!$produto) {
                $return['message'] = "Produto inválido!";
            } else if ($produto->status < 1) {
                $return['message'] = "Produto desativado no sistema.";
            }

            if ($contagem >= 5) {
                $return['message'] = "Aguarde 30 minutos antes de abrir um novo pedido!";
            } else {

                $validade = date('Y-m-d H:i:s', strtotime("+3 days"));

                $invoice = array();
                $invoice['user_id'] = $this->user->id;
                $invoice['type'] = 'buy';
                $invoice['date'] = date('Y-m-d H:i:s');
                $invoice['sum'] = $produto->price;
                $invoice['status'] = 'open';
                $invoice['due_date'] = $validade;

                $ped = Invoice::create($invoice);
                $id = $ped->id;
                if ($id > 0) {

                    $invitem = array();
                    $invitem['invoice_id'] = $id;
                    $invitem['name'] = $produto->name;
                    $invitem['description'] = "-";
                    $invitem['value'] = $produto->price;
                    $invitem['amount'] = 1;
                    $invitem['plan_id'] = $produto->id;
                    $invitem['profit'] = 1;
                    $invitem['expiration_date'] = $validade;
                    InvoicesItem::create($invitem);
                    
                    if ($produto->price == 0) {
                        
                        // Roda bonus e faz ações que tem que fazer nesse sistema 
			$this->load->helper('invoice_paid');
			$payment_method = '';
			invoice_paid($id, $payment_method);

                        $invoice = Invoice::find_by_id($id);
			$invoice->last_att = date('Y-m-d H:i:s');
			$invoice->last_editor = null;
			$invoice->save();
                        
                    }

                    $return['success'] = TRUE;
                    $return['message'] = "Pedido realizado! Redirecionando..";
                    $return['redirect'] = site_url('backoffice/invoices/view/' . $ped->id);
                } else {
                    $return['message'] = "Algum problema ocorreu!";
                }
                
                
            }

            exit(json_encode($return));
        } else {
            
            $this->params['produtos'] = Product::find_by_sql("SELECT * FROM products WHERE status > 0 AND reactivate > 0 ORDER BY id DESC;");
            
            $this->params['page_name'] = 'Reativar Cadastro';
            $this->breadcrumbs->push($this->params['page_name'], '/backoffice/recharge');
            $this->content_view = 'backoffice/recharge/index';
        }
    }

}