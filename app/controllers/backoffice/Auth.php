<?php
/**
 * Project:    mmn.dev
 * File:       Auth.php
 * Author:     Felipe Medeiros
 * Createt at: 09/06/2016 - 07:31
 */
class Auth extends MY_Controller
{
	/**
	 * Auth constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		$this->theme_view = 'backoffice/theme/auth';
		$this->params['module_name'] = 'Auth';
	}

	/**
	 * Index Page
	 */
	public function index() { redirect('backoffice/login'); }

	/**
	 * Login Page
	 */
	public function login()
	{
            
		if ($this->input->post())
		{
			$email		= $this->input->post('email');
			$password	= $this->input->post('password');
			$return = ['success' => FALSE];
			if (empty($email) || empty($password))
				$return['message'] = 'Preencha todos os campos!';
			elseif (!($user = User::find(['conditions' => ['email = ?', $email]])))
				$return['message'] = 'Email e/ou senha invalido(s)!';
			elseif ($password != $this->encrypt->decode($user->password))
				$return['message'] = 'Email e/ou senha invalido(s)!';
			elseif ($user->banned == 'Y')
				$return['message'] = 'Esta conta encontra-se bloqueada!';
			elseif ($this->settings->lock_login == 'Y')
			$return['message'] = 'Acesso temporariamente fechado!';
			elseif ($this->settings->maintenance == 'Y')
			$return['message'] = 'Estamos em manutenção, volte mais tarde!';
			else
			{
				/*$cookie = array(
				'name' => 'userid',
				'value' => $user->id,
				'expire' => time()+(365*24*60*60),
				);
				$this->input->set_cookie($cookie);*/
				
				
				$user->last_login = date("Y-m-d H:i:s");
				$user->save();
						
							
				$this->session->set_userdata('user', $user->id);   
				$return['success']	= TRUE;
				$return['message']	= 'Aguarde enquanto redirecionamos você!';
				$return['redirect']	= site_url('backoffice/home');
			}
			exit(json_encode($return));
		}
		$this->params['page_name']	= 'Acesso';
		$this->content_view			= 'backoffice/auth/login';
	}

	public function logout()
	{
		if ($this->session->userdata('user'))
			$this->session->unset_userdata('user');

		redirect('backoffice/login');
	}

	public function forgot($token = FALSE)
	{
		if ($token)
		{
			if (!($user = User::find(['conditions' => ['token_forgot = ?', $token]])) || $user->token_forgot_timestamp < time())
				$this->session->set_flashdata('message', ['title' => 'Token Invalido!', 'text' => 'O token informado é invalido ou ja expirou!', 'type' => 'error']);
			else
			{
				# Gerar nova senha
				$new_password = substr(str_shuffle(strtolower(sha1(rand() . time() . "nekdotlggjaoudlpqwejvlfk"))), 0, 8);

				# Salvar nova senha
				$user->password					= $this->encrypt->encode($new_password);
				$user->token_forgot				= '';
				$user->token_forgot_timestamp	= 0;
				$user->save();

				# Enviar email com nova senha
				$this->load->library('parser');
				$this->email->from($this->settings->company_email, $this->settings->company_name);
				$this->email->reply_to($this->settings->company_email, $this->settings->company_name);
				$this->email->to($user->email);
				$this->email->subject('Nova senha');
				$parse_data = array(
					'password'	=> $new_password,
					'link'		=> site_url('backoffice/login'),
					'logo'		=> $this->settings->company_name,
					'logo_dark'	=> $this->settings->company_name,
					'company'	=> $this->settings->company_name
				);
				$email = read_file(APPPATH . 'views/templates/email_pw_reset.html');
				$message = $this->parser->parse_string($email, $parse_data);
				$this->email->message($message);
				$this->email->send();

				$this->session->set_flashdata('message', ['title' => 'Nova Senha', 'text' => 'Uma senha temporaria foi criada e enviada para seu e-mail!', 'type' => 'success']);
			}
			redirect('backoffice/login');
		}
		else
		{
			if ($this->input->post())
			{
				$email = $this->input->post('email');
				$return = ['success' => FALSE];
				if (empty($email))
					$return['message'] = 'Preencha todos os campos!';
				elseif (!($user = User::find(array('conditions' => array('email = ?', $email)))))
					$return['message'] = 'Empreendedor não localizado!';
				else
				{
					$time = time();
					$timestamp = $time + 3600; //uma hora pra alterar a senha.
					# Gerar token baseado com o timestamp atual
					$token = md5($timestamp);

					# Atualiza informações
					$user->token_forgot				= $token;
					$user->token_forgot_timestamp	= $timestamp;
					$user->save();

					# Enviar email
					$this->load->library('parser');
					$this->email->from($this->settings->company_email, $this->settings->company_name);
					$this->email->reply_to($this->settings->company_email, $this->settings->company_name);
					$this->email->to($user->email);
					$this->email->subject('Esqueceu sua senha');
					$parse_data = array(
						'link'		=> site_url('backoffice/forgot/' . $token),
						'logo'		=> $this->settings->company_name,
						'logo_dark'	=> $this->settings->company_name,
						'company'	=> $this->settings->company_name
					);
					$email = read_file(APPPATH . 'views/templates/email_pw_reset_link.html');
					$message = $this->parser->parse_string($email, $parse_data);
					$this->email->message($message);
					$this->email->send();

					$return['success']	= TRUE;
					$return['message']	= 'Para prosseguir com a recuperação de senha, siga as instruções enviadas para seu e-mail!';
					$return['redirect']	= site_url('backoffice/login');
				}
				exit(json_encode($return));
			}
			$this->params['page_name']	= 'Recuperar Conta';
			$this->content_view			= 'backoffice/auth/forgot';
		}
	}
	
	public function sponsor($id = FALSE)
	{
		
		if(empty($id)) $id = mt_rand(1,2);
		
		if (!($sponsor = User::find(array('conditions'	=> array('id = ? or link = ?', $id, $id)))))
		{
			$this->session->set_flashdata('message', array(
				'type'	=> 'error',
				'title'	=> 'Ops, temos problemas!',
				'text'	=> 'O link no qual você tentou acessar é invalido!'
			));
			redirect('backoffice/login');
		} else
		{
			if ($this->input->post())
			{
				$return['success'] = FALSE;
				if ($sponsor->status != 'active' OR $sponsor->banned != 'N')
					$return['message']	= 'Patrocinador não qualificado!';
				else
				{
					$data	= $this->input->post();
					if ($this->settings->lock_register == 'Y')
						$return['message']	= 'No momento não estamos aceitando novos cadastros!';
					elseif (!filter_var($data['email'], FILTER_VALIDATE_EMAIL))
						$return['message']	= 'Informe um email válido!';
					elseif (User::count(array('conditions' => array('email' => $data['email']))) != 0)
						$return['message']	= 'Este email já esta em uso!';
					elseif ($data['password'] != $data['confirm_password'])
						$return['message']	= 'As senhas não conferem!';
					elseif (strlen($data['password']) < 6)
						$return['message']	= 'A senha deve ter no minimo 6 caracteres!';
					elseif (Countrie::count(array('conditions' => array('code = ?', $data['country']))) != 1)
						$return['message']	= 'Pais inexistente!';
					else
					{
						unset($data['confirm_password']);
						$data['password']		= $this->encrypt->encode($data['password']);
						$data['create_date']	= date('Y-m-d H:i:s');

						list($bday, $bmonth, $byear)	= explode('/', $data['birthday']);
						$birthday				= mktime(0, 0, 0, $bmonth, $bday, $byear);
						$data['birthday']		= date('Y-m-d', $birthday);

						$data['enroller']		= $sponsor->id;
                                                
                                                $m = User::create($data);
						if ($m) {
							
                                                    $updatevisit = Setting::find_by_id(1);
                                                    $updatevisit->registers += 1;
                                                    $updatevisit->real_registers += 1;
                                                    $updatevisit->save();

                                                    // Automaticamente atribuindo o pacote free e posicionando o cliente na 
                                                    $invoice = array();
                                                    $invoice['user_id'] = $m->id;
                                                    $invoice['type'] = 'buy';
                                                    $invoice['date'] = date('Y-m-d H:i:s');
                                                    $invoice['sum'] = 0.0;
                                                    $invoice['status'] = 'open';
                                                    $invoice['due_date'] = date('Y-m-d H:i:s');;

                                                        $ped = Invoice::create($invoice);
                                                        $id = $ped->id;
                                                        if ($id > 0) {

                                                        $invitem = array();
                                                        $invitem['invoice_id'] = $id;
                                                        $invitem['name'] = "Pacote Free";
                                                        $invitem['description'] = "-";
                                                        $invitem['value'] = 0;
                                                        $invitem['amount'] = 1;
                                                        $invitem['plan_id'] = 14;
                                                        $invitem['profit'] = 1;
                                                        $invitem['expiration_date'] = date('2050-m-d H:i:s');
                                                        InvoicesItem::create($invitem);

                                                        // Roda bonus e faz ações que tem que fazer nesse sistema 
                                                        $this->load->helper('invoice_paid');
                                                        $payment_method = '';
                                                        invoice_paid($id, $payment_method);

                                                        $invoice = Invoice::find_by_id($id);
                                                        $invoice->last_att = date('Y-m-d H:i:s');
                                                        $invoice->last_editor = null;
                                                        $invoice->save();

                                                    }

                                                    $return['success']	= TRUE;
                                                    $return['message']	= 'Parabens, agora você é um associado!';
                                                    $return['redirect']	= site_url('backoffice/login');							
						} else
							$return['message']	= 'Tivemos alguns problemas no cadastro. Tente mais tarde!';
					}
				}
				exit(json_encode($return));
			}


			$this->params['sponsor'] = $sponsor;
			$this->params['page_name']	= 'Cadastro';
			$this->content_view	= 'backoffice/auth/sponsor';
			
			$query = Countrie::all(['order' => 'name asc']);
			$loadinfo = array();
			foreach ($query as $row) {
				$code = $row->code;
				$loadinfo[$code] = $row->name;
			}
			
			$this->params['options'] = $loadinfo;
		
		}
	}
}