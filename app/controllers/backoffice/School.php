<?php
/**
 * Project:    mmn.dev
 * File:       School.php
 * Author:     Felipe Medeiros
 * Createt at: 09/06/2016 - 06:41
 */
class School extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->user)
			redirect('backoffice/login');

		$this->params['module_name'] = 'Cursos';
		$this->breadcrumbs->push($this->params['module_name'], '/backoffice/school');
	}
	public function index()
	{
		$this->params['page_name'] = 'E-Books/Audiobooks';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/school/index');
		$this->content_view	= 'backoffice/school/index';
	}
}