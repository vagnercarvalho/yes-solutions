<?php
/**
 * Project:    mmn.dev
 * File:       Statistics.php
 * Author:     Felipe Medeiros
 * Createt at: 09/06/2016 - 06:41
 */
class Statistics extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		if (!$this->user)
			redirect('backoffice/login');

		$this->params['module_name'] = 'Estatisticas';
		$this->breadcrumbs->push($this->params['module_name'], '/backoffice/statistics');
	}
	public function index()
	{
		$this->params['page_name'] = 'Geral';
		$this->breadcrumbs->push($this->params['page_name'], '/backoffice/statistics/index');
		$this->content_view	= 'backoffice/statistics/index';

		$this->params['invoices'] = Invoice::all([
			'limit' => 5,
			'order' => 'id desc'
		]);
		$this->params['withdrawals'] = Withdrawal::all([
			'limit' => 5,
			'order' => 'id desc'
		]);
		$this->params['users'] = User::all([
			'limit' => 5,
			'order' => 'id desc'
		]);
	}
}