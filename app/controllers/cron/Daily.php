<?php

/**
 * Author:      Felipe Medeiros
 * File:        Daily.php
 * Created in:  25/06/2016 - 00:59
 */
// echo date('d') . '<br />';
// echo date("t"); exit;

class Daily extends CI_Controller {

    public function index() {
        
    }

    public function verify() {
        $users = User::all([
            'conditions' => [
                'status = ?',
                'active'
        ]]);
        
        // se sexta feira
        foreach ($users as $user) {

            /* Verifica se tem ponto de ambos lados do binario */
            if ($user->pleft > 0 AND $user->pright > 0) {

                $userPosition = Binarytree::find_by_user_id($user->id);

                if ($userPosition->uleft > 0 && $userPosition->uright > 0) {

                    $habilitarDireita = InvoicesItem::find_by_sql("SELECT COUNT(*) AS count FROM invoices i 
                                                                    INNER JOIN invoices_items ii ON (i.id = ii.invoice_id)
                                                                    INNER JOIN users u ON (i.user_id = u.id)
                                                                    WHERE i.status = 'paid' AND i.type = 'buy' AND u.enroller = {$user->id} AND ii.profit > 0 AND ii.value >= 900;")[0]->count;

                    /*
                    $habilitarDireita = InvoicesItem::find_by_sql("SELECT COUNT(*) AS count FROM invoices i 
                                                                    INNER JOIN invoices_items ii ON (i.id = ii.invoice_id)
                                                                    WHERE i.status = 'paid' AND i.type = 'buy' AND i.user_id = {$userPosition->uright} AND ii.profit > 0 AND ii.value >= 900;")[0]->count;

                    $habilitarEsquerda = InvoicesItem::find_by_sql("SELECT COUNT(*) AS count FROM invoices i 
                                                                    INNER JOIN invoices_items ii ON (i.id = ii.invoice_id)
                                                                    WHERE i.status = 'paid' AND i.type = 'buy' AND i.user_id = {$userPosition->uleft} AND ii.profit > 0 AND ii.value >= 900;")[0]->count;
                                                                    */

                    if ($habilitarDireita >= 2) {
                        if ($user->pleft > $user->pright)
                            $point_remove = $user->pright;
                        elseif ($user->pright > $user->pleft)
                            $point_remove = $user->pleft;
                        else
                            $point_remove = $user->pleft;

                        
                        
                        $invoiceItem = Invoice::find_by_sql("SELECT ii.*, p.binario_percentual_payment FROM invoices i "
                            . " INNER JOIN invoices_items ii ON (ii.invoice_id = i.id) "
                            . " INNER JOIN products p ON (p.id = ii.plan_id) "
                            . " WHERE i.status = 'paid' AND i.type = 'buy' AND i.user_id = {$user->id} AND p.reactivate = 0 AND i.virtual_store = 0 "
                            . " ORDER BY i.id DESC;")[0];


                        if ($invoiceItem->value > 0 && $invoiceItem->binario_percentual_payment > 0) {
                            if ($invoiceItem && $invoiceItem->binario_percentual_payment > 0) {
                                $vlrganhobinario = (($point_remove * $invoiceItem->binario_percentual_payment) / 100); //2% DE BINARIO

                                /* Remove os pontos do binário */
                                $user->pleft -= $point_remove;
                                $user->pright -= $point_remove;
                                $user->points += $point_remove;

                                // verifica a qualificação do usuário de acordo com os pontos acumulados
                                $qualifications = Qualification::find_by_sql("SELECT * FROM qualifications WHERE points <= {$point_remove}  ORDER BY pos DESC;");
                                $qualification = $qualifications[0];

                                if ($qualification->teto < $vlrganhobinario) {
                                    $vlrganhobinario = $qualification->teto;
                                }

                                /* Insere extrato */
                                $insert = array();
                                $insert['user_id'] = $user->id;
                                $insert['date'] = date('Y-m-d H:i:s');
                                $insert['value'] = $vlrganhobinario;
                                $insert['description'] = 'Bonus binário.';

                                $contagemdiretosbin = User::count(array('conditions' => array('status = ? and enroller = ?', 'active', $user->id)));

                                if ($user->status == 'active' AND $contagemdiretosbin >= 2)
                                    $insert['type'] = 'credit';
                                else
                                    $insert['type'] = 'lost';

                                $insert['bonus_cod'] = 5;
                                $insert['subtype'] = 'bonus';
                                Extract::create($insert);

                                if ($user->status == 'active' AND $contagemdiretosbin >= 2) {
                                    /* Atualizar usuário */
                                    $user->balance += $vlrganhobinario; //CREDITA VALOR BONUS
                                    $user->ganhos += $vlrganhobinario;
                                    $user->save();
                                    echo "" . $user->firstname . " " . $user->lastname . " - Recebeu " . $vlrganhobinario . " ({$invoiceItem->binario_percentual_payment}%) em bonus binário.<br>";
                                }
                            }
                        }
                    }
                }
            }
            
            if ($user->id > 1 ) {
                $d = new DateTime(date("Y-m-d") . " 23:59:59");
                $d->sub(new DateInterval("P30D"));

                $invoices = Invoice::find_by_sql("SELECT COUNT(*) AS qtd FROM invoices WHERE status = 'paid' AND payment_date > '{$d->format("Y-m-d H:i:s")}' AND type = 'buy' AND user_id = {$user->id};")[0]->qtd;

                if (!($invoices > 0)) {

                    if ($user->status != 'inactive') {
                        $user->status = 'inactive';

                        $user->save();
                    }

                }
            }
            
        }
            
    }
    
    
    public function verifyold() {
        exit("ok");
        error_reporting(E_ALL);
        
        $settingup = Setting::first();
        
        $dtultbonus = substr($settingup->last_daybonus, 0, 10);

        if (empty($dtultbonus)) {
            $yesterday = new DateTime(date("Y-m-d"));
            $yesterday->sub("P1D");
            $dtultbonus = $yesterday->format("Y-m-d");
        }

        $lastPayment = new DateTime($dtultbonus);
        $today = new DateTime(date("Y-m-d"));

        $users = User::all([
                    'conditions' => [
                        'teto != ?',
                        '0'
        ]]);

        
        
        
        
        $lastPayment->add(new DateInterval("P1D"));
        //if ($lastPayment->getTimestamp() < $today->getTimestamp()) {
        while ($lastPayment->getTimestamp() <= $today->getTimestamp()) {

            $days = DailyProfit::find_by_sql("SELECT * FROM daily_profits WHERE reference = '{$lastPayment->format("Y-m-d")}'");

            if (sizeof($days)) {

                $bonus = $days[0];

                if ($bonus->profit > 0) {


                    echo "{$lastPayment->format("Y-m-d")} = {$bonus->profit} <br>";

                    foreach ($users as $user) {
                        $currentDate = date("Y-m-d H:i:s");


                        $invoices = Invoice::find_by_sql("SELECT p.id, i.value, p.payment_date
                                                        FROM invoices p 
                                                        INNER JOIN invoices_items i ON (i.invoice_id = p.id)
                                                        INNER JOIN packages pkg ON (i.plan_id = pkg.id)
                                                        WHERE p.user_id = {$user->id} AND status = 'paid' AND pkg.pay_profit > 0 AND type = 'buy' AND i.expiration_date >= '{$currentDate}';");

                        foreach ($invoices as $invoice) {

                            $paymentDate = new DateTime($invoice->payment_date);

                            if ($paymentDate->getTimestamp() < $lastPayment->getTimestamp()) {
                                $valorganho = number_format($invoice->value * ($bonus->profit / 100), 2, '.', '');

                                /* Insere extrato */
                                $insert = array();
                                $insert['user_id'] = $user->id;
                                $insert['date'] = $lastPayment->format("Y-m-d") . " " . date("H:i:s");
                                $insert['value'] = $valorganho;
                                $insert['description'] = "Rendimento de aplicação pacote {$invoice->id} (" . number_format($bonus->profit, 2, ",", "") . "%) ";

                                /*
                                  if ($user->teto > $user->ganhos AND $user->status == 'active')
                                  $insert['type'] = 'credit';
                                  else
                                  $insert['type'] = 'lost';
                                 * 
                                 */

                                $insert['type'] = 'credit'; // como não tem teto, será sempre crédito
                                $insert['bonus_cod'] = 3;
                                $insert['subtype'] = 'bonus';
                                Extract::create($insert);


                                //if ($user->teto > $user->ganhos AND $user->status == 'active') {
                                /* Atualizar usuário */
                                $u = User::find_by_id($user->id);

                                $u->balance += $valorganho; //CREDITA VALOR BONUS
                                $u->ganhos += $valorganho;
                                echo "" . $user->firstname . " " . $user->lastname . " - Recebeu " . $valorganho . " (" . number_format($bonus->profit, 2, ",", "") . "%) da lucratividade diaria.<br>";
                                $u->save();
                                //}
                            }
                        }
                    }

                    $settingup->last_daybonus = date("Y-m-d H:i:s");
                    $settingup->save();
                }
            }

            $lastPayment->add(new DateInterval('P1D'));
        }
        //}

        $dtback = "" . date("Y-m-d", strtotime("-1 month")) . " 00:00:00";

        /* Mudança de status do pedido - aberto p/ cancelado (buy, upgrade, monthly, recharge) */
        $invoices = Invoice::all(['conditions' => ['status = ? and type != ? and date < ?', 'open', 'monthly', $dtback], 'order' => 'id asc']);
        if ($invoices) {
            foreach ($invoices as $invoice) {
                $invoice->status = 'canceled';
                $invoice->save();
                echo '<br>Pedido ' . $invoice->id . ' cancelado por tempo. <br>';
            }
        }
    }

    
}
