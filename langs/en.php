<?php
######################################################
#### ARQUIVO DE LINGUAGEM >EN< DA LANDING PAGE ####
######################################################

##### PRINCIPAL
$lang['titulo'] = "InvesteJovem.ONLINE";
$lang['frase'] = "Financial Independence Already!";
$lang['fechar'] = "Close";
$lang['restrito'] = "Search your sponsor!";
$lang['cadastrese'] = "Register";
$lang['saibamais'] = "Learn more";


##### MENU
$lang['menu1'] = "Home";
$lang['menu2'] = "The Company";
$lang['menu3'] = "Bonuses";
$lang['menu4'] = "Plans";
$lang['menu5'] = "Contact";
$lang['menu6'] = "Backoffice";


##SLIDERS
$lang['boasvindas'] = "Hello, Welcome to InvesteJovem.ONLINE!";
$lang['frase1'] = "Guy, you are the future! <br/>We will help you SUCCEED.";
$lang['frase2'] = "Be Your Own Boss! We'll help you!";
$lang['frase3'] = "Life is too short not to be taken advantage of! Invest, Guy!";


##### CONTEUDO
$lang['aempresa'] = "The Company";
$lang['quemsomos'] = "Who are we?";
$lang['comofunciona'] = "How it works?";
$lang['nossahistoria'] = "Our Story";
$lang['texto1'] = "<p>
							Hello! Well, let's explain in a simple and uncomplicated way what this is all about!
							<br>We are a group of young investors working with Forex Market, trading and mining bitcoins®!
							<br>We worked for a few years with the Bitcoin® market and Forex market, we also manage one of the largest Bitcoin® mining servers in Brazil!
							<br>Anyway, where do I get into this? Calm down young .. let's get there!
							<br>Today we have a substantial daily profit and we understand this complex and revolutionary market well.
							<br>We decided to open the door for those young people who want to grow up with us and invest!<br>
							The process is simple, you invest in INVESTEJOVEM.ONLINE and we work your money!
							<br>Understand that $1,000.00 applied yields much less than $ 10,000.00 in a daily negotiation,
							That is, we apply their money in our negotiations and the returns are also greater, we pass on the values to the investors.
							<br>As a form of reward we have developed a system with several bonuses for working with us.
							<br> Your money is 75 days with us only and at the end of this period you will have at least 225% of the amount invested, not counting the extra bonuses. You can withdraw your balance at any time, remembering that the minimum amount is $10.00 for cash withdrawal.
							<br> We have also developed several audiobooks and ebooks over time and will make them available for free to our members.
							<br><br>We are the future! INVESTEJOVEM.ONLINE
							<br><br><b>Ready to start profiting and learning?</b> 
							<br>>> Know our history!
							</p>";
$lang['texto2'] = "                            <p>
							Hello! Well, if you got here you have an interest in growing up in life, right?!
							<br>We are a group of young investors thirsting to grow just like you!
							<br><br>We worked for about 5 years in the trading market of Bitcoin® and also in the Forex market, we went through several Multilevel Marketing companies, we acquired a lot of knowledge
							And we decided to set up INVESTEJOVEM.ONLINE putting everything into practice, focused on young people who just like us do not want to be just one more, want more! <br>
							<br>INVESTEJOVEM.ONLINE is made by young people for young people so we treat the various subjects in an uncomplicated and direct way.
							<br>In the beginning as in anything we suffer in the market of transactions, but practice leads to perfection, correct?
							<br>Today we have substantial daily profits and a good box to work day after day plus we manage a bitcoins mining base, one of the largest in Brazil. ;)
							<br><br>We are the future! We bet high and win! Be different, take the first step, be different from the most accommodated in the world!
							<br><br>We are the ones that turn $100.00 into $100,000.00!
							<br><br>95% of all the money in the world is controlled by only 5% of the population, do you agree with that?! We neither!
							<br><br><b>Are you ambitious?! Are you ready to grow?! If so, we'll help you!</b>
							</p>";


##### INFORMATIVOS							
$lang['maissobre'] = ">More about INVESTEJOVEM.ONLINE ..";
$lang['topico1'] = "Safety";
$lang['desc1'] = "All data is protected with the latest technologies.
Priority for us! :D";
$lang['topico2'] = "Instant Cash Out";
$lang['desc2'] = "Got it and it automatically counts! No secret!:)";
$lang['topico3'] = "SSL Encryption";
$lang['desc3'] = "You are on a fully protected connection. ;)";
$lang['topico4'] = "Guaranteed Profit";
$lang['desc4'] = "We guarantee a minimum of <b>225%</b> of its value in 75 days!! *-*";
$lang['dadosimportantes'] = "Important information ..";
$lang['desc5'] = "Incoming";
$lang['desc6'] = "Draw out";
$lang['desc7'] = "Last Incoming";
$lang['desc8'] = "Last draw out";
$lang['desc9'] = "Online Visitors";
$lang['desc10'] = "Total Investors";
$lang['realtime'] = "The data presented is calculated in real-time. :D";


##### BONUS		
$lang['bonificacoes'] = "Bonuses";
$lang['comoganhar'] = "How can I win?";
$lang['bonus1'] = "DAILY PROFITABILITY";
$lang['descbonus1'] = "<p>Earn daily <b>3%</b> On the active amount in your account for 75 days.
							<br><b>Ex:</b> Get a package of $100.00 and get $3.00 a day, in 75 days you earned $225.
							</p>";
$lang['bonus2'] = "DAILY NETWORK PROFIT";
$lang['descbonus2'] = "<p>Receive while active daily <b>0.2%</b> On the active value of its direct nominees.
							<br><b>Ex:</b> Indicated someone with a package of $3,000.00 and get $6.00 a day, in 75 days he earned $450.00.
							</p>";
$lang['bonus3'] = "DIRECT INDICATION";
$lang['descbonus3'] = "<p>Earn <b>5%</b> On the asset value acquired by its nominee.
							<br><b>Ex:</b> Indicated someone with a package of $3,000.00 and automatically won $150.00.
							</p>";
$lang['bonus4'] = "INDIRECT INDICATION";
$lang['descbonus4'] = "<p>Earn <b>2%</b> On the asset value acquired by an indirect nominee.
							<br><b>Ex:</b> Your indicated indicated someone with a package of $3,000.00 and you automatically won $60.00
							<br>Bonus pays up to 5th level! Spectacular, right?! ;D
							</p>";
$lang['bonus5'] = "BINARY";
$lang['descbonus5'] = "<p>Earn daily <b>15%</b> on bonus from your lower leg of your binary network.
							<br><b>Ex:</b> It has 5000 points on the left leg and 6000 points on the right, we pay 15% the value of the lower leg daily, that is, it won $750.00.
							<br>Each point is worth $1.00.
							</p>";
$lang['bonus6'] = "KNOWLEDGE (+Prizes)";
$lang['descbonus6'] = "<p>Earn <b>a lot of knowledge </b> our investment courses. ;)
							<br>Nothing more valuable than knowledge, right?! :D
							</p>";
$lang['msgteto'] = "<font color='red'><b>*</b></font> Plan valid for 75 days or until reaching the ceiling of 225%. <font color='red'><b>*</b></font>
<br><font color='red'><b>*</b></font> Daily profitability and daily network profitability paid from Monday to Friday. <font color='red'><b>*</b></font>";
					
##### PLANO			
$lang['nossoplano'] = "Our Plan ;)";
$lang['saibamaisplano'] = "Learn more about our plan";
$lang['plano1'] = "INVESTOR <font color='#0B8480'>225%</font>";
$lang['digite'] = "Enter";
$lang['calcular'] = "Calculate";
$lang['digitevalor'] = "Please enter a value.";
$lang['produto'] = "<font color='#0B8480'>Right to Online Courses. <strong>(Ebooks/Audiobooks)</strong></font>";
$lang['planocarreira'] = "Career plan with <strong>Awards</strong> <br>(Money, travel, cars, etc.)";		
$lang['participacao'] = "Participation in the company's investments.";
$lang['ganhebinario'] = "Earn <strong>15%</strong> in binary.";
$lang['saqueminimo'] = "Minimum withdrawal <strong>$ 10.00</strong>.";
$lang['validadeplano'] = "Valid for 75 days.";		
$lang['ganhominimo'] = "Minimum gain in 75 days:";


##### APRESENTAÇÃO			
$lang['saibamaisplano'] = "Know our business plan";
$lang['planodenegocios'] = "Business plan";
$lang['anterior'] = "Previous";
$lang['proxima'] = "Next";


##### FALE CONOSCO	
$lang['faleconosco'] = "Contact us";
$lang['ondeencontrar'] = "Where can you find us?";
$lang['telefone'] = "Telephone";
$lang['atendimento'] = "Attendance";
$lang['hratendimento'] = "Monday - Friday 9:00am - 18:00pm";
$lang['copyright'] = "All rights reserved.";
$lang['creditos'] = "<br />Developed by <a href='http://www.viniciusmenegatti.com/' style='color: inherit;' target='_blank'>
<b>ViniciusMenegatti.com</b></a>.";


##### JAVASCRIPT		
$lang['valormin'] = "Min Value $10.00!";
$lang['valormax'] = "Max Value $10,000.00!";
$lang['pontos'] = "points";
$lang['vale'] = "Earn";
$lang['receba'] = "Earn";
$lang['aodia'] = "per day. (3% daily)";
$lang['alertavalor'] = "Enter a value. (Min $10.00 / Max $10,000.00)";
$lang['precadastro'] = "<h2>Stay tuned!<br><br>Pre-registration released 24/04!<br><br>
Be one of the first to enter this business and change your life! We are waiting for you.<br><br>
INVESTEJOVEM.ONLINE - WE ARE THE FUTURE!</h2>";







