<?php
######################################################
#### ARQUIVO DE LINGUAGEM >PT-BR< DA LANDING PAGE ####
######################################################

##### PRINCIPAL
$lang['titulo'] = "FELIPE SITES";
$lang['frase'] = "Tudo em um Só Lugar!";
$lang['fechar'] = "Fechar";
$lang['restrito'] = "Procure seu patrocinador!";
$lang['cadastrese'] = "Cadastre-se";
$lang['saibamais'] = "Saiba mais";

##### MENU
$lang['menu1'] = "Home";
$lang['menu2'] = "A Empresa";
$lang['menu3'] = "Bonificações";
$lang['menu4'] = "Planos";
$lang['menu5'] = "Contato";
$lang['menu6'] = "Escritório";

##SLIDERS
$lang['boasvindas'] = "Olá, seja bem-vindo(a) ao FELIPE SITES!";
$lang['frase1'] = "Você é o futuro! <br/>Nós iremos te ajudar a obter SUCE\$\$O!";
$lang['frase2'] = "Seja você seu próprio Chefe! Nós vamos te ajudar!";
$lang['frase3'] = "A vida é muito curta para não ser aproveitada!";

##### CONTEUDO
$lang['aempresa'] = "A Empresa";
$lang['quemsomos'] = "Quem somos nós?";
$lang['comofunciona'] = "Como Funciona?";
$lang['nossahistoria'] = "Nossa história";
$lang['texto1'] = "<p>
Olá! Bom, vamos explicar de maneira simples e descomplicada o que é isso tudo!<br>
<br>
Somos um grupo de jovens investidores que trabalham com Mercado Forex, negociação e mineração de bitcoins®! Trabalhamos a alguns anos com o mercado de Bitcoin® e mercado Forex, administramos também um dos maiores servidores de mineração de Bitcoin® do Brasil!<br>
<br>
Tá mas eaí, onde entro nisso? Calma... vamos chegar lá!<br>
<br>
Hoje temos um lucro diário substancial e entendemos bem este mercado tão complexo e revolucionário.<br>
<br>
Decidimos abrir as portas para aqueles jovens que querem crescer conosco e investir!<br>
O processo é simples, você investe no FELIPE SITES e nós trabalhamos o seu dinheiro! 
<br>
Entenda que $1,000.00 aplicados rende muito menos que $10,000.00 em uma negociação diária, ou seja aplicamos seu dinheiro em nossas negociações e os retornos são igualmente maiores, repassamos assim os valores aos investidores.<br>
<br>
Como forma de recompensa desenvolvemos um sistema com diversos bônus por estar trabalhando conosco.<br>
<br>
Seu dinheiro fica 75 dias conosco apenas e ao fim deste periodo você vai ter no minimo 225% do valor investido, sem contar os bônus extras. Você pode sacar a qualquer instante seu saldo, lembrando que o valor minimo é $10.00 para o saque.<br>
<br>
Desenvolvemos também diversos audiobooks e ebooks ao passar do tempo e iremos disponibiliza-los gratuitamente para nossos associados.<br>
<br>
FELIPE SITES - TUDO EM UM SÓ LUGAR!
<br>
<b>Pronto para começar a lucrar?</b> 
</p>";
$lang['texto2'] = "<p>
Olá! Bom, se você chegou até aqui você tem interesse de crescer na vida, correto?!
<br>Somos um grupo de jovens investidores com sede de crescer exatamente como você!
<br><br>Trabalhamos a cerca de 5 anos no mercado de negociação do Bitcoin® e também no mercado Forex, passamos por diversas empresas de Marketing multinível, adquirimos muito conhecimento
e decidimos montar a FELIPE SITES colocando tudo em prática, focada para jovens que assim como nós não querem ser apenas mais um, querem mais, almejam mais! <br>
<br>A FELIPE SITES é feita por jovens para jovens portanto tratamos os diversos assuntos de maneira descomplicada e direta.
<br>No inicio como em qualquer coisa sofremos no mercado de transações, porém a pratica leva a perfeição, correto?
<br>Hoje temos lucros diários substanciais e um bom caixa para trabalhar dia após dia além do mais administramos uma base de mineração de bitcoins, uma das maiores
do Brasil. ;)
<br><br>Nós somos o futuro! Nós apostamos alto e ganhamos! Seja diferente, dê o primeiro passo, seja diferente da maioria de acomodados que existem no mundo!
<br><br>Nós somos aqueles que transformam $100.00 em $100,000.00!
<br><br>95% de todo dinheiro do mundo é controlado por apenas 5% da população, você concorda com isso?! Nós também não!
<br><br><b>Você é ambicioso?! Está pronto para crescer?! Se sim, nós vamos te ajudar!</b>
<br>
<br>FELIPE SITES - TUDO EM UM SÓ LUGAR!
<br><b>Pronto para começar a lucrar?</b> 
</p>";

##### INFORMATIVOS							
$lang['maissobre'] = "Segurança & Tecnologia";
$lang['topico1'] = "Segurança";
$lang['desc1'] = "Todos os dados são protegidos com as ultimas tecnologias.
Prioridade para nós! :D";
$lang['topico2'] = "Saques Instantâneos";
$lang['desc2'] = "Sacou e automaticamente tá na conta! Sem segredo! :)";
$lang['topico3'] = "Criptografia SSL";
$lang['desc3'] = "Você está em uma conexão totalmente protegida. ;)";
$lang['topico4'] = "Lucro Garantido";
$lang['desc4'] = "Garantimos minimamente <b>450%</b> do seu valor em 90 dias!!
Bom né?! *-*";
$lang['dadosimportantes'] = "Estatisticas";
$lang['desc5'] = "Entradas";
$lang['desc6'] = "Saídas";
$lang['desc7'] = "Última Entrada";
$lang['desc8'] = "Última Saída";
$lang['desc9'] = "Visitantes Onlines";
$lang['desc10'] = "Total Investidores";
/*$lang['realtime'] = "Os dados apresentados são calculados em real-time. :D";*/

##### BONUS		
$lang['bonificacoes'] = "Bonificações";
$lang['comoganhar'] = "Como eu posso ganhar?";
$lang['bonus1'] = "BÔNUS DE LUCRATIVIDADE DIÁRIA";
$lang['descbonus1'] = "<p>Receba diariamente <b>5%</b> sobre o valor ativo em sua conta por 90 dias.
<br><b>Ex:</b> Adquira um pacote de $100.00 e ganhe $5.00 ao dia, em 90 dias ganhou $450.00.
</p>";
$lang['bonus2'] = "BÔNUS DE LUCRATIVIDADE DIÁRIA EM REDE";
$lang['descbonus2'] = "<p>Receba enquanto ativo diariamente <b>0.5%</b> sobre o valor ativo de seus indicados diretos.
<br><b>Ex:</b> Indicou alguém com pacote de $1,000.00 e ganhe $5.00 ao dia, em 90 dias ganhou $450.00.
</p>";
$lang['bonus3'] = "BÔNUS DE INDICAÇÃO DIRETA";
$lang['descbonus3'] = "<p>Receba <b>7%</b> sobre o valor ativo adquirido por seu indicado.
<br><b>Ex:</b> Indicou alguém com pacote de $1,000.00 e ganhou automaticamente $70.00.
</p>";
$lang['bonus4'] = "BÔNUS DE INDICAÇÃO INDIRETA";
$lang['descbonus4'] = "<p>Receba <b>5%</b> sobre o valor ativo adquirido por um indicado indireto.
<br><b>Ex:</b> Seu indicado indicou alguém com pacote de $1,000.00 e você ganhou automaticamente $50.00
<br>O Bônus paga até o 5º nível! Espetacular, né?! ;D
</p>";
$lang['bonus5'] = "BÔNUS BINÁRIO";
$lang['descbonus5'] = "<p>Receba diariamente <b>20%</b> em bônus de sua perna menor de sua rede binária.
<br><b>Ex:</b> Tem 5000 pontos na perna esquerda e 6000 pontos na direita, pagamos 20% o valor da menor perna diariamente, ou seja ganhou $1.000,00.
<br>Cada ponto vale $1.00.
</p>";
$lang['bonus6'] = "BÔNUS DE CONHECIMENTO (+Prêmios)";
$lang['descbonus6'] = "<p>Receba <b>muito conhecimento</b> com nossos cursos de investimento. ;)
<br>Nada mais valioso que o conhecimento, né?! :D
</p>";
$lang['msgteto'] = "<font color='red'><b>*</b></font> Plano válido por 90 dias ou até o atingir o teto de 450%. <font color='red'><b>*</b></font>
<br><font color='red'><b>*</b></font> Lucratividade diária e lucratividade diária em rede pagos de Segunda à Sexta. <font color='red'><b>*</b></font>";

##### PLANO			
$lang['nossoplano'] = "Nosso Plano ;)";
$lang['saibamaisplano'] = "Saiba mais sobre nosso plano";
$lang['plano1'] = "INVESTIDOR <font color='#0B8480'>450%</font>";
$lang['digite'] = "Digite";
$lang['calcular'] = "Calcular";
$lang['digitevalor'] = "Digite um valor.";
$lang['produto'] = "<font color='#0B8480'>Direito a Cursos Online. <strong>(Ebooks/Audiobooks)</strong></font>";
$lang['planocarreira'] = "Plano de carreira com <strong>prêmios</strong> <br>(dinheiro, viagens, carros, etc)";		
$lang['participacao'] = "Participação nos investimentos da empresa.";
$lang['ganhebinario'] = "Ganhe <strong>15%</strong> no binário.";
$lang['saqueminimo'] = "Saque mínimo de <strong>$10.00</strong>.";
$lang['validadeplano'] = "Válido por 90 dias.";		
$lang['ganhominimo'] = "Ganho mínimo em 90 dias:";

##### APRESENTAÇÃO			
$lang['saibamaisplano'] = "Conheça nosso plano de negócio";
$lang['planodenegocios'] = "Plano de negócios";
$lang['anterior'] = "Anterior";
$lang['proxima'] = "Próxima";

##### FALE CONOSCO	
$lang['faleconosco'] = "Fale conosco";
$lang['ondeencontrar'] = "Onde você pode nos encontrar";
$lang['telefone'] = "Telefone";
$lang['atendimento'] = "Atendimento";
$lang['hratendimento'] = "Segunda - Sexta 9:00am - 18:00pm";
$lang['copyright'] = "Todos os direitos reservados.";
$lang['creditos'] = "<br />Desenvolvido por <a href='http://www.felipesites.com/' style='color: inherit;' target='_blank'>
<b>felipesites.com</b></a>.";

##### JAVASCRIPT		
$lang['valormin'] = "Valor minimo $10.00!";
$lang['valormax'] = "Valor maximo $10,000.00!";
$lang['pontos'] = "pontos";
$lang['vale'] = "Vale";
$lang['receba'] = "Receba";
$lang['aodia'] = "ao dia. (5% diário)";
$lang['alertavalor'] = "Digite um valor. (Minimo $10.00 / Maximo $10,000.00)";
$lang['precadastro'] = "<h2>Fique atento!<br><br>Pré-cadastro liberado 25/09!<br><br>
Seja um dos primeiros a entrar neste negócio e mude sua vida! Nós estamos te esperando.<br><br>
FELIPE SITES - Tudo em um Só Lugar!</h2>";